package ites.connection.protocol;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.util.Log;

import ites.connection.interfaces.ConnectionHost;
import ites.connection.interfaces.OnActiveServerChangeListener;
import ites.connection.interfaces.OnConnectionStateListener;
import ites.connection.interfaces.OnServerListChangeListener;

public class ConnectionClient implements Preference.OnPreferenceChangeListener, Observer {
    private static final String                       TAG                    = ":>"
                                                                                     + ConnectionClient.class
                                                                                             .getSimpleName();

    public static final String                        ACTIVE_SERVER_NAME_KEY = "server_name";
    public static final String                        ACTIVE_SERVER_ID_KEY   = "server_id";
    public static final String                        IP_PORT_KEY            = ConnectionServerIp.IP_PORT_KEY;
    public static final String                        OPTIONAL_IP_KEY        = ConnectionServerIp.OPTIONAL_IP_KEY;
    public static final String                        BTH_SEVICE_UUID_KEY    = ConnectionServerBluetooth.BTH_SEVICE_UUID_KEY;
    public static final String                        BTH_SEVICE_NAME_KEY    = ConnectionServerBluetooth.BTH_SEVICE_NAME_KEY;

    // Message codes
    static final int                                  MSG_INVALID            = -1;
    static final int                                  MSG_HANDSHAKE          = 1;                                            // UDP
    static final int                                  MSG_CONNECT            = 2;                                            // TCP
    static final int                                  MSG_ONLINE             = 3;                                            // UDP
    static final int                                  MSG_GOODBYE            = 4;                                            // UDP
    static final int                                  MSG_DATA               = 255;                                          // TCP
                                                                                                                              // (Max
                                                                                                                              // value)

    // Datagram "Magic": 8 bytes
    static final String                               MAGIC_CLIENT           = "ICP#Clnt";
    static final String                               MAGIC_SERVER           = "ICP#Srvr";

    static final int                                  MAGIC_LENGTH           = 8;
    // Header: "Magic"; 4b (int) code; 4b (int) data length
    static final int                                  HEADER_LENGTH          = MAGIC_LENGTH + 8;

    static final int                                  MSG_MAX_SIZE           = 1048576;

    private ConnectionHost                            mHostApp;

    private String                                    mName;
    private int                                       mFlavour;

    // private boolean mIsHtc;
    // private HtcHandshakeTask mHtcHandshakeTask;
    // private Timer mHtcHandshakeTimer = new Timer();

    private int                                       mIpPort;
    private String                                    mOptionalIp;
    private UUID                                      mBthServiceUuid;                                                       // =
                                                                                                                              // UUID.fromString("c61d3581-bea9-4a86-93ee-e30202804000");
    private String                                    mBthServiceName;                                                       // =
                                                                                                                              // "Inventory Data Exchange";

    private Map<Protocol, ConnectionServer>           mConnectionServers;

    private boolean                                   mStarted;

    private ConnectionsQueue                          mConnectionsQueue;

    private ServerList                                mServers;

    // Listeners
    private HashSet<OnActiveServerChangeListener>     mActiveServerListeners;
    private HashSet<OnConnectionStateListener>        mConnectionStateListeners;
    private OnConnectionStateListener.ConnectionState mCurrentConnectionState;
    private Protocol                                  mCurrentConnectionProtocol;
    private HashSet<OnServerListChangeListener>       mServerListListeners;

    @SuppressWarnings("ALL")
    interface ConnectionFlowManager {
        public void serverStarted(ConnectionAddress address, byte[] data);

        public void serverIsOnline(ConnectionAddress address, byte[] data);

        public void serverStopped(ConnectionAddress address, byte[] data);

        public void serverConnected(ConnectionAddress address, byte[] data);
    }

    static class DataHandler extends Handler {
        static final String      DATA_KEY    = "data";
        static final String      ADDRESS_KEY = "address";

        private ConnectionClient mMaster;

        DataHandler(ConnectionClient master) {
            mMaster = master;
        }

        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            try {
                mMaster.read((ConnectionAddress) bundle.getParcelable(ADDRESS_KEY),
                        bundle.getByteArray(DATA_KEY));
            } catch (Exception ignored) {
            }
        }
    }

    static class StateHandler extends Handler {
        static final int         STATE_ACTIVE   = 1;
        static final int         STATE_INACTIVE = 0;

        private ConnectionClient mMaster;

        StateHandler(ConnectionClient master) {
            mMaster = master;
        }

        @Override
        public void handleMessage(Message msg) {
            final Protocol protocol = Protocol.fromInt(msg.arg1);

            if (msg.what == STATE_ACTIVE) {
                // new Thread(new Runnable() {
                // @Override
                // public void run() {
                mMaster.mConnectionServers.get(protocol).sendHandshake(null);
                // }
                // }).run();

            } else if (msg.what == STATE_INACTIVE) {
                ServerList servers = mMaster.mServers;
                for (int i = servers.getSize() - 1; i >= 0; i--) {
                    ServerListItem server = servers.get(i);
                    if (server.getProtocol() == protocol) {
                        servers.serverStopped(server.getAddress(), server.getName().getBytes());
                    }
                }
            }
            mMaster.mServers.notifyChanged(null);
        }
    }

    // private class HtcHandshakeTask extends TimerTask {
    // private static final String TAG = ":>HtcHandshakeTask";
    //
    // public void run() {
    // Log.d(TAG, "HtcHandshakeTask.run():");
    // if (mHostApp.isInForeground()) {
    // if (!mActiveServerListeners.isEmpty() || !mServers.isConnectedAndOnline()
    // || mServers.getActiveServer().isDumb()) {
    // ConnectionServer ipServer = mConnectionServers.get(Protocol.IpV4);
    // if (ipServer != null && ipServer.isActive()) {
    // ipServer.sendHandshake(null);
    // }
    // }
    // }
    // }
    // }

    public ConnectionClient(ConnectionHost host, short flavour) {
        mHostApp = host;

        mName = android.os.Build.MODEL + " (" + android.os.Build.VERSION.RELEASE + ")";
        // mIsHtc = mName.startsWith("HTC");

        mFlavour = (int) flavour << 16;

        DataHandler dataHandler = new DataHandler(this);
        StateHandler stateHandler = new StateHandler(this);

        mConnectionsQueue = new ConnectionsQueue();
        mConnectionsQueue.start();

        mConnectionServers = new EnumMap<>(Protocol.class);
        mConnectionServers.put(Protocol.IpV4, new ConnectionServerIp(mName, this, dataHandler,
                stateHandler));
        mConnectionServers.put(Protocol.Bluetooth, new ConnectionServerBluetooth(mName, this,
                dataHandler, stateHandler));

        mServers = new ServerList(this);
        mServers.addObserver(this);

        mActiveServerListeners = new HashSet<>();
        mConnectionStateListeners = new HashSet<>();
        mCurrentConnectionState = OnConnectionStateListener.ConnectionState.Unconnected;
        mCurrentConnectionProtocol = Protocol.Unknown;
        mServerListListeners = new HashSet<>();
    }

    synchronized private void read(ConnectionAddress address, byte[] data) {
        Log.d(TAG, "read(): from " + address.toString());
        int code = MSG_INVALID;
        ByteBuffer buffer = null;
        if (data.length >= HEADER_LENGTH) {
            buffer = ByteBuffer.wrap(data);

            byte[] magicBytes = new byte[8];
            int tmpCode = buffer.get(magicBytes).getInt();
            String magic = new String(magicBytes);

            if (magic.equals(MAGIC_SERVER) && (tmpCode & mFlavour) == mFlavour) {
                code = tmpCode & 0xffff;
            }
        }

        if (code != MSG_INVALID) {
            int dataLength = buffer.getInt();
            if (dataLength != buffer.limit() - HEADER_LENGTH) {
                return;
            }

            byte[] message = new byte[dataLength];
            buffer.get(message);

            switch (code) {
            case MSG_HANDSHAKE:
                Log.d(TAG, "read(): MSG_HANDSHAKE");
                mServers.serverStarted(address, message);
                break;

            case MSG_CONNECT:
                Log.d(TAG, "read(): MSG_CONNECT");
                mServers.serverConnected(address, message);
                break;

            case MSG_ONLINE:
                Log.d(TAG, "read(): MSG_ONLINE");
                mServers.serverIsOnline(address, message);
                break;

            case MSG_GOODBYE:
                Log.d(TAG, "read(): MSG_GOODBYE");
                mServers.serverStopped(address, message);
                break;

            case MSG_DATA:
                Log.d(TAG, "read(): MSG_DATA");
                mHostApp.onNetworkMessageReceived(message);
                break;

            default:
                break;
            }
        }
    }

    public boolean isRunning() {
        if (mStarted) {
            for (Map.Entry<Protocol, ConnectionServer> entry : mConnectionServers.entrySet()) {
                if (entry.getValue().isActive()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void start() {
        Log.d(TAG, "start()");
        if (isRunning()) {
            return;
        }
        mStarted = true;

        Bundle params = new Bundle();
        // Ip
        params.putInt(ConnectionServerIp.IP_PORT_KEY, mIpPort);
        params.putString(ConnectionServerIp.OPTIONAL_IP_KEY, mOptionalIp);
        // Bluetooth
        params.putSerializable(ConnectionServerBluetooth.BTH_SEVICE_UUID_KEY, mBthServiceUuid);
        params.putString(ConnectionServerBluetooth.BTH_SEVICE_NAME_KEY, mBthServiceName);

        boolean isStarted = false;
        for (Map.Entry<Protocol, ConnectionServer> entry : mConnectionServers.entrySet()) {
            ConnectionServer server = entry.getValue();
            server.start(params);
            if (server.isStarted()) {
                isStarted = true;
            }
        }

        mStarted = isStarted;

        // if (mStarted && mIsHtc) {
        // mHtcHandshakeTask = new HtcHandshakeTask();
        // mHtcHandshakeTimer.schedule(mHtcHandshakeTask, 6000, 6000);
        // }
    }

    public void stop() {
        Log.d(TAG, "stop()");
        if (!mStarted) {
            return;
        }
        mStarted = false;

        for (Map.Entry<Protocol, ConnectionServer> entry : mConnectionServers.entrySet()) {
            ConnectionServer server = entry.getValue();
            server.stop();
        }

        // if (mIsHtc && mHtcHandshakeTask != null) {
        // mHtcHandshakeTask.cancel();
        // mHtcHandshakeTask = null;
        // mHtcHandshakeTimer.purge();
        // }

        mServers.notifyChanged(null);
    }

    ServerList getServerList() {
        return mServers;
    }

    public Bundle getParameters() {
        Bundle p = new Bundle();
        ServerListItem server = mServers.getActiveServer();
        if (server != null) {
            p.putLong(ACTIVE_SERVER_ID_KEY, server.getId());
            p.putString(ACTIVE_SERVER_NAME_KEY, server.getName());
        }
        p.putInt(IP_PORT_KEY, mIpPort);
        p.putString(OPTIONAL_IP_KEY, mOptionalIp);
        p.putSerializable(BTH_SEVICE_UUID_KEY, mBthServiceUuid);
        p.putString(BTH_SEVICE_NAME_KEY, mBthServiceName);

        return p;
    }

    public void setParameters(Bundle p) {
        boolean restartIpServer = false;

        if (p.containsKey(IP_PORT_KEY)) {
            int ipPort = p.getInt(IP_PORT_KEY);
            if (ipPort != mIpPort) {
                mIpPort = ipPort;
                restartIpServer = true;
            }
        }

        if (p.containsKey(OPTIONAL_IP_KEY)) {
            String optionalIp = p.getString(OPTIONAL_IP_KEY);
            if (!(optionalIp == null || optionalIp.equals(mOptionalIp))) {
                mOptionalIp = optionalIp;
                restartIpServer = true;
            }
        }

        if (restartIpServer) {
            ConnectionServer ipServer = mConnectionServers.get(Protocol.IpV4);
            if (ipServer != null && ipServer.isStarted()) {
                Bundle ipParams = new Bundle();
                ipParams.putInt(ConnectionServerIp.IP_PORT_KEY, mIpPort);
                ipParams.putString(ConnectionServerIp.OPTIONAL_IP_KEY, mOptionalIp);

                ipServer.stop();
                ipServer.start(ipParams);
                mStarted = mStarted || ipServer.isStarted();
            }
        }

        if (p.containsKey(BTH_SEVICE_UUID_KEY)) {
            mBthServiceUuid = (UUID) p.getSerializable(BTH_SEVICE_UUID_KEY);

            if (p.containsKey(BTH_SEVICE_NAME_KEY)) {
                mBthServiceName = p.getString(BTH_SEVICE_NAME_KEY);
            }

            ConnectionServer ipServer = mConnectionServers.get(Protocol.Bluetooth);
            if (ipServer != null && ipServer.isStarted()) {
                Bundle bthParams = new Bundle();
                bthParams.putSerializable(ConnectionServerBluetooth.BTH_SEVICE_UUID_KEY,
                        mBthServiceUuid);
                bthParams.putString(ConnectionServerBluetooth.BTH_SEVICE_NAME_KEY, mBthServiceName);

                ipServer.stop();
                ipServer.start(bthParams);
                mStarted = mStarted || ipServer.isStarted();
            }
        }

        if (p.containsKey(ACTIVE_SERVER_ID_KEY)) {
            if (!mStarted) {
                mServers.setInitialActiveServer(p.getString(ACTIVE_SERVER_NAME_KEY),
                        new ConnectionAddress(p.getLong(ACTIVE_SERVER_ID_KEY)));
            }
        }
    }

    public boolean hasNetwork(Protocol protocol) {
        ConnectionServer server = mConnectionServers.get(protocol);
        return server != null && server.isActive();
    }

    public boolean sendMessage(byte[] message) {
        Protocol protocol = mServers.getActiveServer().getProtocol();
        ConnectionServer server = mConnectionServers.get(protocol);
        return server != null && server.sendMessage(message);
    }

    void sendOnline(ConnectionAddress address, int interval) {
        ConnectionServer server = mConnectionServers.get(address.getProtocol());
        if (server != null) {
            server.sendOnline(address, interval);
        }
    }

    void sendConnected() {
        Protocol protocol = mServers.getActiveServer().getProtocol();
        ConnectionServer server = mConnectionServers.get(protocol);
        if (server != null) {
            server.sendConnected();
        }
    }

    void connectToServer(ServerListItem host) {
        if (host == null) {
            return;
        }

        final ConnectionServer server = mConnectionServers.get(host.getProtocol());
        if (server != null) {
            final ConnectionAddress address = host.getAddress();
            postToConnectionQueue(new Runnable() {
                @Override
                public void run() {
                    server.connectToHost(address);
                }
            });
        }
    }

    void disconnectFromServer(ServerListItem host) {
        if (host == null) {
            return;
        }

        final ConnectionServer server = mConnectionServers.get(host.getProtocol());
        if (server != null) {
            final ConnectionAddress address = host.getAddress();
            if (!server.disconnectFromHostImmediately(address)) {
                postToConnectionQueue(new Runnable() {
                    @Override
                    public void run() {
                        server.disconnectFromHost(address);
                    }
                });
            }
        }
    }

    void postToConnectionQueue(Runnable operation) {
        mConnectionsQueue.getHandler().post(operation);
    }

    void registerBroadcastReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        mHostApp.registerBroadcastReceiver(receiver, filter);
    }

    DhcpInfo getDhcpInfo() {
        return mHostApp.getDhcpInfo();
    }

    WifiManager getWifiManager() {
        return mHostApp.getWifiManager();
    }

    void runOnUiThread(Runnable runnable) {
        mHostApp.runOnUiThread(runnable);
    }

    byte[] wrapMessage(byte[] message, int messageCode, boolean prependSize) {
        int packetSize = HEADER_LENGTH + message.length;

        ByteBuffer buffer = ByteBuffer.allocate(packetSize + (prependSize ? 4 : 0));

        if (prependSize) {
            buffer.putInt(packetSize);
        }

        // Header
        buffer.put(MAGIC_CLIENT.getBytes());
        buffer.putInt(mFlavour | messageCode);
        buffer.putInt(message.length);

        // Message
        buffer.put(message);

        return buffer.array();
    }

    byte[] wrapServiceMessage(String magic, int messageCode, boolean prependSize) {
        int packetSize = MAGIC_LENGTH + 4;

        ByteBuffer buffer = ByteBuffer.allocate(packetSize + (prependSize ? 4 : 0));

        if (prependSize) {
            buffer.putInt(packetSize);
        }

        // Header
        buffer.put(magic.getBytes());
        buffer.putInt(mFlavour | (messageCode & 0xffff));

        return buffer.array();
    }

    public void wakeUp() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mServers.wakeUp();

                // // Specially for HTC phones
                // if (mIsHtc) {
                // for (Map.Entry<Protocol, ConnectionServer> entry :
                // mConnectionServers
                // .entrySet()) {
                // ConnectionServer server = entry.getValue();
                // if (server.isStarted()) {
                // server.sendHandshake(null);
                // }
                // }
                // }
            }
        }).run();
    }

    public void putToSleep() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mServers.putToSleep();
            }
        }).run();
    }

    public void sendHandshake() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<Protocol, ConnectionServer> entry : mConnectionServers.entrySet()) {
                    ConnectionServer server = entry.getValue();
                    if (server.isStarted()) {
                        server.sendHandshake(null);
                    }
                }
            }
        }).run();
    }

    @Override
    public void update(Observable observable, Object data) {
        notifyOnConnectionStateListeners();
        notifyOnServerListChangeListeners();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        try {
            long id = Long.decode(preference.getKey());
            Log.d(TAG, "onPreferenceChange(): id = " + id + ", newValue = " + newValue);
            if ((Boolean) newValue) {
                mServers.connect(new ConnectionAddress(id));

            } else {
                mServers.disconnect();
            }

            notifyOnActiveServerChangeListeners();

        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    void removeConnectedServer(ServerListItem server) {
        mHostApp.removeActiveServer(server.getId());
    }

    public synchronized void registerOnActiveServerChangeListener(
            OnActiveServerChangeListener listener) {
        if (listener != null && !mActiveServerListeners.contains(listener)) {
            mActiveServerListeners.add(listener);
        }
    }

    public synchronized void removeOnActiveServerChangeListener(
            OnActiveServerChangeListener listener) {
        if (listener != null && mActiveServerListeners.contains(listener)) {
            mActiveServerListeners.remove(listener);
        }
    }

    synchronized void notifyOnActiveServerChangeListeners() {
        ArrayList<OnActiveServerChangeListener> toRemoveList = new ArrayList<>();

        ServerListItem server = mServers.getActiveServer();
        long id = -1;
        String name = null;
        if (server != null) {
            id = server.getId();
            name = server.getName();
        }

        for (OnActiveServerChangeListener listener : mActiveServerListeners) {
            try {
                listener.onActiveServerChanged(id, name);
            } catch (Exception e) {
                e.printStackTrace();
                toRemoveList.add(listener);
            }
        }

        for (OnActiveServerChangeListener listener : toRemoveList) {
            removeOnActiveServerChangeListener(listener);
        }
    }

    public synchronized void registerOnConnectionStateListener(OnConnectionStateListener listener) {
        if (listener != null && !mConnectionStateListeners.contains(listener)) {
            mConnectionStateListeners.add(listener);
            listener.onConnectionStateChanged(mCurrentConnectionState, mCurrentConnectionProtocol);
        }
    }

    public synchronized void removeOnConnectionStateListener(OnConnectionStateListener listener) {
        if (listener != null && mConnectionStateListeners.contains(listener)) {
            mConnectionStateListeners.remove(listener);
        }
    }

    synchronized void notifyOnConnectionStateListeners() {
        ArrayList<OnConnectionStateListener> toRemoveList = new ArrayList<>();

        OnConnectionStateListener.ConnectionState state = getCurrentConnectionState();
        Protocol protocol = getCurrentConnectionProtocol();
        if (!(mCurrentConnectionState == state && mCurrentConnectionProtocol == protocol)) {
            mCurrentConnectionState = state;
            mCurrentConnectionProtocol = protocol;
            for (OnConnectionStateListener listener : mConnectionStateListeners) {
                try {
                    listener.onConnectionStateChanged(mCurrentConnectionState,
                            mCurrentConnectionProtocol);
                } catch (Exception e) {
                    e.printStackTrace();
                    toRemoveList.add(listener);
                }
            }

            for (OnConnectionStateListener listener : toRemoveList) {
                removeOnConnectionStateListener(listener);
            }
        }
    }

    public OnConnectionStateListener.ConnectionState getCurrentConnectionState() {
        ServerListItem server = mServers.getActiveServer();
        OnConnectionStateListener.ConnectionState state;

        if (server == null) {
            state = OnConnectionStateListener.ConnectionState.Unconnected;
        } else {
            if (mServers.isConnecting()) {
                state = OnConnectionStateListener.ConnectionState.Connecting;
            } else {
                if (mServers.isConnectedAndOnline()) {
                    state = server.isDumb() ? OnConnectionStateListener.ConnectionState.Dumb
                            : OnConnectionStateListener.ConnectionState.Active;

                } else {
                    state = OnConnectionStateListener.ConnectionState.Inactive;
                }
            }
        }

        return state;
    }

    public Protocol getCurrentConnectionProtocol() {
        ServerListItem server = mServers.getActiveServer();
        return server == null ? Protocol.Unknown : server.getProtocol();
    }

    public synchronized void registerOnServerListChangeListener(OnServerListChangeListener listener) {
        if (listener != null && !mServerListListeners.contains(listener)) {
            mServerListListeners.add(listener);
            listener.onServerListChanged(makeServerListInfo());
        }
    }

    public synchronized void removeOnServerListChangeListener(OnServerListChangeListener listener) {
        if (listener != null && mServerListListeners.contains(listener)) {
            mServerListListeners.remove(listener);
        }
    }

    synchronized void notifyOnServerListChangeListeners() {
        ArrayList<OnServerListChangeListener> toRemoveList = new ArrayList<>();

        for (OnServerListChangeListener listener : mServerListListeners) {
            try {
                listener.onServerListChanged(makeServerListInfo());
            } catch (Exception e) {
                e.printStackTrace();
                toRemoveList.add(listener);
            }
        }

        for (OnServerListChangeListener listener : toRemoveList) {
            removeOnServerListChangeListener(listener);
        }
    }

    private ArrayList<OnServerListChangeListener.ServerInfo> makeServerListInfo() {
        ArrayList<OnServerListChangeListener.ServerInfo> list = new ArrayList<>();

        ServerListItem activeServer = mServers.getActiveServer();

        for (int i = 0; i < mServers.getSize(); i++) {
            ServerListItem server = mServers.get(i);

            OnServerListChangeListener.ServerInfo info = new OnServerListChangeListener.ServerInfo();
            info.name = server.getName();
            info.address = server.getAddress().toString();
            info.id = server.getId();
            info.protocol = server.getProtocol();
            if (server.equals(activeServer)) {
                if (mServers.isConnecting()) {
                    info.state = OnServerListChangeListener.ServerState.Connecting;
                } else {
                    if (mServers.isConnectedAndOnline()) {
                        info.state = server.isDumb() ? OnServerListChangeListener.ServerState.Dumb
                                : OnServerListChangeListener.ServerState.Active;

                    } else {
                        info.state = OnServerListChangeListener.ServerState.Inactive;
                    }
                }

                info.isActive = true;
            } else {
                info.isActive = false;
                info.state = OnServerListChangeListener.ServerState.Active;
            }

            list.add(info);
        }

        return list;
    }
}
