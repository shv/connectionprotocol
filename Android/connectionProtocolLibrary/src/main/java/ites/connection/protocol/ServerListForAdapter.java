package ites.connection.protocol;

import android.content.Context;
import android.preference.PreferenceCategory;

import java.util.EnumMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import ites.connection.R;

public class ServerListForAdapter implements Observer {
    private static final String                    TAG           = ":>"
                                                                         + ServerListForAdapter.class
                                                                                 .getSimpleName();

    private static final int                       IC_ACTIVE     = 0;
    private static final int                       IC_INACTIVE   = 1;
    private static final int                       IC_CONNECTING = 2;
    private static final int                       IC_DUMB       = 3;

    public static final String                     NAME_KEY      = "name";

    private ConnectionClient                       mConnectionClient;
    private ServerList                             mServers;

    private Map<Protocol, int[]> mIcons;

    public ServerListForAdapter(ConnectionClient connectionClient) {
        mConnectionClient = connectionClient;
        mServers = mConnectionClient.getServerList();

        mIcons = new EnumMap<>(Protocol.class);
        mIcons.put(Protocol.IpV4,
                new int[] { R.drawable.ic_wifi_active, R.drawable.ic_wifi_inactive,
                        R.drawable.ic_wifi_connecting, R.drawable.ic_wifi_dumb });

        mIcons.put(Protocol.Bluetooth, new int[] {
                R.drawable.ic_bluetooth_active, R.drawable.ic_bluetooth_inactive,
                R.drawable.ic_bluetooth_connecting, R.drawable.ic_bluetooth_dumb });

        mIcons.put(Protocol.Unknown, new int[] { 0, 0, 0, 0 });

        mServers.addObserver(this);
        mServers.notifyChanged(null);
    }

    @Override
    public void update(Observable observable, Object data) {

    }
}
