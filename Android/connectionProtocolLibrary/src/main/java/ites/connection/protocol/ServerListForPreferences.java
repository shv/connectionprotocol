package ites.connection.protocol;

import ites.connection.R;

import java.util.EnumMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.util.Log;

public class ServerListForPreferences implements Observer {
    private static final String  TAG           = ":>"
                                                       + ServerListForPreferences.class
                                                               .getSimpleName();

    private static final int     IC_ACTIVE     = 0;
    private static final int     IC_INACTIVE   = 1;
    private static final int     IC_CONNECTING = 2;
    private static final int     IC_DUMB       = 3;

    private Context              mContext;
    private PreferenceCategory   mParentCategory;
    private ConnectionClient     mConnectionClient;
    private ServerList           mServers;
    private Map<Protocol, int[]> mIcons;

    public ServerListForPreferences(Context context, PreferenceCategory parentCategory,
            ConnectionClient connectionClient) {
        mContext = context;
        mParentCategory = parentCategory;
        mConnectionClient = connectionClient;
        mServers = mConnectionClient.getServerList();

        mIcons = new EnumMap<>(Protocol.class);
        mIcons.put(Protocol.IpV4,
                new int[] { R.drawable.ic_wifi_active, R.drawable.ic_wifi_inactive,
                        R.drawable.ic_wifi_connecting, R.drawable.ic_wifi_dumb });

        mIcons.put(Protocol.Bluetooth, new int[] { R.drawable.ic_bluetooth_active,
                R.drawable.ic_bluetooth_inactive, R.drawable.ic_bluetooth_connecting,
                R.drawable.ic_bluetooth_dumb });

        mIcons.put(Protocol.Unknown, new int[] { 0, 0, 0, 0 });

        mServers.addObserver(this);
        mServers.notifyChanged(null);
    }

    public void release() {
        mServers.deleteObserver(this);
    }

    @Override
    public void update(Observable observable, final Object data) {
        // Update list
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateConnections((ServerListItem) data);
            }
        });
    }

    @SuppressLint("NewApi")
    synchronized void updateConnections(ServerListItem item) {
        Resources res = mContext.getResources();
        ServerListItem activeServer = mServers.getActiveServer();
        mParentCategory.removeAll();

        if (mServers.getSize() == 0) {

            Preference pref = new Preference(mContext);
            pref.setKey(res.getString(R.string.preference_no_connections_key));
            pref.setTitle(res.getString(R.string.preference_no_connections));
            pref.setSelectable(false);
            pref.setEnabled(false);
            pref.setPersistent(false);

            mParentCategory.addPreference(pref);

        } else {
            // Get networks availability
            boolean connectionIsAvailable = false;
            Map<Protocol, Boolean> networks = new EnumMap<>(Protocol.class);
            Protocol[] protocols = Protocol.values();
            for (Protocol protocol : protocols) {
                boolean available = mConnectionClient.hasNetwork(protocol);
                connectionIsAvailable = connectionIsAvailable || available;
                networks.put(protocol, available);
            }

            boolean showIcons = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB;
            boolean haveConnection = (activeServer != null);

            for (int i = 0; i < mServers.getSize(); i++) {
                ServerListItem server = mServers.get(i);
                Log.d(TAG,
                        "updateConnections(): server = " + server + ", protocol = "
                                + server.getProtocol());

                int[] icons = mIcons.get(server.getProtocol());
                boolean networkIsActive = networks.get(server.getProtocol());

                CheckBoxPreference pref = new CheckBoxPreference(mContext);
                pref.setKey(String.valueOf(server.getId()));
                pref.setSummary(server.getAddress().toString());
                pref.setPersistent(false);
                pref.setOnPreferenceChangeListener(mConnectionClient);

                mParentCategory.addPreference(pref);

                if (server.equals(activeServer)) {
                    pref.setChecked(true);

                    if (mServers.isConnecting()) {
                        pref.setTitle(server.getName() + res.getString(R.string.connecting));

                        if (showIcons) {
                            pref.setIcon(icons[IC_CONNECTING]);
                        }

                    } else {
                        if (mServers.isConnectedAndOnline()) {
                            pref.setTitle(server.getName()
                                    + (server.isDumb() ? res.getString(R.string.not_responding)
                                            : ""));

                            if (showIcons) {
                                if (networkIsActive) {
                                    pref.setIcon(icons[server.isDumb() ? IC_DUMB : IC_ACTIVE]);
                                } else {
                                    pref.setIcon(icons[IC_INACTIVE]);
                                }
                            }

                        } else {
                            pref.setTitle(server.getName() + res.getString(R.string.offline));

                            if (showIcons) {
                                pref.setIcon(icons[IC_INACTIVE]);
                            }
                        }
                    }

                    pref.setSelectable(connectionIsAvailable);
                    pref.setEnabled(connectionIsAvailable);

                } else {
                    pref.setTitle(server.getName()
                            + (server.isDumb() ? res.getString(R.string.not_responding) : ""));

                    if (showIcons) {
                        if (networkIsActive) {
                            pref.setIcon(icons[server.isDumb() ? IC_DUMB : IC_ACTIVE]);
                        } else {
                            pref.setIcon(icons[IC_INACTIVE]);
                        }
                    }

                    boolean enabled = !haveConnection && networkIsActive && !server.isDumb();
                    pref.setSelectable(enabled);
                    pref.setEnabled(enabled);
                }
            }
        }
    }
}
