package ites.connection.protocol;

import ites.connection.protocol.ConnectionClient.ConnectionFlowManager;

import java.util.Observable;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
//import java.util.ArrayList;
//import java.util.HashMap;

import android.util.Log;

/**
 * @author Administrator
 * 
 */
class ServerList extends Observable implements ConnectionFlowManager {
    private static final String                                  TAG                             = ":>"
                                                                                                         + ServerList.class
                                                                                                                 .getSimpleName();
    static final int                                             SERVER_STATUS_NONE              = 0;
    static final int                                             SERVER_STATUS_CONNECTING_NEW    = 1;
    static final int                                             SERVER_STATUS_CONNECTING_RESUME = 2;
    static final int                                             SERVER_STATUS_CONNECTED_ONLINE  = 3;
    static final int                                             SERVER_STATUS_CONNECTED_OFFLINE = 4;

    private static final int                                     TIMEOUT                         = 5000;
    private static final int                                     SLEEPING_TIMEOUT                = 30000;

    private ConnectionClient                                     mMaster;

    // private ArrayList<ServerListItem> mServerList = new
    // ArrayList<ServerListItem>();
    // private HashMap<InetAddress, ServerListItem> mServerMap = new
    // HashMap<InetAddress, ServerListItem>();
    private CopyOnWriteArrayList<ServerListItem>                 mServerList                     = new CopyOnWriteArrayList<>();
    private ConcurrentHashMap<ConnectionAddress, ServerListItem> mServerMap                      = new ConcurrentHashMap<>();

    private ServerListItem                                       mActiveServer;
    private int                                                  mServerStatus                   = SERVER_STATUS_NONE;

    private Timer                                                mTimer                          = new Timer();

    private boolean                                              mSleeping;

    ServerList(ConnectionClient master) {
        mMaster = master;
    }

    void setInitialActiveServer(String name, ConnectionAddress address) {
        if (getSize() == 0) {
            mActiveServer = new ServerListItem(this, name, address);
            append(mActiveServer);
            mServerStatus = SERVER_STATUS_CONNECTED_OFFLINE;
        }
    }

    void sendOnlineMessage(ConnectionAddress address) {
        mMaster.sendOnline(address, mSleeping ? SLEEPING_TIMEOUT : TIMEOUT);
    }

    synchronized boolean contains(ServerListItem server) {
        return find(server) != null;
    }

    private synchronized ServerListItem find(ServerListItem server) {
        int index = mServerList.indexOf(server);

        if (index != -1) {
            return mServerList.get(index);

        } else {
            for (ServerListItem item : mServerList) {
                if (item.equals(server)) {
                    return item;
                }
            }
        }
        return null;
    }

    synchronized void putToSleep() {
        if (!mSleeping) {
            Log.d(TAG, "putToSleep()");
            mSleeping = true;
            for (ServerListItem server : mServerList) {
                if (!server.equals(mActiveServer)
                        || mServerStatus != SERVER_STATUS_CONNECTED_OFFLINE) {
                    server.stopOnlineCheck(mTimer, false);
                    server.startOnlineCheck(mTimer, SLEEPING_TIMEOUT);
                }
            }
            mTimer.purge();
        }
    }

    synchronized void wakeUp() {
        if (mSleeping) {
            Log.d(TAG, "wakeUp()");
            mSleeping = false;
            for (ServerListItem server : mServerList) {
                if (!server.equals(mActiveServer)
                        || mServerStatus != SERVER_STATUS_CONNECTED_OFFLINE) {
                    server.stopOnlineCheck(mTimer, false);
                    server.startOnlineCheck(mTimer, TIMEOUT);
                }
            }
            mTimer.purge();
        }
    }

    private synchronized void append(ServerListItem server) {
        Log.d(TAG, "append()");
        if (server == null || contains(server)) {
            return;
        }

        mServerList.add(server);
        mServerMap.put(server.getAddress(), server);

        setChanged();
        notifyObservers();
    }

    void append(String name, ConnectionAddress address) {
        ServerListItem server = new ServerListItem(this, name, address);
        this.append(server);

        server.startOnlineCheck(mTimer, mSleeping ? SLEEPING_TIMEOUT : TIMEOUT);
    }

    synchronized void remove(ServerListItem server) {
        if (server == null) {
            return;
        }

        server.stopOnlineCheck(mTimer);

        if (server.equals(mActiveServer)) {
            mActiveServer = null;
            mServerStatus = SERVER_STATUS_NONE;
        }

        mServerList.remove(server);
        mServerMap.remove(server.getAddress());

        setChanged();
        notifyObservers();
    }

    void remove(int index) {
        remove(get(index));
    }

    void remove(ConnectionAddress address) {
        remove(get(address));
    }

    synchronized ServerListItem get(int index) {
        if (index < 0 || index >= mServerList.size()) {
            return null;
        }

        return mServerList.get(index);
    }

    synchronized ServerListItem get(ConnectionAddress address) {
        return mServerMap.get(address);
    }

    long getId(int index) {
        ServerListItem item = get(index);

        return (item == null) ? 0 : item.getId();
    }

    long getId(ConnectionAddress address) {
        ServerListItem item = get(address);

        return (item == null) ? 0 : item.getId();
    }

    synchronized int getSize() {
        return mServerList.size();
    }

    synchronized ServerListItem getActiveServer() {
        return mActiveServer;
    }

    synchronized boolean isConnecting() {
        return mServerStatus == SERVER_STATUS_CONNECTING_NEW
                || mServerStatus == SERVER_STATUS_CONNECTING_RESUME;
    }

    synchronized boolean isConnectedAndOnline() {
        return mServerStatus == SERVER_STATUS_CONNECTED_ONLINE;
    }

    void notifyChanged(final ServerListItem server) {
        setChanged();
        mMaster.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyObservers(server);
            }
        });
    }

    synchronized void connect(ConnectionAddress address) {
        Log.d(TAG, "connect(): " + mActiveServer);
        ServerListItem server = get(address);
        if (mActiveServer == null) {
            mActiveServer = server;
            if (mActiveServer != null) {
                mServerStatus = SERVER_STATUS_CONNECTING_NEW;
                notifyChanged(mActiveServer);
                mMaster.connectToServer(mActiveServer);
            }
        }
    }

    synchronized void disconnect() {
        if (mActiveServer != null) {
            ServerListItem server = mActiveServer;
            switch (mServerStatus) {
            case SERVER_STATUS_CONNECTED_ONLINE:
            case SERVER_STATUS_CONNECTING_NEW:
            case SERVER_STATUS_CONNECTING_RESUME:
                mMaster.disconnectFromServer(mActiveServer);
                mServerStatus = SERVER_STATUS_NONE;
                mActiveServer = null;
                notifyChanged(server);
                break;

            case SERVER_STATUS_CONNECTED_OFFLINE:
                remove(mActiveServer);
                break;
            }
        }
    }

    synchronized void connectionLost(ServerListItem server) {
        if (server.equals(mActiveServer)) {
            mMaster.disconnectFromServer(server);
            mServerStatus = SERVER_STATUS_CONNECTED_OFFLINE;
            notifyChanged(server);
            server.stopOnlineCheck(mTimer);

        } else {
            remove(server);
        }
    }

    synchronized void connectionFailed() {
        if (mActiveServer != null) {
            final ServerListItem server = mActiveServer;
            if (mServerStatus == SERVER_STATUS_CONNECTING_NEW) {
                Log.d(TAG, "connectionFailed(): mServerStatus == SERVER_STATUS_CONNECTING_NEW");
                mServerStatus = SERVER_STATUS_NONE;
                mActiveServer = null;
                mMaster.removeConnectedServer(server);

            } else if (mServerStatus == SERVER_STATUS_CONNECTING_RESUME) {
                Log.d(TAG, "connectionFailed(): mServerStatus == SERVER_STATUS_CONNECTING_RESUME");
                mServerStatus = SERVER_STATUS_CONNECTED_OFFLINE;
            }

            notifyChanged(server);
        }
    }

    @Override
    synchronized public void serverStarted(ConnectionAddress address, byte[] data) {
        String name = new String(data);
        ServerListItem server = find(new ServerListItem(this, name, address));
        if (server == null) {
            append(name, address);

        } else if (server.equals(mActiveServer) && mServerStatus == SERVER_STATUS_CONNECTED_OFFLINE) {
            mServerStatus = SERVER_STATUS_CONNECTING_RESUME;
            notifyChanged(server);
            server.startOnlineCheck(mTimer, mSleeping ? SLEEPING_TIMEOUT : TIMEOUT);
            mMaster.connectToServer(mActiveServer);
        }
    }

    @Override
    synchronized public void serverIsOnline(ConnectionAddress address, byte[] data) {
        String name = new String(data);
        ServerListItem server = find(new ServerListItem(this, name, address));
        if (server != null) {
            server.onlineAnswerReceived();
        }
    }

    @Override
    synchronized public void serverStopped(ConnectionAddress address, byte[] data) {
        String name = new String(data);
        ServerListItem server = find(new ServerListItem(this, name, address));
        if (server == null) {
            return;
        }

        if (server.equals(mActiveServer)) {
            switch (mServerStatus) {
            case SERVER_STATUS_CONNECTED_ONLINE:
            case SERVER_STATUS_CONNECTING_RESUME:
                mMaster.disconnectFromServer(server);
                mServerStatus = SERVER_STATUS_CONNECTED_OFFLINE;
                notifyChanged(server);
                server.stopOnlineCheck(mTimer);
                break;

            case SERVER_STATUS_CONNECTING_NEW:
                mMaster.disconnectFromServer(server);
                remove(server);
                break;
            }

        } else {
            remove(server);
        }
    }

    @Override
    synchronized public void serverConnected(ConnectionAddress address, byte[] data) {
        String name = new String(data);
        ServerListItem server = find(new ServerListItem(this, name, address));
        Log.d(TAG, "serverConnected(): " + address + ", " + server + ", " + mActiveServer + ", "
                + mServerStatus);
        if (server != null
                && server.equals(mActiveServer)
                && (mServerStatus == SERVER_STATUS_CONNECTING_NEW || mServerStatus == SERVER_STATUS_CONNECTING_RESUME)) {
            mServerStatus = SERVER_STATUS_CONNECTED_ONLINE;
            notifyChanged(server);
            mMaster.sendConnected();
        }
    }
}
