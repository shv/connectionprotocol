package ites.connection.protocol;

public enum Protocol {
    IpV4(0), Bluetooth(1), Unknown(-1);

    private int mValue;

    private Protocol(int value) {
        mValue = value;
    }

    public boolean equals(int value) {
        return mValue == value;
    }

    public int getValue() {
        return mValue;
    }

    public static Protocol fromInt(int value) {
        Protocol[] values = Protocol.values();
        for (Protocol v : values) {
            if (v.equals(value)) {
                return v;
            }
        }
        return Unknown;
    }
}