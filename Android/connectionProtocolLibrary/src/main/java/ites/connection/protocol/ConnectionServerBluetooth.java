package ites.connection.protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import ites.connection.protocol.ConnectionClient.DataHandler;
import ites.connection.protocol.ConnectionClient.StateHandler;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

class ConnectionServerBluetooth extends ConnectionServer {
    private static final String                  TAG                 = ":>"
                                                                             + ConnectionServerBluetooth.class
                                                                                     .getSimpleName();
    static final String                          BTH_SEVICE_UUID_KEY = "bth_sevice_uuid";
    static final String                          BTH_SEVICE_NAME_KEY = "bth_sevice_name";

    // Bluetooth service "Magic": 8 bytes
    static final String                          MAGIC_BLUETOOTH     = "ICP#Blth";
    // Bluetooth service codes
    static final int                             MSG_BTH_CONNECT     = 1;
    static final int                             MSG_BTH_DISCONNECT  = 2;

    private UUID                                 mServiceUuid;
    private String                               mServiceName;

    private BluetoothAdapter                     mAdapter            = BluetoothAdapter
                                                                             .getDefaultAdapter();
    private Acceptor                             mAcceptor;
    private ConcurrentHashMap<String, Neighbour> mNeighbours         = new ConcurrentHashMap<>();

    private BluetoothBroadcastReceiver           mStateReceiver      = new BluetoothBroadcastReceiver();

    private String                               mActiveServerAddress;

    private class BluetoothBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                case BluetoothAdapter.STATE_OFF: {
                    stopInner();
                }
                    break;

                case BluetoothAdapter.STATE_TURNING_OFF:
                    break;

                case BluetoothAdapter.STATE_ON:
                    startInner();
                    break;

                case BluetoothAdapter.STATE_TURNING_ON:
                    break;
                }
            }
        }
    }

    private class Neighbour {
        private final BluetoothSocket   mSocket;
        private final String            mBluetoothAddress;
        private final ConnectionAddress mAddress;
        private final Listener          mListener;
        private boolean                 mAlive;

        private class Listener implements Runnable {
            private static final String TAG = ":>Listener";

            private DataInputStream     mInStream;

            public Listener() {
                DataInputStream tmpInStream;
                try {
                    tmpInStream = new DataInputStream(mSocket.getInputStream());
                    mInStream = tmpInStream;
                    mAlive = true;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                while (mAlive) {
                    int packetSize;

                    try {
                        packetSize = mInStream.readInt();

                        if (packetSize > ConnectionClient.MSG_MAX_SIZE) {
                            Log.e(TAG, "run(): packetSize > MESSAGE_MAX_SIZE");
                            mInStream.skip(mInStream.available());
                            continue;
                        }

                    } catch (IOException e) {
                        Log.d(TAG, "run(): IOException 1");
                        e.printStackTrace();
                        if (mBluetoothAddress.equals(mActiveServerAddress)) {
                            sendHandshake(mAddress);
                        }
                        break;
                    }

                    byte[] data = new byte[packetSize];
                    try {
                        mInStream.readFully(data);

                    } catch (IOException e) {
                        Log.d(TAG, "run(): IOException 2");
                        e.printStackTrace();
                        if (mBluetoothAddress.equals(mActiveServerAddress)) {
                            sendHandshake(mAddress);
                        }
                        break;
                    }

                    sendDataHandlerMessage(data, mAddress);
                }

                if (mAlive) {
                    interrupt();
                }
            }

            // CODE SNIPPET FOR UNKNOWN DATA LENGTH
            //
            // private static final int BUFFER_LENGTH = 64;
            // private static final int LIMIT = ConnectionClient.MSG_MAX_SIZE -
            // BUFFER_LENGTH;
            //
            // @Override
            // public void run() {
            // int read = 0;
            // int length = 0;
            // ArrayList<byte[]> buffers = new ArrayList<byte[]>();
            //
            // while (true) {
            // byte[] buffer = new byte[BUFFER_LENGTH];
            // try {
            // read = mInStream.read(buffer);
            // if (read > 0) {
            // buffers.add(buffer);
            // length += read;
            // }
            //
            // if (read != BUFFER_LENGTH || mInStream.available() == 0 || length
            // >= LIMIT) {
            // ByteBuffer dataBuffer = ByteBuffer.allocate(length);
            // for (byte[] slice : buffers) {
            // int byteCount = length > BUFFER_LENGTH ? BUFFER_LENGTH : length;
            // dataBuffer.put(slice, 0, byteCount);
            //
            // length -= byteCount;
            // }
            //
            // byte[] data = dataBuffer.array();
            // sendDataHandlerMessage(data, mAddress);
            // Log.d(TAG, "Accepted message \"" + new String(data) + "\" from "
            // + mAddress);
            //
            // buffers.clear();
            // length = 0;
            // }
            //
            // } catch (IOException e) {
            // e.printStackTrace();
            // break;
            // }
            // }
            //
            // cancel();
            // }

            public void interrupt() {
                if (mAlive) {
                    mAlive = false;

                    if (mBluetoothAddress.equals(mActiveServerAddress)) {
                        mActiveServerAddress = null;
                    }

                    try {
                        mSocket.close();
                    } catch (IOException e) {
                    }
                }
            }
        }

        private class Sender implements Runnable {
            // private static final String TAG = ":>Sender";

            private DataOutputStream mOutStream;
            private byte[]           mPacket;

            public Sender(byte[] packet) {
                DataOutputStream tmpOut;
                try {
                    tmpOut = new DataOutputStream(mSocket.getOutputStream());
                    mOutStream = tmpOut;
                    mPacket = packet;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                if (mOutStream != null) {
                    try {
                        mOutStream.write(mPacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public Neighbour(BluetoothSocket socket) {
            mSocket = socket;
            mBluetoothAddress = socket.getRemoteDevice().getAddress();
            mAddress = new ConnectionAddress(mBluetoothAddress);
            mListener = new Listener();
        }

        public String getAddress() {
            return mBluetoothAddress;
        }

        // public boolean isAlive() {
        // return mAlive;
        // }

        public void listen() {
            new Thread(mListener).start();
        }

        public void send(byte[] data) {
            if (mAlive) {
                new Thread(new Sender(data)).start();
            }
        }

        public void release() {
            mListener.interrupt();
        }
    }

    private class Acceptor implements Runnable {
        private static final String   TAG = ":>Acceptor";

        private BluetoothServerSocket mServerSocket;
        private boolean               mAlive;

        public Acceptor() {
            if (mAdapter == null || !mAdapter.isEnabled()) {
                return;
            }

            BluetoothServerSocket tmpSocket;
            try {
                tmpSocket = mAdapter.listenUsingRfcommWithServiceRecord(mServiceName, mServiceUuid);
                mServerSocket = tmpSocket;
                mAlive = true;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (mAlive) {
                BluetoothSocket socket;
                try {
                    socket = mServerSocket.accept();

                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }

                if (socket != null) {
                    Neighbour neighbour = new Neighbour(socket);
                    Log.d(TAG, "Accepted connection from " + neighbour.getAddress());
                    Neighbour oldNeighbour = mNeighbours.put(neighbour.getAddress(), neighbour);
                    if (oldNeighbour != null) {
                        oldNeighbour.release();
                    }
                    neighbour.listen();
                }
            }

            if (mAlive) {
                interrupt();
            }
        }

        public boolean isAlive() {
            return mAlive;
        }

        public void interrupt() {
            if (mAlive) {
                mAlive = false;
                try {
                    mServerSocket.close();
                    mServerSocket = null;
                } catch (IOException e1) {
                }
            }
        }
    }

    private class Connector implements Runnable {
        private static final String TAG = ":>Connector";

        private BluetoothDevice     mDevice;

        private Connector(BluetoothDevice device) {
            mDevice = device;
        }

        @Override
        public void run() {
            BluetoothSocket socket;
            try {
                socket = mDevice.createRfcommSocketToServiceRecord(mServiceUuid);

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            mAdapter.cancelDiscovery();

            try {
                socket.connect();

            } catch (IOException e) {
                Log.d(TAG, "Connection with " + mDevice.getAddress() + " failed.");
                e.printStackTrace();

                try {
                    socket.close();
                } catch (IOException e1) {
                }
                return;
            }

            Neighbour neighbour = new Neighbour(socket);
            Log.d(TAG, "Established connection with " + neighbour.getAddress());
            Neighbour oldNeighbour = mNeighbours.put(neighbour.getAddress(), neighbour);
            if (oldNeighbour != null) {
                oldNeighbour.release();
            }
            neighbour.listen();
        }
    }

    ConnectionServerBluetooth(String name, ConnectionClient master, DataHandler dataHandler,
            StateHandler stateHandler) {
        super(name, master, dataHandler, stateHandler);
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerBroadcastReceiver(mStateReceiver, filter);
    }

    @Override
    String getServiceMagic() {
        return MAGIC_BLUETOOTH;
    }

    @Override
    Protocol getProtocol() {
        return Protocol.Bluetooth;
    }

    @Override
    void start(Bundle params) {
        Log.d(TAG, "start()");
        if (isStarted()) {
            return;
        }
        setStarted();

        mServiceUuid = (UUID) params.getSerializable(BTH_SEVICE_UUID_KEY);
        mServiceName = params.getString(BTH_SEVICE_NAME_KEY);

        if (mServiceUuid == null || mServiceName == null) {
            setStopped();
            return;
        }

        startInner();
    }

    @Override
    void stop() {
        Log.d(TAG, "stop()");
        stopInner();
        setStopped();
    }

    @Override
    boolean isActive() {
        return mAcceptor != null && mAcceptor.isAlive();
    }

    @Override
    void connectToHost(ConnectionAddress address) {
        Log.d(TAG, "connectToHost(): " + address.toString());
        mActiveServerAddress = address.getBluetoothAddress();
        sendServiceMessage(mActiveServerAddress, MSG_BTH_CONNECT);
    }

    @Override
    boolean disconnectFromHostImmediately(ConnectionAddress address) {
        Log.d(TAG, "disconnectFromHostImmediately(): " + address.toString());
        return false;
    }

    @Override
    void disconnectFromHost(ConnectionAddress address) {
        Log.d(TAG, "disconnectFromHost(): " + address.toString());
        if (address.getBluetoothAddress().equals(mActiveServerAddress)) {
            mActiveServerAddress = null;
        }
        sendServiceMessage(address.getBluetoothAddress(), MSG_BTH_DISCONNECT);
    }

    @Override
    void sendHandshake(ConnectionAddress address) {
        if (isActive()) {
            if (address == null) {
                Set<BluetoothDevice> devices = mAdapter.getBondedDevices();
                int cnt = 1;
                for (BluetoothDevice device : devices) {
                    String peerAddress = device.getAddress();
                    if (mNeighbours.containsKey(peerAddress)) {
                        Log.d(TAG, "sendHandshake(): to " + peerAddress);
                        send(getName().getBytes(), peerAddress, ConnectionClient.MSG_HANDSHAKE);

                    } else {
                        Log.d(TAG, "sendHandshake(): try to connect to " + peerAddress);
                        // It's a heavy weight operation. Let's give a chance to
                        // other threads
                        final BluetoothDevice deviceFinal = device;
                        new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                Connector connector = new Connector(deviceFinal);
                                new Thread(connector).start();
                            }
                        }, 3000 * cnt++);
                    }
                }

            } else {
                Log.d(TAG, "sendHandshake(): to " + address.getBluetoothAddress());
                send(getName().getBytes(), address.getBluetoothAddress(),
                        ConnectionClient.MSG_HANDSHAKE);
            }
        }
    }

    @Override
    void sendOnline(ConnectionAddress address, int interval) {
        byte[] ba = getName().getBytes();
        ByteBuffer buffer = ByteBuffer.allocate(ba.length + 4).putInt(interval).put(ba);

        send(buffer.array(), address.getBluetoothAddress(), ConnectionClient.MSG_ONLINE);
    }

    @Override
    void sendConnected() {
        Log.d(TAG, "sendConnected(): ");
        send(getName().getBytes(), mActiveServerAddress, ConnectionClient.MSG_CONNECT);
    }

    @Override
    boolean sendMessage(byte[] message) {
        if (mActiveServerAddress != null) {
            send(message, mActiveServerAddress, ConnectionClient.MSG_DATA);
            return true;
        }
        return false;
    }

    private void send(byte[] message, String address, int messageCode) {
        if (!isActive() || address == null) {
            return;
        }

        Neighbour neighbour = mNeighbours.get(address);
        if (neighbour == null) {
            return;
        }

        byte[] data = wrapMessage(message, messageCode, true);
        neighbour.send(data);
    }

    private void sendServiceMessage(String address, int messageCode) {
        if (!isActive() || address == null) {
            return;
        }

        Neighbour neighbour = mNeighbours.get(address);
        if (neighbour == null) {
            return;
        }

        byte[] data = wrapServiceMessage(messageCode, true);
        neighbour.send(data);
    }

    private void startInner() {
        Log.d(TAG, "startInner():");
        if (!isStarted() || isActive()) {
            return;
        }

        mAcceptor = new Acceptor();
        if (!mAcceptor.isAlive()) {
            mAcceptor = null;
            return;
        }

        new Thread(mAcceptor).start();

        sendStateHandlerMessage(StateHandler.STATE_ACTIVE);
    }

    private void stopInner() {
        Log.d(TAG, "stopInner()");
        if (!isStarted() || !isActive()) {
            return;
        }

        mAcceptor.interrupt();

        for (ConcurrentHashMap.Entry<String, Neighbour> entry : mNeighbours.entrySet()) {
            entry.getValue().release();
        }
        mNeighbours.clear();

        sendStateHandlerMessage(StateHandler.STATE_INACTIVE);
    }
}
