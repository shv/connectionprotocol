package ites.connection.protocol;

import java.util.Timer;
import java.util.TimerTask;

import android.util.Log;

/**
 * @author Administrator
 * 
 */
//@SuppressWarnings("unused")
class ServerListItem {
    private static final String TAG                        = ":>"
                                                                   + ServerListItem.class
                                                                           .getSimpleName();

    private static final int    MISSED_ONLINE_CHECKS_LIMIT = 10;

    private String              mName;
    private ConnectionAddress   mAddress;

    private ServerList          mParentList;
    private CheckOnlineTask     mCheckOnlineTask;
    private int                 mMissedOnlineChecks        = 0;
    private boolean             mDumb;

    private class CheckOnlineTask extends TimerTask {
        private static final String TAG = ":>CheckOnlineTask";

        public void run() {
            Log.d(TAG, "CheckOnlineTask.run(): mMissedOnlineChecks = " + mMissedOnlineChecks);
            if (mCheckOnlineTask != null) {
                if (mMissedOnlineChecks >= MISSED_ONLINE_CHECKS_LIMIT) {
                    mMissedOnlineChecks = 0;
                    mParentList.connectionLost(ServerListItem.this);

                } else {
                    mMissedOnlineChecks++;
                    mParentList.sendOnlineMessage(mAddress);
                }

                if (mMissedOnlineChecks == 3) {
                    mDumb = true;
                    mParentList.notifyChanged(ServerListItem.this);
                }
            }
        }
    }

    public ServerListItem(ServerList parent, String name, ConnectionAddress address) {
        mName = name;
        mAddress = address;

        mParentList = parent;
        mDumb = false;
    }

    void startOnlineCheck(Timer timer, int timeout) {
        Log.d(TAG, "startOnlineCheck() timeout = " + timeout);
        stopOnlineCheck(timer);

        mCheckOnlineTask = new CheckOnlineTask();
        timer.schedule(mCheckOnlineTask, 0, timeout);
    }

    void stopOnlineCheck(Timer timer, boolean purge) {
        Log.d(TAG, "stopOnlineCheck()");
        if (mCheckOnlineTask != null) {
            mCheckOnlineTask.cancel();
            mCheckOnlineTask = null;
            if (purge) {
                timer.purge();
            }
        }
    }

    void stopOnlineCheck(Timer timer) {
        stopOnlineCheck(timer, true);
    }

    void onlineAnswerReceived() {
        if (mDumb) {
            mDumb = false;
            mParentList.notifyChanged(this);
        }

        mMissedOnlineChecks = 0;
    }

    String getName() {
        return mName;
    }

    ConnectionAddress getAddress() {
        return mAddress;
    }

    public Protocol getProtocol() {
        return mAddress.getProtocol();
    }

    long getId() {
        return mAddress == null ? 0 : mAddress.getAddress();
    }

    boolean isDumb() {
        return mDumb;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new ServerListItem(this.mParentList, this.mName, this.mAddress);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof ServerListItem) || o.hashCode() != this.hashCode()) {
            return false;
        }

        ServerListItem sli = (ServerListItem) o;
        return (sli.mName == this.mName || sli.mName.equals(this.mName))
                && (sli.mAddress == this.mAddress || sli.mAddress.equals(this.mAddress));
    }

    @Override
    public int hashCode() {
        return mName.hashCode() ^ ((mAddress == null) ? 0 : mAddress.hashCode());
    }

    @Override
    public String toString() {
        return "" + mName + " (" + mAddress + ") " + super.toString();
    }
}
