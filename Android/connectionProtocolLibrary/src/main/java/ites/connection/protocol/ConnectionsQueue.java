package ites.connection.protocol;

import android.os.Handler;
import android.os.Looper;
//import android.os.Message;

class ConnectionsQueue extends Thread {
    @SuppressWarnings("unused")
    private static final String TAG = ":> " + ConnectionsQueue.class.getSimpleName();
    
    private Handler             mHandler;
    
    public Handler getHandler() {
        return mHandler;
    }

    @Override
    public void run() {
        Looper.prepare();

        mHandler = new Handler();
//        {
//            public void handleMessage(Message msg) {
//                // process incoming messages here
//            }
//        };

        Looper.loop();
    }
}
