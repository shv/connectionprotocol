package ites.connection.protocol;

import ites.connection.util.Inet;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.os.Parcel;
import android.os.Parcelable;

class ConnectionAddress implements Parcelable {
    @SuppressWarnings("UnusedDeclaration")
    private static final String TAG       = ":>" + ConnectionAddress.class.getSimpleName();

    static final long           BTH_MASK  = 0x4000000000000000L;

    private long                mFlat;
    private InetAddress         mIp;
    private String              mBluetooth;
    private Protocol            mProtocol = Protocol.Unknown;

    ConnectionAddress(InetAddress ipAddress) {
        if (ipAddress != null) {
            mIp = ipAddress;
            mFlat = (long) Inet.inet2int(ipAddress) & 0xffffffffL;

//            long l = 3232235520L;
//            int i = (int) l;
//            Log.e(TAG, "i (-1062731776) = " + i);
//            
//            int i2 = -1062731776;
//            long l2 = (long) i2 & 0xffffffffL;
//            Log.e(TAG, "l2 (3232235520L) = " + l2);

        } else {
            throw new RuntimeException("Invalid ip address: null");
        }

        mProtocol = Protocol.IpV4;
    }

//    ConnectionAddress(byte[] ipAddress) {
//        try {
//            mIp = InetAddress.getByAddress(ipAddress);
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//            throw new RuntimeException("Invalid ip address: " + e.getMessage());
//        }
//        
//        mFlat = Inet.inet2int(mIp);
//        mProtocol = Protocol.IpV4;
//    }

    ConnectionAddress(String btAddress) {
        String[] parts = btAddress.split(":");
        if (parts.length == 6) {
            long flatAddress = 0;
            for (String part : parts) {
                try {
                    flatAddress = (flatAddress << 8) | Integer.valueOf(part, 16);

                } catch (NumberFormatException e) {
                    throw new RuntimeException("Invalid bluetooth address. (" + btAddress + ")");
                }
            }
            mBluetooth = btAddress;
            mFlat = BTH_MASK | flatAddress;

        } else {
            throw new RuntimeException("Invalid bluetooth address. (" + btAddress + ")");
        }

        mProtocol = Protocol.Bluetooth;
    }

    ConnectionAddress(long address) {
        if ((address & BTH_MASK) == 0) {
            mIp = Inet.int2inet((int) address);
            mFlat = address;
            mProtocol = Protocol.IpV4;

        } else {
            String strAddress = "";
            long tmpAddress = address;
            for (int i = 0; i < 6; i++) {
                strAddress = String.format(":%02X", 0xFF & tmpAddress) + strAddress;
                tmpAddress = tmpAddress >> 8;
            }
            mBluetooth = strAddress.substring(1);
            mFlat = address;
            mProtocol = Protocol.Bluetooth;
        }
    }

    Protocol getProtocol() {
        return mProtocol;
    }

    long getAddress() {
        return mFlat;
    }

    InetAddress getIpAddress() {
        return mIp;
    }

    String getBluetoothAddress() {
        return mBluetooth;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new ConnectionAddress(this.getAddress());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof ConnectionAddress) || o.hashCode() != this.hashCode()) {
            return false;
        }

        ConnectionAddress sa = (ConnectionAddress) o;
        return (sa.mIp == this.mIp || sa.mIp.equals(this.mIp))
                && (sa.mBluetooth == this.mBluetooth || sa.mBluetooth.equals(this.mBluetooth));
    }

    @Override
    public int hashCode() {
        return mProtocol == Protocol.IpV4 ? mIp.hashCode() : mBluetooth.hashCode();
    }

    @Override
    public String toString() {
        return (mFlat & BTH_MASK) == 0 ? mIp.toString().replaceFirst("^/", "")
                .replaceAll("/[\\d\\D]+", "") : mBluetooth;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mFlat);
        dest.writeInt(mProtocol.getValue());

        switch (mProtocol) {
        case IpV4:
            dest.writeByteArray(mIp.getAddress());
            break;

        case Bluetooth:
            dest.writeString(mBluetooth);
            break;

        default:
            break;
        }
    }

    //@formatter:off
    public static final Parcelable.Creator<ConnectionAddress> CREATOR = 
        new Parcelable.Creator<ConnectionAddress>() {
            public ConnectionAddress createFromParcel(Parcel in) {
                return new ConnectionAddress(in);
            }

            public ConnectionAddress[] newArray(int size) {
                return new ConnectionAddress[size];
            }
        };
    //@formatter:off

    private ConnectionAddress(Parcel in) {
        mFlat = in.readLong();
        mProtocol = Protocol.fromInt(in.readInt());
        
        switch (mProtocol) {
        case IpV4:
            byte[] ipAddress = new byte[4];
            in.readByteArray(ipAddress);
            try {
                mIp = InetAddress.getByAddress(ipAddress);
            } catch (UnknownHostException e) {
            }
            break;
            
        case Bluetooth:
            mBluetooth = in.readString();
            break;
            
        default:
            break;
        }
    }
}
