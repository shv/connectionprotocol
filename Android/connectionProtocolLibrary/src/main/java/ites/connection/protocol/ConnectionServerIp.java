package ites.connection.protocol;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.text.TextUtils;
import android.util.Log;

import ites.connection.protocol.ConnectionClient.DataHandler;
import ites.connection.protocol.ConnectionClient.StateHandler;
import ites.connection.util.Inet;

class ConnectionServerIp extends ConnectionServer {
    private static final String   TAG             = ":>" + ConnectionServerIp.class.getSimpleName();

    static final String           IP_PORT_KEY     = "ip_port";
    static final String           OPTIONAL_IP_KEY = "optional_ip";

    static final int              UDP_MAX_SIZE    = 1024;

    static final String           MULTICAST_GROUP = "230.229.170.71";
    static final int              MULTICAST_TTL   = 4;

    private InetAddress           mLocalAddress;
    private InetAddress           mBroadcastAddress;
    private int                   mIpPort;
    private String                mOptionalIp;
    private InetAddress           mOptionalIpAddress;

    private DatagramSocket        mUdpSocket;

    private Thread                mUdpListenerThread;
    private Thread                mUdpBroadcastListenerThread;
    private Thread                mUdpMulticastListenerThread;

    private Socket                mTcpSocket;
    private Thread                mTcpListenerThread;
    private TcpConnector          mTcpConnector;

    private WifiBroadcastReceiver mStateReceiver;

    private class WifiBroadcastReceiver extends BroadcastReceiver {
        // private static final String TAG = ":>WifiBroadcastReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

            ConnectivityManager manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = manager.getActiveNetworkInfo();

            if (noConnectivity) {
                if (netInfo == null || netInfo.getType() != ConnectivityManager.TYPE_WIFI) {
                    stopInner();
                }
            } else {
                if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    startInner();
                }
            }
        }
    }

    private class UdpListener implements Runnable {
        private static final String       TAG     = ":>UdpListener";

        private DatagramSocket            mSocket;
        private ByteBuffer                mBuffer = ByteBuffer.allocate(UDP_MAX_SIZE);
        private WifiManager.MulticastLock mLock;

        public UdpListener(boolean broadcast) {
            Log.d(TAG, TAG + "(): " + (broadcast ? "broadcast" : "local"));

            if (mLocalAddress == null) {
                return;
            }

            WifiManager wfm = getWifiManager();
            if (wfm != null) {
                mLock = wfm.createMulticastLock(TAG);
                mLock.setReferenceCounted(true);
                mLock.acquire();
            }

            try {
                mSocket = new DatagramSocket(null);
                mSocket.setReuseAddress(true);
                mSocket.bind(broadcast ? new InetSocketAddress(mIpPort) : new InetSocketAddress(
                        mLocalAddress, mIpPort));

            } catch (SocketException e) {
                Log.d(TAG, TAG + "(): SocketException");
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (mSocket != null && mSocket.isBound() && !Thread.currentThread().isInterrupted()) {
                Log.d(TAG, "run(): " + mSocket.getLocalAddress().toString());

                mBuffer.clear();
                DatagramPacket packet = new DatagramPacket(mBuffer.array(), mBuffer.capacity());
                try {
                    mSocket.receive(packet);

                } catch (IOException e) {
                    Log.d(TAG, "run(): IOException");
                    e.printStackTrace();
                    continue;
                }

                if (Thread.currentThread().isInterrupted()) {
                    return;
                }

                byte[] data = new byte[packet.getLength()];
                mBuffer.get(data);

                sendDataHandlerMessage(data, new ConnectionAddress(packet.getAddress()));
            }

            if (mSocket != null) {
                mSocket.close();
            }

            if (mLock != null) {
                mLock.release();
            }
        }
    }

    private class MulticastListener implements Runnable {
        private static final String       TAG     = ":>MulticastListener";

        private MulticastSocket           mSocket;
        private ByteBuffer                mBuffer = ByteBuffer.allocate(UDP_MAX_SIZE);
        private WifiManager.MulticastLock mLock;

        public MulticastListener() {
            Log.d(TAG, TAG + "(): ");

            if (mLocalAddress == null) {
                return;
            }

            WifiManager wfm = getWifiManager();
            if (wfm != null) {
                mLock = wfm.createMulticastLock(TAG);
                mLock.setReferenceCounted(true);
                mLock.acquire();
            }

            try {
                mSocket = new MulticastSocket(mIpPort);
                mSocket.setReuseAddress(true);
                mSocket.setTimeToLive(MULTICAST_TTL);
                mSocket.joinGroup(InetAddress.getByName(MULTICAST_GROUP));

            } catch (IOException e) {
                Log.d(TAG, TAG + "(): SocketException");
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (mSocket != null && mSocket.isBound() && !Thread.currentThread().isInterrupted()) {
                // Log.d(TAG, "run(): " + mSocket.getLocalAddress().toString());

                mBuffer.clear();
                DatagramPacket packet = new DatagramPacket(mBuffer.array(), mBuffer.capacity());
                try {
                    mSocket.receive(packet);

                } catch (IOException e) {
                    Log.d(TAG, "run(): IOException");
                    e.printStackTrace();
                    continue;
                }

                if (Thread.currentThread().isInterrupted()) {
                    return;
                }

                byte[] data = new byte[packet.getLength()];
                mBuffer.get(data);

                Log.d(TAG, "run(): received from " + packet.getAddress().toString());
                sendDataHandlerMessage(data, new ConnectionAddress(packet.getAddress()));
            }

            if (mSocket != null) {
                try {
                    mSocket.leaveGroup(mLocalAddress);
                    mSocket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (mLock != null) {
                mLock.release();
            }
        }
    }

    private class UdpSender implements Runnable {
        private static final String TAG = ":>UdpSender";
        DatagramPacket              mPacket;

        public UdpSender(DatagramPacket packet) {
            Log.d(TAG, TAG + "()");
            mPacket = packet;
        }

        public void run() {
            try {
                mUdpSocket.send(mPacket);

            } catch (IOException e) {
                Log.d(TAG, "run(): IOException");
                e.printStackTrace();
            }
        }
    }

    private class TcpConnector implements Runnable {
        private static final String TAG          = ":>TcpConnector";

        private static final int    MAX_ATTEMPTS = 3;

        private InetAddress         mAddress;
        private boolean             mInterrupted = false;
        private int                 mAttempt;

        public TcpConnector(InetAddress hostAddress) {
            Log.d(TAG, TAG + "(): address = " + hostAddress.toString());
            Log.d(TAG, TAG + "(): hashCode = " + hostAddress.hashCode());
            try {
                Log.d(TAG, TAG + "(): isReachable = " + hostAddress.isReachable(500));
            } catch (IOException e) {
                e.printStackTrace();
            }
            mAddress = hostAddress;
        }

        public void interrupt() {
            mInterrupted = true;
        }

        @Override
        public void run() {
            while (mTcpSocket == null && !mInterrupted && mAttempt < MAX_ATTEMPTS) {
                Socket tmpSocket;
                try {
                    tmpSocket = new Socket();
                    tmpSocket.setReuseAddress(true);
                    tmpSocket.setKeepAlive(true);
                    tmpSocket.setSendBufferSize(ConnectionClient.MSG_MAX_SIZE);
                    tmpSocket.setReceiveBufferSize(ConnectionClient.MSG_MAX_SIZE);
                    tmpSocket.bind(new InetSocketAddress(mLocalAddress, mIpPort));
                    Log.d(TAG, TAG + "(): mAddress = " + mAddress + ", mIpPort = " + mIpPort);
                    tmpSocket.connect(new InetSocketAddress(mAddress, mIpPort));

                    mTcpSocket = tmpSocket;

                    mTcpListenerThread = new Thread(new TcpListener());
                    mTcpListenerThread.start();

                } catch (SocketException e) {
                    Log.d(TAG, "run(): SocketException: " + e.getMessage());
                    e.printStackTrace();
                    ++mAttempt;
                    if (e.getMessage().contains("EADDRINUSE")) {
                        interrupt();
                    }

                } catch (IllegalArgumentException e) {
                    Log.d(TAG, "run(): IllegalArgumentException");
                    e.printStackTrace();
                    ++mAttempt;

                } catch (IOException e) {
                    Log.d(TAG, "run(): IOException");
                    e.printStackTrace();
                    ++mAttempt;
                }

                if (mTcpSocket == null) {
                    Log.d(TAG, "run(): failed to create socket");
                    try {
                        // TODO Autoconnect timeout
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        interrupt();
                    }
                }
            }

            if (mInterrupted || mAttempt == MAX_ATTEMPTS) {
                Log.d(TAG, "run(): " + (mInterrupted ? "interrupted" : "MAX_ATTEMPTS"));
                if (mTcpSocket != null) {
                    if (mTcpSocket.isConnected()) {
                        try {
                            mTcpSocket.close();
                        } catch (IOException ignore) {
                        }
                    }
                    mTcpSocket = null;
                }

                if (mAttempt == MAX_ATTEMPTS) {
                    notifyConnectionFailed();
                }
            }

            mTcpConnector = null;
            Log.d(TAG, "run() finished");
        }
    }

    private class TcpListener implements Runnable {
        private static final String TAG = ":>TcpListener";

        public TcpListener() {
            Log.d(TAG, TAG + "()");
            mTcpConnector = null;
        }

        @Override
        public void run() {
            if (mTcpSocket == null || !mTcpSocket.isConnected()) {
                return;
            }

            InetAddress serverAddress = mTcpSocket.getInetAddress();
            DataInputStream in;

            try {
                in = new DataInputStream(mTcpSocket.getInputStream());

            } catch (IOException e) {
                Log.d(TAG, "run(): IOException 1");
                e.printStackTrace();
                closeServerSocket();
                sendHandshakeNative(serverAddress);
                return;
            }

            while (mTcpSocket != null && !mTcpSocket.isClosed()
                    && !Thread.currentThread().isInterrupted()) {
                int packetSize;

                try {
                    packetSize = in.readInt();
                    Log.d(TAG, "run(): packetSize = " + packetSize);

                    if (packetSize > ConnectionClient.MSG_MAX_SIZE) {
                        Log.e(TAG, "run(): packetSize: MESSAGE_MAX_SIZE");
                        closeServerSocket();
                        sendHandshakeNative(serverAddress);
                        return;
                    }

                } catch (IOException e) {
                    Log.d(TAG, "run(): IOException 2");
                    e.printStackTrace();
                    if (!Thread.currentThread().isInterrupted()) {
                        closeServerSocket();
                        sendHandshakeNative(serverAddress);
                    }
                    return;
                }

                byte[] data = new byte[packetSize];
                try {
                    in.readFully(data);

                } catch (IOException e) {
                    Log.d(TAG, "run(): IOException 3");
                    e.printStackTrace();

                    if (!Thread.currentThread().isInterrupted()) {
                        closeServerSocket();
                        sendHandshakeNative(serverAddress);
                    }
                    return;
                }

                sendDataHandlerMessage(data, new ConnectionAddress(serverAddress));
            }
        }
    }

    /**
     * @author Administrator
     * 
     */
    private class TcpSender implements Runnable {
        private static final String TAG = ":>TcpSender";
        byte[]                      mPacket;

        public TcpSender(byte[] packet) {
            Log.d(TAG, TAG + "()");
            mPacket = packet;
        }

        public void run() {
            if (mTcpSocket == null) {
                return;
            }
            Log.d(TAG, "run(): " + new String(mPacket) + ", length = " + mPacket.length
                    + " send to " + mTcpSocket.getInetAddress().toString());

            try {
                DataOutputStream out = new DataOutputStream(mTcpSocket.getOutputStream());
                // TODO: flush size? Default 8192 bytes
                BufferedOutputStream stream = new BufferedOutputStream(out);

                stream.write(mPacket);
                stream.flush();

            } catch (IOException e) {
                Log.d(TAG, "run(): IOException");
                e.printStackTrace();
            }
        }
    }

    public ConnectionServerIp(String name, ConnectionClient master, DataHandler dataHandler,
            StateHandler stateHandler) {
        super(name, master, dataHandler, stateHandler);
        mStateReceiver = new WifiBroadcastReceiver();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerBroadcastReceiver(mStateReceiver, filter);
    }

    @Override
    Protocol getProtocol() {
        return Protocol.IpV4;
    }

    @Override
    void start(Bundle params) {
        Log.d(TAG, "start()");
        if (isStarted()) {
            return;
        }
        setStarted();

        mIpPort = params.getInt(IP_PORT_KEY);
        if (mIpPort == 0) {
            setStopped();
            return;
        }

        mOptionalIp = params.getString(OPTIONAL_IP_KEY);

        startInner();
    }

    @Override
    void stop() {
        Log.d(TAG, "stop()");
        mOptionalIpAddress = null;

        stopInner();
        setStopped();
    }

    @Override
    boolean isActive() {
        return mUdpSocket != null && mUdpSocket.isBound();
    }

    @Override
    void connectToHost(ConnectionAddress address) {
        Log.d(TAG, "connectToHost(): " + mTcpSocket);
        if (mTcpSocket != null) {
            if (!mTcpSocket.isClosed()) {
                return;
            }
            mTcpSocket = null;
        }

        if (mTcpConnector != null) {
            // if (mServers.getActiveServer() != null) {
            // mHostApp.inform(mHostApp.getResources().getString(
            // ites.connection.R.string.message_already_connecting,
            // mServers.getActiveServer().toString()));
            // }
            return;
        }

        Log.d(TAG, "connectToHost(): " + address.toString());
        mTcpConnector = new TcpConnector(address.getIpAddress());
        // postToConnectionQueue(mTcpConnector);
        mTcpConnector.run();
    }

    @Override
    boolean disconnectFromHostImmediately(ConnectionAddress address) {
        Log.d(TAG, "disconnectFromHostImmediately(): mTcpConnector = " + mTcpConnector);
        if (mTcpConnector != null) {
            mTcpConnector.interrupt();
            return true;
        }
        return false;
    }

    @Override
    void disconnectFromHost(ConnectionAddress address) {
        Log.d(TAG, "disconnectFromHost(): mTcpConnector = " + mTcpConnector);
        if (mTcpConnector != null) {
            mTcpConnector.interrupt();

        } else {
            closeServerSocket();
        }
    }

    @Override
    void sendHandshake(ConnectionAddress address) {
        if (isActive()) {
            if (address == null) {
                sendHandshakeNative(mBroadcastAddress);
                if (!TextUtils.isEmpty(mOptionalIp)) {
                    if (mOptionalIpAddress == null) {
                        try {
                            mOptionalIpAddress = InetAddress.getByName(mOptionalIp);
                        } catch (Exception e) {
//                        } catch (UnknownHostException | NetworkOnMainThreadException e) {
                            mOptionalIp = "";
                            e.printStackTrace();
                        }
                    }
                }

                if (mOptionalIpAddress != null) {
                    sendHandshakeNative(mOptionalIpAddress);
                }

            } else {
                sendHandshakeNative(address.getIpAddress());
            }
        }
    }

    private void sendHandshakeNative(InetAddress address) {
        Log.d(TAG, "sendHandshake(): " + address.toString());
        sendUdp(getName().getBytes(), address, ConnectionClient.MSG_HANDSHAKE);
    }

    @Override
    void sendOnline(ConnectionAddress address, int interval) {
        // Log.d(TAG, "sendOnline(): " + address.toString() + ", " + interval);
        byte[] ba = getName().getBytes();
        ByteBuffer buffer = ByteBuffer.allocate(ba.length + 4).putInt(interval).put(ba);

        sendUdp(buffer.array(), address.getIpAddress(), ConnectionClient.MSG_ONLINE);
    }

    @Override
    void sendConnected() {
        // Log.d(TAG, "sendConnected(): ");
        sendTcp(getName().getBytes(), ConnectionClient.MSG_CONNECT);
    }

    @Override
    boolean sendMessage(byte[] message) {
        if (mTcpSocket != null && mTcpSocket.isConnected()) {
            // Log.d(TAG, "sendMessage(): ");
            sendTcp(message, ConnectionClient.MSG_DATA);
            return true;
        }
        return false;
    }

    void startInner() {
        Log.d(TAG, "startInner()");
        if (!isStarted() || isActive()) {
            return;
        }

        DhcpInfo dhcp = getDhcpInfo();
        if (dhcp == null) {
            return;
        }
        mLocalAddress = Inet.int2inet(dhcp.ipAddress);
        mBroadcastAddress = Inet.int2inet(dhcp.ipAddress | ~dhcp.netmask);

        if (mLocalAddress == null || mBroadcastAddress == null) {
            return;
        }

        // Network> UDP
        DatagramSocket tmpSocket;
        try {
            tmpSocket = new DatagramSocket(null);
            tmpSocket.setReuseAddress(true);
            tmpSocket.bind(new InetSocketAddress(mLocalAddress, mIpPort));
            tmpSocket.setBroadcast(true);

        } catch (SocketException e) {
            Log.d(TAG, "start(): SocketException");
            e.printStackTrace();
            return;
        }

        mUdpSocket = tmpSocket;

        mUdpListenerThread = new Thread(new UdpListener(false));
        mUdpListenerThread.start();
        mUdpBroadcastListenerThread = new Thread(new UdpListener(true));
        mUdpBroadcastListenerThread.start();
        mUdpMulticastListenerThread = new Thread(new MulticastListener());
        mUdpMulticastListenerThread.start();

        sendStateHandlerMessage(StateHandler.STATE_ACTIVE);
    }

    void stopInner() {
        Log.d(TAG, "stopInner()");
        if (!isStarted() || !isActive()) {
            return;
        }

        if (mUdpSocket != null) {
            mUdpSocket.close();
            mUdpSocket = null;

            mUdpListenerThread.interrupt();
            mUdpListenerThread = null;
            mUdpBroadcastListenerThread.interrupt();
            mUdpBroadcastListenerThread = null;
            mUdpMulticastListenerThread.interrupt();
            mUdpMulticastListenerThread = null;
        }

        if (mTcpConnector != null) {
            mTcpConnector.interrupt();

        } else {
            closeServerSocket();
        }

        sendStateHandlerMessage(StateHandler.STATE_INACTIVE);
    }

    private void sendUdp(byte[] message, InetAddress address, int messageCode) {
        if (mUdpSocket == null || address == null) {
            return;
        }

        byte[] data = wrapMessage(message, messageCode, false);

        DatagramPacket packet = new DatagramPacket(data, data.length, address, mIpPort);

        new Thread(new UdpSender(packet)).start();
    }

    private void sendTcp(byte[] message, int messageCode) {
        if (mTcpSocket == null || !mTcpSocket.isConnected()) {
            return;
        }

        byte[] data = wrapMessage(message, messageCode, true);

        new Thread(new TcpSender(data)).start();
    }

    private void closeServerSocket() {
        if (mTcpSocket == null) {
            Log.d(TAG, "closeServerSocket(): mTcpSocket == null");
            return;

        } else if (!mTcpSocket.isConnected()) {
            Log.d(TAG, "closeServerSocket(): !mTcpSocket.isConnected()");
            mTcpSocket = null;
            return;
        }

        Log.d(TAG, "closeServerSocket(): " + mTcpSocket);
        Socket tcpSocket = mTcpSocket;

        mTcpSocket = null;
        if (mTcpListenerThread != null) {
            Log.d(TAG, "closeServerSocket(): mTcpListenerThread != null");
            mTcpListenerThread.interrupt();
        }
        mTcpListenerThread = null;

        try {
            if (tcpSocket != null) {
                Log.d(TAG, "closeServerSocket(): tcpSocket != null");
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                    tcpSocket.shutdownInput();
                    tcpSocket.shutdownOutput();
                }
                tcpSocket.close();
                Log.d(TAG,
                        "closeServerSocket(): tcpSocket.isConnected() = " + tcpSocket.isConnected());
                Log.d(TAG, "closeServerSocket(): tcpSocket.isBound() = " + tcpSocket.isBound());
                Log.d(TAG, "closeServerSocket(): tcpSocket.isClosed() = " + tcpSocket.isClosed());
            }

        } catch (IOException e) {
            Log.d(TAG, "closeServerSocket(): IOException");
            e.printStackTrace();
        }
        Log.d(TAG, "closeServerSocket(): finished");
    }
}
