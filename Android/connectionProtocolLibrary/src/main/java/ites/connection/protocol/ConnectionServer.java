package ites.connection.protocol;

import ites.connection.protocol.ConnectionClient.DataHandler;
import ites.connection.protocol.ConnectionClient.StateHandler;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

abstract class ConnectionServer {
    private static final String TAG = ":>" + ConnectionServer.class.getSimpleName();

    private String             mName;
    final private DataHandler  mDataHandler;
    final private StateHandler mStateHandler;
    private boolean            mStarted;
    private ConnectionClient   mMaster;

    ConnectionServer(String name, ConnectionClient master, DataHandler dataHandler,
            StateHandler stateHandler) {
        mName = name;
        mDataHandler = dataHandler;
        mStateHandler = stateHandler;
        mMaster = master;
    }

    String getName() {
        return mName;
    }
    
    String getServiceMagic() {
        return null;
    }

    abstract Protocol getProtocol();

    abstract void start(Bundle parameters);

    abstract void stop();

    abstract boolean isActive();

    abstract void connectToHost(ConnectionAddress address);

    abstract boolean disconnectFromHostImmediately(ConnectionAddress address);

    abstract void disconnectFromHost(ConnectionAddress address);

    abstract void sendHandshake(ConnectionAddress address);

    abstract void sendOnline(ConnectionAddress address, int interval);

    abstract void sendConnected();

    abstract boolean sendMessage(byte[] message);

    boolean isStarted() {
        return mStarted;
    }

    void setStarted() {
        mStarted = true;
    }

    void setStopped() {
        mStarted = false;
    }

    void notifyConnectionFailed() {
        mMaster.getServerList().connectionFailed();
    }

    void registerBroadcastReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        mMaster.registerBroadcastReceiver(receiver, filter);
    }

    byte[] wrapMessage(byte[] message, int messageCode, boolean prependSize) {
        return mMaster.wrapMessage(message, messageCode, prependSize);
    }

    byte[] wrapServiceMessage(int messageCode, boolean prependSize) {
        String magic = getServiceMagic();
        if (magic != null && magic.length() == ConnectionClient.MAGIC_LENGTH) {
            return mMaster.wrapServiceMessage(magic, messageCode, prependSize);
        }
        return null;
    }
    
    DhcpInfo getDhcpInfo() {
        return mMaster.getDhcpInfo();
    }

    WifiManager getWifiManager() {
        return mMaster.getWifiManager();
    }

    void postToConnectionQueue(Runnable operation) {
        mMaster.postToConnectionQueue(operation);
    }

    void sendDataHandlerMessage(byte[] data, ConnectionAddress address) {
        Log.d(TAG, "sendDataHandlerMessage(): from " + address.toString());
        
        Bundle bundle = new Bundle();
        bundle.putByteArray(DataHandler.DATA_KEY, data);
        bundle.putParcelable(DataHandler.ADDRESS_KEY, address);

        Message message = mDataHandler.obtainMessage();
        message.setData(bundle);
        mDataHandler.sendMessage(message);
    }

    void sendStateHandlerMessage(int state) {
        Message message = mStateHandler.obtainMessage(state);
        message.arg1 = getProtocol().getValue();
        mStateHandler.sendMessage(message);
    }
}
