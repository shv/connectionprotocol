package ites.connection.util;

import java.util.EnumMap;
import java.util.Map;

import ites.connection.R;
import ites.connection.interfaces.OnServerListChangeListener.ServerState;
import ites.connection.protocol.Protocol;

public class ServerStateIcon {
    private Map<Protocol, int[]>          mIcons;
    private Map<ServerState, Integer> mIndex;

    public ServerStateIcon() {
        mIndex = new EnumMap<>(ServerState.class);
        mIndex.put(ServerState.Active, 0);
        mIndex.put(ServerState.Inactive, 1);
        mIndex.put(ServerState.Connecting, 2);
        mIndex.put(ServerState.Dumb, 3);

        mIcons = new EnumMap<>(Protocol.class);
        mIcons.put(Protocol.IpV4,
                new int[] { R.drawable.ic_wifi_active, R.drawable.ic_wifi_inactive,
                        R.drawable.ic_wifi_connecting, R.drawable.ic_wifi_dumb });

        mIcons.put(Protocol.Bluetooth, new int[] { R.drawable.ic_bluetooth_active,
                R.drawable.ic_bluetooth_inactive, R.drawable.ic_bluetooth_connecting,
                R.drawable.ic_bluetooth_dumb });

        mIcons.put(Protocol.Unknown, new int[] { 0, 0, 0, 0 });
    }

    public int getIconRes(ServerState state, Protocol protocol) {
        if (protocol == Protocol.Unknown) {
            return R.drawable.ic_no_connections;
        }

        return mIcons.get(protocol)[mIndex.get(state)];
    }
}
