package ites.connection.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class Inet {
    public static int inet2int(InetAddress address) {
        return address == null ? 0 : Integer.reverseBytes(ByteBuffer.wrap(address.getAddress())
                .getInt());
    }

    public static InetAddress int2inet(int flat) {
        InetAddress address = null;

        try {
            byte[] bytes = ByteBuffer.allocate(4).putInt(Integer.reverseBytes(flat)).array();
            address = InetAddress.getByAddress(bytes);
        } catch (UnknownHostException | NullPointerException e) {
            e.printStackTrace();
        }

        return address;
    }
}
