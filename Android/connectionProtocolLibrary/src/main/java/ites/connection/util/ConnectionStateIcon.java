package ites.connection.util;

import java.util.EnumMap;
import java.util.Map;

import ites.connection.R;
import ites.connection.interfaces.OnConnectionStateListener.ConnectionState;
import ites.connection.protocol.Protocol;

public class ConnectionStateIcon {
    private Map<Protocol, int[]>          mIcons;
    private Map<ConnectionState, Integer> mIndex;

    public ConnectionStateIcon() {
        mIndex = new EnumMap<>(ConnectionState.class);
        mIndex.put(ConnectionState.Active, 0);
        mIndex.put(ConnectionState.Inactive, 1);
        mIndex.put(ConnectionState.Connecting, 2);
        mIndex.put(ConnectionState.Dumb, 3);

        mIcons = new EnumMap<>(Protocol.class);
        mIcons.put(Protocol.IpV4,
                new int[] { R.drawable.ic_wifi_active, R.drawable.ic_wifi_inactive,
                        R.drawable.ic_wifi_connecting, R.drawable.ic_wifi_dumb });

        mIcons.put(Protocol.Bluetooth, new int[] { R.drawable.ic_bluetooth_active,
                R.drawable.ic_bluetooth_inactive, R.drawable.ic_bluetooth_connecting,
                R.drawable.ic_bluetooth_dumb });

        mIcons.put(Protocol.Unknown, new int[] { 0, 0, 0, 0 });
    }

    public int getIconRes(ConnectionState state, Protocol protocol) {
        if (state == ConnectionState.Unconnected || protocol == Protocol.Unknown) {
            return R.drawable.ic_no_connections;
        }

        return mIcons.get(protocol)[mIndex.get(state)];
    }
}
