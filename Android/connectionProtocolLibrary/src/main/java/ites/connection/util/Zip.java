package ites.connection.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public final class Zip {
    public static byte[] compress(byte[] data) throws IOException {
        if (data.length == 0) {
            return new byte[0];
        }
        
        Deflater deflater = new Deflater();
        deflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length / 2);

        ByteBuffer length = ByteBuffer.allocate(4);
        length.putInt(data.length);
        outputStream.write(length.array());
        
        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer); // returns the generated code... index
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();

        deflater.end();

        return output;
    }

    public static byte[] extract(byte[] data) throws IOException, DataFormatException {
        ByteBuffer wrap = ByteBuffer.wrap(data);
        int length = wrap.getInt();
        if (length == 0) {
            return new byte[0];
        }
        
        byte[] zipped = new byte[wrap.remaining()];
        wrap.get(zipped);
        
        Inflater inflater = new Inflater();
        inflater.setInput(zipped);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(length);
        byte[] buffer = new byte[1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();

        inflater.end();

        return output;
    }
}
