package ites.connection.interfaces;

public interface OnActiveServerChangeListener {
    public void onActiveServerChanged(long id, String name);
}
