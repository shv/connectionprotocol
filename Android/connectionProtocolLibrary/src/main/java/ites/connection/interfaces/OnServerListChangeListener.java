package ites.connection.interfaces;

import android.os.Bundle;

import java.util.ArrayList;

import ites.connection.protocol.Protocol;

public interface OnServerListChangeListener {
    enum ServerState {
        Connecting, Active, Dumb, Inactive
    }

    public static class ServerInfo {
        public String      name;
        public String      address;
        public long        id;
        public Protocol    protocol;
        public boolean     isActive;
        public ServerState state;
    }

    public void onServerListChanged(ArrayList<ServerInfo> serverList);
}
