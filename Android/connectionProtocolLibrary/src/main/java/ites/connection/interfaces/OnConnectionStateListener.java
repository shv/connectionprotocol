package ites.connection.interfaces;

import ites.connection.protocol.Protocol;

public interface OnConnectionStateListener {
    enum ConnectionState {
        Unconnected, Connecting, Active, Dumb, Inactive
    }

    public void onConnectionStateChanged(ConnectionState state, Protocol protocol);
}
