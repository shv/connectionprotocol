package ites.connection.interfaces;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;

//import android.content.res.Resources;

public interface ConnectionHost {

    /**
     * @return the DHCP-assigned addresses from the last successful DHCP
     *         request, if any or <b>null</b>.
     */
    DhcpInfo getDhcpInfo();

    /**
     * @return the system Wi-Fi service, if any or <b>null</b>.
     */
    WifiManager getWifiManager();

    /**
     * Submit received data to host app.
     * 
     * @param data
     */
    void onNetworkMessageReceived(byte[] data);

    /**
     * Registers broadcast receiver with host app.
     * 
     * @param receiver
     * @param filter
     */
    void registerBroadcastReceiver(BroadcastReceiver receiver, IntentFilter filter);

    /**
     * Indicates whether host app has visible activities.
     * 
     * @return
     */
    boolean isInForeground();

    void removeActiveServer(long serverId);

    // /**
    // * Inform host app about events, errors, etc.
    // * @param message
    // */
    // void inform(String message);
    //
    // /**
    // * Gets application resources.
    // */
    // Resources getResources();

    void runOnUiThread(Runnable runnable);
}
