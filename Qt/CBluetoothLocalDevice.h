#ifndef CBLUETOOTHLOCALDEVICE_H
#define CBLUETOOTHLOCALDEVICE_H

#include <QObject>
#include <QList>
#include <QMutex>

#include "CBluetoothAddress.h"
#include "CBluetoothHostInfo.h"

#ifndef _INC_WINDOWS
#include <windows.h>
#endif

class CBluetoothLocalDevice : public QObject
{
    Q_OBJECT
public:
    explicit CBluetoothLocalDevice(QObject* parent = 0);
    CBluetoothLocalDevice(const CBluetoothAddress& address, QObject* parent = 0);
    virtual ~CBluetoothLocalDevice();

    enum Error {
        NoError,
        PairingError,
        UnknownError = 100
    };

    enum HostMode {
        HostPoweredOff,
        HostConnectable,
        HostDiscoverable,
        HostDiscoverableLimitedInquiry
    };

    enum Pairing {
        Unpaired,
        Paired,
        AuthorizedPaired
    };

    static QList<CBluetoothHostInfo> allDevices();

    CBluetoothAddress address() const {return m_Info.address();}
    QString           name()    const {return m_Info.name();}

    HostMode hostMode() const;
    bool     isValid()  const {return m_hRadio != NULL;}

    QList<CBluetoothAddress> connectedDevices() const;
//    QList<CBluetoothAddress> pairedDevices()    const; // Non Qt
//    Pairing pairingStatus(const CBluetoothAddress& address) const;

//    void powerOn();
//    void requestPairing(const CBluetoothAddress & address, Pairing pairing);
//    void setHostMode(CBluetoothLocalDevice::HostMode mode);

signals:
    void deviceConnected(   const CBluetoothAddress& address);
    void deviceDisconnected(const CBluetoothAddress& address);
    void error(CBluetoothLocalDevice::Error error);
    void hostModeStateChanged(CBluetoothLocalDevice::HostMode state);
//    void pairingDisplayConfirmation(const CBluetoothAddress& address, QString pin);
//    void pairingDisplayPinCode(const CBluetoothAddress& address, QString pin);
//    void pairingFinished(const CBluetoothAddress& address, CBluetoothLocalDevice::Pairing pairing);

public slots:
//    void pairingConfirmation(bool accept);

private:
    void interfaceArrival();
    void interfaceRemove();

    void* m_hRadio;

    bool               m_bIsDefaultAdapter;
    CBluetoothHostInfo m_Info;

private:
    static void* findDefaultAdapter(CBluetoothHostInfo* pInfo);
    static void* findAdapter(const CBluetoothAddress& address, CBluetoothHostInfo* pInfo);

    static LRESULT CALLBACK winMessagesHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    static void initWinListener( CBluetoothLocalDevice* pInstance);
    static void closeWinListener(CBluetoothLocalDevice* pInstance);

    static HWND                          s_hWndListener;
    static QList<CBluetoothLocalDevice*> s_lsInstances;
    static QMutex                        s_Mutex;
};

#endif // CBLUETOOTHLOCALDEVICE_H
