#ifndef CBLUETOOTHDEVICEINFO_H
#define CBLUETOOTHDEVICEINFO_H

#include <QList>
#include <QUuid>

#include "CBluetoothAddress.h"

class CBluetoothDeviceInfo
{
public:
    CBluetoothDeviceInfo();
    CBluetoothDeviceInfo(const CBluetoothAddress& address, const QString& name);
    CBluetoothDeviceInfo(const CBluetoothDeviceInfo& other);
    ~CBluetoothDeviceInfo() {}

    enum DataCompleteness {
        DataComplete,
        DataIncomplete,
        DataUnavailable
    };

    CBluetoothAddress address() const {return m_Address;}
    QString           name()    const {return m_qsName;}

    bool isValid() const {return !m_Address.isNull();}

//    QList<QUuid> serviceUuids(DataCompleteness* completeness = 0) const;

private:
    QString           m_qsName;
    CBluetoothAddress m_Address;
};

#endif // CBLUETOOTHDEVICEINFO_H
