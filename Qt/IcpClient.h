#ifndef ICPCLIENT_H
#define ICPCLIENT_H

#include <QObject>

#include "IcpAddress.h"
#include "IcpSocket.h"
#include "IcpServer.h"

namespace icp {

class IcpClient : public QObject
{
    Q_OBJECT
public:
    explicit IcpClient(const QString& qsName, IcpSocket* pSocket, IcpServer* pServer, QObject *parent = 0);
    virtual ~IcpClient() {delete m_pSocket;}
//    virtual ~IcpClient() {m_pSocket->abort(); delete m_pSocket;}

public:
    const QString&    name()    const {return m_qsName;}
    const IcpAddress& address() const {return m_pSocket->peerAddress();}
    IcpSocket*        socket()        {return m_pSocket;}

    IcpAddress::Protocol protocol() const {return m_pSocket->protocol();}

    void sendData(const QByteArray& baData) {m_pServer->sendData(m_pSocket, baData);}
    void sendGoodbye()                      {m_pServer->sendGoodbye(address());}

    void startTimeoutTimer(int iTimeOut);

private:
    void timerEvent(QTimerEvent* /*event*/) {m_pSocket->abort();}

    QString    m_qsName;
    IcpSocket* m_pSocket;
    IcpServer* m_pServer;

    int m_iTimerId;
};

}

#endif // ICPCLIENT_H
