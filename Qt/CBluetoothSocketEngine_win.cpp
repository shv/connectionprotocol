#include "CBluetoothSocketEngine_p.h"

#include "CBluetoothSocket_p.h"

#include "private/qthread_p.h"
#include <QAbstractEventDispatcher>
#include <QSocketNotifier>
#include <QEvent>

#include <initguid.h>
#include <winsock2.h>
#include <ws2bth.h>

// Common constructs
#define Q_CHECK_VALID_SOCKETLAYER(function, returnValue) do { \
    if (!isValid()) { \
        qWarning(""#function" was called on an uninitialized socket device"); \
        return returnValue; \
    } } while (0)
//#define Q_CHECK_INVALID_SOCKETLAYER(function, returnValue) do { \
//    if (isValid()) { \
//        qWarning(""#function" was called on an already initialized socket device"); \
//        return returnValue; \
//    } } while (0)
#define Q_CHECK_STATE(function, checkState, returnValue) do { \
    if (d->socketState != (checkState)) { \
        qWarning(""#function" was not called in "#checkState); \
        return (returnValue); \
    } } while (0)
#define Q_CHECK_NOT_STATE(function, checkState, returnValue) do { \
    if (d->socketState == (checkState)) { \
        qWarning(""#function" was called in "#checkState); \
        return (returnValue); \
    } } while (0)
#define Q_CHECK_STATES(function, state1, state2, returnValue) do { \
    if (d->socketState != (state1) && d->socketState != (state2)) { \
        qWarning(""#function" was called" \
                 " not in "#state1" or "#state2); \
        return (returnValue); \
    } } while (0)
//#define Q_CHECK_TYPE(function, type, returnValue) do { \
//    if (d->socketType != (type)) { \
//        qWarning(#function" was called by a" \
//                 " socket other than "#type""); \
//        return (returnValue); \
//    } } while (0)

static const QUuid NULL_UUID = QUuid();

#if defined(QNATIVESOCKETENGINE_DEBUG)

void verboseWSErrorDebug(int r)
{
    switch (r) {
        case WSANOTINITIALISED : qDebug("WSA error : WSANOTINITIALISED"); break;
        case WSAEINTR: qDebug("WSA error : WSAEINTR"); break;
        case WSAEBADF: qDebug("WSA error : WSAEBADF"); break;
        case WSAEACCES: qDebug("WSA error : WSAEACCES"); break;
        case WSAEFAULT: qDebug("WSA error : WSAEFAULT"); break;
        case WSAEINVAL: qDebug("WSA error : WSAEINVAL"); break;
        case WSAEMFILE: qDebug("WSA error : WSAEMFILE"); break;
        case WSAEWOULDBLOCK: qDebug("WSA error : WSAEWOULDBLOCK"); break;
        case WSAEINPROGRESS: qDebug("WSA error : WSAEINPROGRESS"); break;
        case WSAEALREADY: qDebug("WSA error : WSAEALREADY"); break;
        case WSAENOTSOCK: qDebug("WSA error : WSAENOTSOCK"); break;
        case WSAEDESTADDRREQ: qDebug("WSA error : WSAEDESTADDRREQ"); break;
        case WSAEMSGSIZE: qDebug("WSA error : WSAEMSGSIZE"); break;
        case WSAEPROTOTYPE: qDebug("WSA error : WSAEPROTOTYPE"); break;
        case WSAENOPROTOOPT: qDebug("WSA error : WSAENOPROTOOPT"); break;
        case WSAEPROTONOSUPPORT: qDebug("WSA error : WSAEPROTONOSUPPORT"); break;
        case WSAESOCKTNOSUPPORT: qDebug("WSA error : WSAESOCKTNOSUPPORT"); break;
        case WSAEOPNOTSUPP: qDebug("WSA error : WSAEOPNOTSUPP"); break;
        case WSAEPFNOSUPPORT: qDebug("WSA error : WSAEPFNOSUPPORT"); break;
        case WSAEAFNOSUPPORT: qDebug("WSA error : WSAEAFNOSUPPORT"); break;
        case WSAEADDRINUSE: qDebug("WSA error : WSAEADDRINUSE"); break;
        case WSAEADDRNOTAVAIL: qDebug("WSA error : WSAEADDRNOTAVAIL"); break;
        case WSAENETDOWN: qDebug("WSA error : WSAENETDOWN"); break;
        case WSAENETUNREACH: qDebug("WSA error : WSAENETUNREACH"); break;
        case WSAENETRESET: qDebug("WSA error : WSAENETRESET"); break;
        case WSAECONNABORTED: qDebug("WSA error : WSAECONNABORTED"); break;
        case WSAECONNRESET: qDebug("WSA error : WSAECONNRESET"); break;
        case WSAENOBUFS: qDebug("WSA error : WSAENOBUFS"); break;
        case WSAEISCONN: qDebug("WSA error : WSAEISCONN"); break;
        case WSAENOTCONN: qDebug("WSA error : WSAENOTCONN"); break;
        case WSAESHUTDOWN: qDebug("WSA error : WSAESHUTDOWN"); break;
        case WSAETOOMANYREFS: qDebug("WSA error : WSAETOOMANYREFS"); break;
        case WSAETIMEDOUT: qDebug("WSA error : WSAETIMEDOUT"); break;
        case WSAECONNREFUSED: qDebug("WSA error : WSAECONNREFUSED"); break;
        case WSAELOOP: qDebug("WSA error : WSAELOOP"); break;
        case WSAENAMETOOLONG: qDebug("WSA error : WSAENAMETOOLONG"); break;
        case WSAEHOSTDOWN: qDebug("WSA error : WSAEHOSTDOWN"); break;
        case WSAEHOSTUNREACH: qDebug("WSA error : WSAEHOSTUNREACH"); break;
        case WSAENOTEMPTY: qDebug("WSA error : WSAENOTEMPTY"); break;
        case WSAEPROCLIM: qDebug("WSA error : WSAEPROCLIM"); break;
        case WSAEUSERS: qDebug("WSA error : WSAEUSERS"); break;
        case WSAEDQUOT: qDebug("WSA error : WSAEDQUOT"); break;
        case WSAESTALE: qDebug("WSA error : WSAESTALE"); break;
        case WSAEREMOTE: qDebug("WSA error : WSAEREMOTE"); break;
        case WSAEDISCON: qDebug("WSA error : WSAEDISCON"); break;
        default: qDebug("WSA error : Unknown"); break;
    }
    qErrnoWarning(r, "more details");
}

/*
    Returns a human readable representation of the first \a len
    characters in \a data.
*/
static QByteArray qt_prettyDebug(const char *data, int len, int maxLength)
{
    if (!data) return "(null)";
    QByteArray out;
    for (int i = 0; i < len; ++i) {
        char c = data[i];
        if (isprint(int(uchar(c)))) {
            out += c;
        } else switch (c) {
        case '\n': out += "\\n"; break;
        case '\r': out += "\\r"; break;
        case '\t': out += "\\t"; break;
        default:
            QString tmp;
            tmp.sprintf("\\%o", c);
            out += tmp.toLatin1().constData();
        }
    }

    if (len < maxLength)
        out += "...";

    return out;
}


#define WS_ERROR_DEBUG(x) verboseWSErrorDebug(x);
#else
#define WS_ERROR_DEBUG(x) Q_UNUSED(x)
#endif

///----------------------------------------------------------------------------
/// CWindowsSockInit
///----------------------------------------------------------------------------
class CWindowsSockInit
{
public:
    CWindowsSockInit();
    ~CWindowsSockInit();
    int version;
};

///----------------------------------------------------------------------------
CWindowsSockInit::CWindowsSockInit() :
    version(0)
{
    qDebug() << "CWindowsSockInit::CWindowsSockInit()";
    WSADATA WSAData = {0};
    int iResult = ::WSAStartup(MAKEWORD(2, 2), &WSAData);
    if (iResult != 0) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
    } else {
        version = 0x22;
    }
}

///----------------------------------------------------------------------------
CWindowsSockInit::~CWindowsSockInit()
{
    qDebug() << "CWindowsSockInit::~CWindowsSockInit()";
    ::WSACleanup();
}


///----------------------------------------------------------------------------
/// CBluetoothSocketEnginePrivate
///----------------------------------------------------------------------------
class CBluetoothSocketEnginePrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(CBluetoothSocketEngine)
public:
    CBluetoothSocketEnginePrivate();
    virtual ~CBluetoothSocketEnginePrivate();

    mutable CBluetoothSocketEngine::Error socketError;
    mutable QString                       socketErrorString;
    mutable bool                          hasSetSocketError;
    CBluetoothSocket::SocketState         socketState;
    CBluetoothServiceInfo::Protocol       socketProtocol;
    QUuid                                 serviceId;
    QString                               serviceName;
    CBluetoothAddress                     localAddress;
    quint32                               localPort;
    CBluetoothAddress                     peerAddress;
    quint32                               peerPort;
    QString                               peerName;
    CAbstractSocketEngineReceiver*        receiver;

    qintptr socketDescriptor;

    QSocketNotifier *readNotifier, *writeNotifier, *exceptNotifier;

    CWindowsSockInit winSock;

    enum ErrorString {
        NonBlockingInitFailedErrorString,
//        BroadcastingInitFailedErrorString,
        RemoteHostClosedErrorString,
//        TimeOutErrorString,
        ResourceErrorString,
//        OperationUnsupportedErrorString,
        ProtocolUnsupportedErrorString,
        InvalidSocketErrorString,
        HostUnreachableErrorString,
        NetworkUnreachableErrorString,
        AccessErrorString,
        ConnectionTimeOutErrorString,
        ConnectionRefusedErrorString,
        AddressInUseErrorString,
        AddressNotAvailableErrorString,
//        AddressProtectedErrorString,
//        DatagramTooLargeErrorString,
//        SendDatagramErrorString,
//        ReceiveDatagramErrorString,
        WriteErrorString,
//        ReadErrorString,
        PortInuseErrorString,
        NotSocketErrorString,
//        InvalidProxyTypeString,
        TemporaryErrorString,

        UnknownSocketErrorString = -1
    };

    void setError(CBluetoothSocketEngine::Error error, ErrorString errorString) const;

//    // native functions
//    int option(CBluetoothSocketEngine::SocketOption option) const;
    bool setOption(CBluetoothSocketEngine::SocketOption option, int value);

    bool createNewSocket(CBluetoothServiceInfo::Protocol &protocol);

    bool nativeConnect(const CBluetoothAddress &address, const QUuid& serviceId);
    bool nativeBind();
    bool nativeRegisterService(const QUuid& serviceId, const QString& serviceName);
    bool nativeListen(int backlog);
    qintptr nativeAccept();
    qint64 nativeBytesAvailable() const;

//    bool nativeHasPendingDatagrams() const;
//    qint64 nativePendingDatagramSize() const;
//    qint64 nativeReceiveDatagram(char *data, qint64 maxLength,
//                                     CBluetoothAddress *address, quint16 *port);
//    qint64 nativeSendDatagram(const char *data, qint64 length,
//                                  const CBluetoothAddress &host, quint16 port);
    qint64 nativeRead(char *data, qint64 maxLength);
    qint64 nativeWrite(const char *data, qint64 length);
//    int nativeSelect(int timeout, bool selectForRead) const;
//    int nativeSelect(int timeout, bool checkRead, bool checkWrite,
//             bool *selectForRead, bool *selectForWrite) const;

    void nativeClose();

    bool fetchConnectionParameters();
};

///----------------------------------------------------------------------------
///
/// On Windows, WSAStartup is called "recursively" for every
/// concurrent CBluetoothSocketEngine. This is safe, because WSAStartup and
/// WSACleanup are reference counted.
///
CBluetoothSocketEnginePrivate::CBluetoothSocketEnginePrivate() :
    socketError(CBluetoothSocketEngine::UnknownError),
    socketErrorString(""),
    hasSetSocketError(false),
    socketDescriptor(INVALID_SOCKET),
    socketState(CBluetoothSocket::UnconnectedState),
    socketProtocol(CBluetoothServiceInfo::UnknownProtocol),
    serviceId(NULL_UUID),
    localPort(0),
    peerPort(0),
    receiver(NULL),
    readNotifier(NULL),
    writeNotifier(NULL),
    exceptNotifier(NULL)
{
}

///----------------------------------------------------------------------------
CBluetoothSocketEnginePrivate::~CBluetoothSocketEnginePrivate()
{
}

///----------------------------------------------------------------------------
void CBluetoothSocketEnginePrivate::setError(CBluetoothSocketEngine::Error error, CBluetoothSocketEnginePrivate::ErrorString errorString) const
{
    if (hasSetSocketError) {
        // Only set socket errors once for one engine; expect the
        // socket to recreate its engine after an error.
        return;
    }
    socketError = error;

    switch (errorString) {
    case NonBlockingInitFailedErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Unable to initialize non-blocking socket");
        break;
//    case BroadcastingInitFailedErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Unable to initialize broadcast socket");
//        break;
//    // should not happen anymore
    case RemoteHostClosedErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("The remote host closed the connection");
        break;
//    case TimeOutErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Network operation timed out");
//        break;
    case ResourceErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Out of resources");
        break;
//    case OperationUnsupportedErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Unsupported socket operation");
//        break;
    case ProtocolUnsupportedErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Protocol type not supported");
        break;
    case InvalidSocketErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Invalid socket descriptor");
        break;
    case HostUnreachableErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Host unreachable");
        break;
    case NetworkUnreachableErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Network unreachable");
        break;
    case AccessErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Permission denied");
        break;
    case ConnectionTimeOutErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Connection timed out");
        break;
    case ConnectionRefusedErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Connection refused");
        break;
    case AddressInUseErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("The bound address is already in use");
        break;
    case AddressNotAvailableErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("The address is not available");
        break;
//    case AddressProtectedErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("The address is protected");
//        break;
//    case DatagramTooLargeErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Datagram was too large to send");
//        break;
//    case SendDatagramErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Unable to send a message");
//        break;
//    case ReceiveDatagramErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Unable to receive a message");
//        break;
    case WriteErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Unable to write");
        break;
//    case ReadErrorString:
//        socketErrorString = CBluetoothSocketEngine::tr("Network error");
//        break;
    case PortInuseErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Another socket is already listening on the same port");
        break;
    case NotSocketErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Operation on non-socket");
        break;
//    case InvalidProxyTypeString:
//        socketErrorString = CBluetoothSocketEngine::tr("The proxy type is invalid for this operation");
//        break;
    case TemporaryErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Temporary error");
        break;
    case UnknownSocketErrorString:
        socketErrorString = CBluetoothSocketEngine::tr("Unknown error");
        break;
    }
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::setOption(CBluetoothSocketEngine::SocketOption option, int value)
{
    Q_Q(const CBluetoothSocketEngine);
    if (!q->isValid())
        return false;

    int n = 0;
    int level = SOL_SOCKET; // default

    switch (option) {
    case CBluetoothSocketEngine::ReceiveBufferSocketOption:
        n = SO_RCVBUF;
        break;
    case CBluetoothSocketEngine::SendBufferSocketOption:
        n = SO_SNDBUF;
        break;
    case CBluetoothSocketEngine::NonBlockingSocketOption:
    {
        unsigned long buf = value;
        unsigned long outBuf;
        DWORD sizeWritten = 0;
        if (::WSAIoctl(socketDescriptor, FIONBIO, &buf, sizeof(unsigned long), &outBuf, sizeof(unsigned long), &sizeWritten, 0,0) == SOCKET_ERROR) {
            WS_ERROR_DEBUG(::WSAGetLastError());
            return false;
        }
        return true;
        break;
    }
    case CBluetoothSocketEngine::AddressReusable:
        n = SO_REUSEADDR;
        break;
    case CBluetoothSocketEngine::BindExclusively:
        n = SO_EXCLUSIVEADDRUSE;
        break;
    case CBluetoothSocketEngine::ReceiveOutOfBandData:
        n = SO_OOBINLINE;
        break;
    case CBluetoothSocketEngine::KeepAliveOption:
        n = SO_KEEPALIVE;
        break;
    }

    if (::setsockopt(socketDescriptor, level, n, (char*)&value, sizeof(value)) != 0) {
        WS_ERROR_DEBUG(::WSAGetLastError());
        return false;
    }
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::createNewSocket(CBluetoothServiceInfo::Protocol& protocol)
{
    qDebug() << "CBluetoothSocketEnginePrivate::createNewSocket():";
    int bthProtocol = -1;
    switch (protocol) {
    case CBluetoothServiceInfo::RfcommProtocol:
        bthProtocol = BTHPROTO_RFCOMM;
        break;
    case CBluetoothServiceInfo::L2capProtocol:
        bthProtocol = BTHPROTO_L2CAP;
        break;
    default:
        return false;
    }

//    SOCKET socket = ::socket(AF_BTH, SOCK_STREAM, bthProtocol);
    int af   = AF_BTH;
    int type = SOCK_STREAM;

#ifndef WSA_FLAG_NO_HANDLE_INHERIT
#define WSA_FLAG_NO_HANDLE_INHERIT 0x80
#endif
    QSysInfo::WinVersion osver = QSysInfo::windowsVersion();

    SOCKET socket = INVALID_SOCKET;
    // Windows 7 or later, try the new API
    if ((osver & QSysInfo::WV_NT_based) >= QSysInfo::WV_6_1) {
        // this call fails if the windows 7 service pack 1 or hot fix isn't installed.
        socket = ::WSASocket(af, type, bthProtocol, NULL, 0, WSA_FLAG_NO_HANDLE_INHERIT | WSA_FLAG_OVERLAPPED);
    }

    // Try the old API if the new one failed on Windows 7, or always on earlier versions
    if (socket == INVALID_SOCKET && ((osver & QSysInfo::WV_NT_based) <= QSysInfo::WV_6_1)) {
        socket = ::WSASocket(af, type, bthProtocol, NULL, 0, WSA_FLAG_OVERLAPPED);
#ifdef HANDLE_FLAG_INHERIT
        if (socket != INVALID_SOCKET) {
            // make non inheritable the old way
            ::SetHandleInformation((HANDLE)socket, HANDLE_FLAG_INHERIT, 0);
        }
#endif
    }

    if (socket == INVALID_SOCKET) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
        switch (err) {
        case WSANOTINITIALISED:
            //###
            break;
        case WSAEAFNOSUPPORT:
        case WSAESOCKTNOSUPPORT:
        case WSAEPROTOTYPE:
        case WSAEINVAL:
            setError(CBluetoothSocketEngine::UnsupportedProtocolError, ProtocolUnsupportedErrorString);
            break;
        case WSAEMFILE:
        case WSAENOBUFS:
            setError(CBluetoothSocketEngine::UnknownError, ResourceErrorString);
            break;
        default:
            break;
        }

        return false;

    } else if (QAbstractEventDispatcher::instance()) {
//        // Because of WSAAsyncSelect() WSAAccept returns a non blocking socket
//        // with the same attributes as the listening socket including the current
//        // WSAAsyncSelect(). To be able to change the socket to blocking mode the
//        // WSAAsyncSelect() call must be cancled.
//        QSocketNotifier n(socket, QSocketNotifier::Write);
//        n.setEnabled(true);
//        n.setEnabled(false);
//        // TODO:
    }

    socketDescriptor = socket;
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::nativeConnect(const CBluetoothAddress& address, const QUuid& serviceId)
{
    SOCKADDR_BTH sockAddrBthRemote = {0};
    sockAddrBthRemote.addressFamily  = AF_BTH;
    sockAddrBthRemote.btAddr         = address.toUInt64();
    sockAddrBthRemote.serviceClassId =(GUID) serviceId;

    forever {
        int connectResult = ::WSAConnect(socketDescriptor, (struct sockaddr *) &sockAddrBthRemote, sizeof(SOCKADDR_BTH), 0,0,0,0);
        if (connectResult = SOCKET_ERROR) {
            int err = ::WSAGetLastError();
            WS_ERROR_DEBUG(err);

            switch (err) {
            case WSANOTINITIALISED:
                //###
                break;
            case WSAEISCONN:
                socketState = CBluetoothSocket::ConnectedState;
                break;
            case WSAEWOULDBLOCK: {
                // If WSAConnect returns WSAEWOULDBLOCK on the second
                // connection attempt, we have to check SO_ERROR's
                // value to detect ECONNREFUSED. If we don't get
                // ECONNREFUSED, we'll have to treat it as an
                // unfinished operation.
                int  value         = 0;
                int  valueSize     = sizeof(value);
                bool tryAgain      = false;
                bool errorDetected = false;
                int  tries         = 0;
                do {
                    if (::getsockopt(socketDescriptor, SOL_SOCKET, SO_ERROR, (char *) &value, &valueSize) == 0) {
                        if (value == WSAECONNREFUSED) {
                            setError(CBluetoothSocketEngine::UnknownError, ConnectionRefusedErrorString);
                            socketState = CBluetoothSocket::UnconnectedState;
                            errorDetected = true;
                            break;
                        }
                        if (value == WSAETIMEDOUT) {
                            setError(CBluetoothSocketEngine::NetworkError, ConnectionTimeOutErrorString);
                            socketState = CBluetoothSocket::UnconnectedState;
                            errorDetected = true;
                            break;
                        }
                        if (value == WSAEHOSTUNREACH) {
                            setError(CBluetoothSocketEngine::NetworkError, HostUnreachableErrorString);
                            socketState = CBluetoothSocket::UnconnectedState;
                            errorDetected = true;
                            break;
                        }
                        if (value == WSAEADDRNOTAVAIL) {
                            setError(CBluetoothSocketEngine::NetworkError, AddressNotAvailableErrorString);
                            socketState = CBluetoothSocket::UnconnectedState;
                            errorDetected = true;
                            break;
                        }
                        if (value == NOERROR) {
                            // When we get WSAEWOULDBLOCK the outcome was not known, so a
                            // NOERROR might indicate that the result of the operation
                            // is still unknown. We try again to increase the chance that we did
                            // get the correct result.
                            tryAgain = !tryAgain;
                        }
                    }
                    tries++;
                } while (tryAgain && (tries < 2));

                if (errorDetected) {
                    break;
                }
                // fall through
            }
            case WSAEINPROGRESS:
                setError(CBluetoothSocketEngine::NoSocketError, InvalidSocketErrorString);
                socketState = CBluetoothSocket::ConnectingState;
                break;
            case WSAEACCES:
                setError(CBluetoothSocketEngine::OperationError, AccessErrorString);
                socketState = CBluetoothSocket::UnconnectedState;
                break;
            case WSAENOBUFS:
                setError(CBluetoothSocketEngine::InputOutputError, ResourceErrorString);
                break;
            case WSAEADDRINUSE:
                setError(CBluetoothSocketEngine::NetworkError, AddressInUseErrorString);
                break;
            case WSAECONNREFUSED:
                setError(CBluetoothSocketEngine::UnknownError, ConnectionRefusedErrorString);
                socketState = CBluetoothSocket::UnconnectedState;
                break;
            case WSAETIMEDOUT:
                setError(CBluetoothSocketEngine::NetworkError, ConnectionTimeOutErrorString);
                break;
            case WSAEDISCON:
            case WSAECONNRESET:
            case WSAECONNABORTED:
            case WSAESHUTDOWN:
                setError(CBluetoothSocketEngine::NetworkError, RemoteHostClosedErrorString);
                break;
            case WSAENETUNREACH:
                setError(CBluetoothSocketEngine::NetworkError, NetworkUnreachableErrorString);
                socketState = CBluetoothSocket::UnconnectedState;
                break;
            case WSAEHOSTDOWN:
            case WSAENETDOWN:
                setError(CBluetoothSocketEngine::PoweredOffError, NetworkUnreachableErrorString);
                break;
            case WSAEADDRNOTAVAIL:
                setError(CBluetoothSocketEngine::NetworkError, AddressNotAvailableErrorString);
                socketState = CBluetoothSocket::UnconnectedState;
                break;
            case WSAEINVAL:
                setError(CBluetoothSocketEngine::OperationError, InvalidSocketErrorString);
                break;
            default:
                break;
            }

            if (socketState != CBluetoothSocket::ConnectedState) {
                return false;
            }
        }
        break;
    }

    qDebug() << "CBluetoothSocketEnginePrivate::nativeConnect(): connected";
    socketState = CBluetoothSocket::ConnectedState;
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::nativeBind()
{
    SOCKADDR_BTH sockAddrBthLocal = {0};
    sockAddrBthLocal.addressFamily = AF_BTH;
    sockAddrBthLocal.port = BT_PORT_ANY;

    int iAddrSize = sizeof(SOCKADDR_BTH);
    if (::bind(socketDescriptor, (struct sockaddr*) &sockAddrBthLocal, iAddrSize) == SOCKET_ERROR) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
        switch (err) {
        case WSANOTINITIALISED:
            //###
            break;
        default:
            setError(CBluetoothSocketEngine::UnknownError, UnknownSocketErrorString);
            break;
        }

        return false;
    }

    socketState = CBluetoothSocket::BoundState;
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::nativeRegisterService(const QUuid& serviceId, const QString& serviceName)
{
    SOCKADDR_BTH sockAddrBthLocal = {0};
    sockAddrBthLocal.addressFamily = AF_BTH;
    sockAddrBthLocal.btAddr        = localAddress.toUInt64();
    sockAddrBthLocal.port          = localPort;

    CSADDR_INFO csAddrInfo = {0};
    csAddrInfo.LocalAddr.iSockaddrLength  = sizeof(SOCKADDR_BTH);
    csAddrInfo.LocalAddr.lpSockaddr       = (LPSOCKADDR) &sockAddrBthLocal;
    csAddrInfo.RemoteAddr.iSockaddrLength = sizeof(SOCKADDR_BTH);
    csAddrInfo.RemoteAddr.lpSockaddr      = (LPSOCKADDR) &sockAddrBthLocal;
    csAddrInfo.iSocketType                = SOCK_STREAM;
    csAddrInfo.iProtocol                  = BTHPROTO_RFCOMM;

    WSAQUERYSET wsaQuerySet = {0};
    wsaQuerySet.dwSize                  = sizeof(WSAQUERYSET);
    wsaQuerySet.lpServiceClassId        = (LPGUID) &serviceId;
    wsaQuerySet.lpszServiceInstanceName = (LPWSTR) serviceName.toUtf8().data();
    wsaQuerySet.lpszComment             = L"";
    wsaQuerySet.dwNameSpace             = NS_BTH;
    wsaQuerySet.dwNumberOfCsAddrs       = 1;                           // Must be 1.
    wsaQuerySet.lpcsaBuffer             = (LPCSADDR_INFO) &csAddrInfo; // Req'd.

    if (::WSASetService(&wsaQuerySet, RNRSERVICE_REGISTER, 0) == SOCKET_ERROR) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
        switch (err) {
        case WSANOTINITIALISED:
            //###
            break;
        default:
            setError(CBluetoothSocketEngine::ServiceAlreadyRegisteredError, UnknownSocketErrorString);
            break;
        }

        return false;
    }

    this->serviceId = serviceId;
    this->serviceName = serviceName;
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::nativeListen(int backlog)
{
    if (::listen(socketDescriptor, backlog) == SOCKET_ERROR) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
        switch (err) {
        case WSANOTINITIALISED:
            //###
            break;
        case WSAEADDRINUSE:
            setError(CBluetoothSocketEngine::InputOutputError, PortInuseErrorString);
            break;
        default:
            break;
        }

        return false;
    }

    socketState = CBluetoothSocket::ListeningState;
    return true;
}

///----------------------------------------------------------------------------
qintptr CBluetoothSocketEnginePrivate::nativeAccept()
{
    qDebug() << "CBluetoothSocketEnginePrivate::nativeAccept():";
    qintptr acceptedDescriptor = ::accept(socketDescriptor, NULL, NULL);
    if (acceptedDescriptor == INVALID_SOCKET) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
        qDebug() << "CBluetoothSocketEnginePrivate::nativeAccept(): Error";
        switch (err) {
        case WSANOTINITIALISED:
            //###
            break;
        case WSAEACCES:
            setError(CBluetoothSocketEngine::OperationError, AccessErrorString);
            break;
        case WSAECONNREFUSED:
            setError(CBluetoothSocketEngine::OperationError, ConnectionRefusedErrorString);
            break;
        case WSAECONNRESET:
            setError(CBluetoothSocketEngine::NetworkError, RemoteHostClosedErrorString);
            break;
        case WSAENETDOWN:
            setError(CBluetoothSocketEngine::PoweredOffError, NetworkUnreachableErrorString);
        case WSAENOTSOCK:
            setError(CBluetoothSocketEngine::UnknownError, NotSocketErrorString);
            break;
        case WSAEINVAL:
        case WSAEOPNOTSUPP:
            setError(CBluetoothSocketEngine::UnsupportedProtocolError, ProtocolUnsupportedErrorString);
            break;
        case WSAEFAULT:
        case WSAEMFILE:
        case WSAENOBUFS:
            setError(CBluetoothSocketEngine::UnknownError, ResourceErrorString);
            break;
        case WSAEWOULDBLOCK:
            setError(CBluetoothSocketEngine::TemporaryError, TemporaryErrorString);
            break;
        default:
            setError(CBluetoothSocketEngine::UnknownError, UnknownSocketErrorString);
            break;
        }

    } else if (QAbstractEventDispatcher::instance()) {
        // Because of WSAAsyncSelect() WSAAccept returns a non blocking socket
        // with the same attributes as the listening socket including the current
        // WSAAsyncSelect(). To be able to change the socket to blocking mode the
        // WSAAsyncSelect() call must be cancled.
        QSocketNotifier n(acceptedDescriptor, QSocketNotifier::Read);
        n.setEnabled(true);
        n.setEnabled(false);
        qDebug() << "CBluetoothSocketEnginePrivate::nativeAccept(): Ok";
    }

    return acceptedDescriptor;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEnginePrivate::nativeBytesAvailable() const
{
    unsigned long nbytes      = 0;
    unsigned long dummy       = 0;
    DWORD         sizeWritten = 0;
    if (::WSAIoctl(socketDescriptor, FIONREAD, &dummy, sizeof(dummy), &nbytes, sizeof(nbytes), &sizeWritten, 0,0) == SOCKET_ERROR) {
        WS_ERROR_DEBUG(::WSAGetLastError());
        return -1;
    }

///    // ioctlsocket sometimes reports 1 byte available for datagrams
///    // while the following recvfrom returns -1 and claims connection
///    // was reset (udp is connectionless). so we peek one byte to
///    // catch this case and return 0 bytes available if recvfrom
///    // fails.
///    if (nbytes == 1 && socketType == QAbstractSocket::UdpSocket) {
///        char c;
///        WSABUF buf;
///        buf.buf = &c;
///        buf.len = sizeof(c);
///        DWORD flags = MSG_PEEK;
///        if (::WSARecvFrom(socketDescriptor, &buf, 1, 0, &flags, 0,0,0,0) == SOCKET_ERROR)
///            return 0;
///    }
    return nbytes;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEnginePrivate::nativeRead(char* data, qint64 maxLength)
{
    qint64 ret = -1;

    WSABUF buf;
    buf.buf = data;
    buf.len = maxLength;

    DWORD bytesRead = 0;
    DWORD flags     = 0;

    if (::WSARecv(socketDescriptor, &buf, 1, &bytesRead, &flags, 0,0) == SOCKET_ERROR) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);
        switch (err) {
        case WSAEWOULDBLOCK:
            ret = -2;
            break;
        case WSAEBADF:
        case WSAEINVAL:
            //error string is set in read(), not here in nativeRead()
            break;
        case WSAECONNRESET:
        case WSAECONNABORTED:
            // this will be handled in CBluetoothSocketEngine::read
            ret = 0;
            break;
        default:
            break;
        }

    } else {
        if (::WSAGetLastError() == WSAEWOULDBLOCK) {
            ret = -2;
        } else {
            ret = qint64(bytesRead);
        }
    }

    return ret;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEnginePrivate::nativeWrite(const char* data, qint64 length)
{
    Q_Q(CBluetoothSocketEngine);
    qint64 ret = 0;
    qint64 bytesToSend = length;

    for (;;) {
        WSABUF buf;
        buf.buf = (char*)data + ret;
        buf.len = bytesToSend;
        DWORD flags = 0;
        DWORD bytesWritten = 0;

        int socketRet = ::WSASend(socketDescriptor, &buf, 1, &bytesWritten, flags, 0,0);

        ret += qint64(bytesWritten);

        int err;
        if (socketRet != SOCKET_ERROR) {
            if (ret == length) {
                break;
            } else {
                continue;
            }

        } else if ((err = ::WSAGetLastError()) == WSAEWOULDBLOCK) {
            break;
        } else if (err == WSAENOBUFS) {
            // this function used to not send more than 49152 per call to WSASendTo
            // to avoid getting a WSAENOBUFS. However this is a performance regression
            // and we think it only appears with old windows versions. We now handle the
            // WSAENOBUFS and hope it never appears anyway.
            // just go on, the next loop run we will try a smaller number
        } else {
            WS_ERROR_DEBUG(err);
            switch (err) {
            case WSAECONNRESET:
            case WSAECONNABORTED:
                ret = -1;
                setError(CBluetoothSocketEngine::NetworkError, WriteErrorString);
                q->close();
                break;
            default:
                break;
            }
            break;
        }

        // for next send:
        bytesToSend = qMin<qint64>(49152, length - ret);
    }

    return ret;
}

///----------------------------------------------------------------------------
void CBluetoothSocketEnginePrivate::nativeClose()
{
    qDebug() << "CBluetoothSocketEnginePrivate::nativeClose()";
    ::closesocket(socketDescriptor);
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEnginePrivate::fetchConnectionParameters()
{
    localPort = 0;
    localAddress.clear();
    peerPort = 0;
    peerAddress.clear();

    if (socketDescriptor == INVALID_SOCKET) {
       return false;
    }

    // Determine local address
    SOCKADDR_BTH sockAddrBthLocal = {0};
    int iAddrSize = sizeof(SOCKADDR_BTH);
    if (::getsockname(socketDescriptor, (struct sockaddr*) &sockAddrBthLocal, &iAddrSize) == SOCKET_ERROR) {
        int err = ::WSAGetLastError();
        WS_ERROR_DEBUG(err);

        if (err == WSAENOTSOCK) {
            setError(CBluetoothSocketEngine::OperationError, InvalidSocketErrorString);
            return false;
        } else {
            setError(CBluetoothSocketEngine::UnknownError, UnknownSocketErrorString);
        }

    } else {
        localAddress = CBluetoothAddress(sockAddrBthLocal.btAddr);
        localPort = sockAddrBthLocal.port;
    }

    // Determine peer address
    SOCKADDR_BTH sockAddrBthPeer = {0};
    if (::getpeername(socketDescriptor, (struct sockaddr*) &sockAddrBthPeer, &iAddrSize) == SOCKET_ERROR) {
//        WS_ERROR_DEBUG(::WSAGetLastError());

    } else {
        peerAddress = CBluetoothAddress(sockAddrBthPeer.btAddr);
        peerPort = sockAddrBthPeer.port;
    }

    socketProtocol = CBluetoothServiceInfo::UnknownProtocol;
    WSAPROTOCOL_INFO info = {0};
    int iValueSize = sizeof(WSAPROTOCOL_INFO);
    if (::getsockopt(socketDescriptor, SOL_SOCKET, SO_PROTOCOL_INFO, (char*) &info, &iValueSize) == SOCKET_ERROR) {
        WS_ERROR_DEBUG(::WSAGetLastError());

    } else {
        if (info.iProtocol = BTHPROTO_RFCOMM) {
            socketProtocol = CBluetoothServiceInfo::RfcommProtocol;
        } else if (info.iProtocol = BTHPROTO_L2CAP) {
            socketProtocol = CBluetoothServiceInfo::L2capProtocol;
        }
    }

    return true;
}


///----------------------------------------------------------------------------
/// CReadNotifier
///----------------------------------------------------------------------------
class CReadNotifier : public QSocketNotifier
{
public:
    CReadNotifier(qintptr fd, CBluetoothSocketEngine *parent)
        : QSocketNotifier(fd, QSocketNotifier::Read, parent)
    { engine = parent; }

protected:
    bool event(QEvent *);

    CBluetoothSocketEngine *engine;
};

///----------------------------------------------------------------------------
bool CReadNotifier::event(QEvent *e)
{
    if (e->type() == QEvent::SockAct) {
        engine->readNotification();
        return true;
    } else if (e->type() == QEvent::SockClose) {
        engine->closeNotification();
        return true;
    }
    return QSocketNotifier::event(e);
}

///----------------------------------------------------------------------------
/// CWriteNotifier
///----------------------------------------------------------------------------
class CWriteNotifier : public QSocketNotifier
{
public:
    CWriteNotifier(int fd, CBluetoothSocketEngine *parent)
        : QSocketNotifier(fd, QSocketNotifier::Write, parent) { engine = parent; }

protected:
    bool event(QEvent *);

    CBluetoothSocketEngine *engine;
};

///----------------------------------------------------------------------------
bool CWriteNotifier::event(QEvent *e)
{
    if (e->type() == QEvent::SockAct) {
        if (engine->state() == CBluetoothSocket::ConnectingState) {
            engine->connectionNotification();
        } else {
            engine->writeNotification();
        }
        return true;
    }
    return QSocketNotifier::event(e);
}

///----------------------------------------------------------------------------
/// CExceptionNotifier
///----------------------------------------------------------------------------
class CExceptionNotifier : public QSocketNotifier
{
public:
    CExceptionNotifier(int fd, CBluetoothSocketEngine *parent)
        : QSocketNotifier(fd, QSocketNotifier::Exception, parent) { engine = parent; }

protected:
    bool event(QEvent *);

    CBluetoothSocketEngine *engine;
};

///----------------------------------------------------------------------------
bool CExceptionNotifier::event(QEvent *e)
{
    if (e->type() == QEvent::SockAct) {
        if (engine->state() == CBluetoothSocket::ConnectingState) {
            engine->connectionNotification();
        } else {
            engine->exceptionNotification();
        }
        return true;
    }
    return QSocketNotifier::event(e);
}

///----------------------------------------------------------------------------
/// CBluetoothSocketEngine
///----------------------------------------------------------------------------
CBluetoothSocketEngine::CBluetoothSocketEngine(QObject *parent) :
    QObject(*new CBluetoothSocketEnginePrivate(), parent)
{
}

///----------------------------------------------------------------------------
CBluetoothSocketEngine::~CBluetoothSocketEngine()
{
    close();
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::initialize(CBluetoothServiceInfo::Protocol protocol)
{
    qDebug() << "CBluetoothSocketEngine::initialize";
    Q_D(CBluetoothSocketEngine);
    if (isValid()) {
        close();
    }

    // Create the socket
    if (!d->createNewSocket(protocol)) {
        return false;
    }

    // Make the socket nonblocking.
    if (!setOption(NonBlockingSocketOption, 1)) {
        d->setError(CBluetoothSocketEngine::OperationError, CBluetoothSocketEnginePrivate::NonBlockingInitFailedErrorString);
        close();
        return false;
    }

    d->socketProtocol = protocol;
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::initialize(qintptr socketDescriptor)
{
    Q_D(CBluetoothSocketEngine);

    if (isValid()) {
        close();
    }

    d->socketDescriptor = socketDescriptor;

    // determine socket type and protocol
    if (!d->fetchConnectionParameters()) {
        d->socketDescriptor = INVALID_SOCKET;
        qDebug() << "CBluetoothSocketEngine::initialize(): Error 1";
        return false;
    }

    if (d->socketProtocol != CBluetoothServiceInfo::UnknownProtocol) {
        // Make the socket nonblocking.
        if (!setOption(NonBlockingSocketOption, 1)) {
            d->setError(CBluetoothSocketEngine::OperationError, CBluetoothSocketEnginePrivate::NonBlockingInitFailedErrorString);
            close();
            qDebug() << "CBluetoothSocketEngine::initialize(): Error 2";
            return false;
        }
    }

    d->socketState = CBluetoothSocket::ConnectedState;
    return true;
}

///----------------------------------------------------------------------------
qintptr CBluetoothSocketEngine::socketDescriptor() const
{
    return d_func()->socketDescriptor;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::isValid() const
{
    Q_D(const CBluetoothSocketEngine);
    return d->socketDescriptor != INVALID_SOCKET;
}

bool CBluetoothSocketEngine::connectToHost(const CBluetoothAddress& address, const QUuid& serviceId)
{
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::connectToHost(), false);

    Q_CHECK_STATES(CBluetoothSocketEngine::connectToHost(),
                   CBluetoothSocket::UnconnectedState, CBluetoothSocket::ConnectingState, false);

    d->peerAddress = address;
    bool connected = d->nativeConnect(address, serviceId);
    if (connected) {
        d->fetchConnectionParameters();
    }

    return connected;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::bind()
{
    qDebug() << "CBluetoothSocketEngine::bind";
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::bind(), false);
    Q_CHECK_STATE(CBluetoothSocketEngine::bind(), CBluetoothSocket::UnconnectedState, false);

    if (!d->nativeBind()) {
        return false;
    }

    d->fetchConnectionParameters();
    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::registerService(const QUuid& serviceId, const QString& serviceName)
{
    qDebug() << "CBluetoothSocketEngine::registerService";
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::registerService(), false);
    Q_CHECK_STATE(CBluetoothSocketEngine::registerService(), CBluetoothSocket::BoundState, false);

    if (!d->nativeRegisterService(serviceId, serviceName)) {
        return false;
    }

    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::listen()
{
    qDebug() << "CBluetoothSocketEngine::listen";
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::listen(), false);
    Q_CHECK_STATE(CBluetoothSocketEngine::listen(), CBluetoothSocket::BoundState, false);

    // When calling the listen function, it is strongly recommended that a low value is used
    // for the backlog parameter (typically 2 to 4), since only a few client connections are
    // accepted. This reduces the system resources that are allocated for
    // use by the listening socket.
    // https://msdn.microsoft.com/en-us/library/windows/desktop/aa362906%28v=vs.85%29.aspx
    return d->nativeListen(4);
}

///----------------------------------------------------------------------------
qintptr CBluetoothSocketEngine::accept()
{
    qDebug() << "CBluetoothSocketEngine::accept";
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::accept(), INVALID_SOCKET);
    Q_CHECK_STATE(CBluetoothSocketEngine::accept(), CBluetoothSocket::ListeningState, -1);

    return d->nativeAccept();
}

///----------------------------------------------------------------------------
/// Closes the socket. In order to use the socket again, initialize()
/// must be called.
void CBluetoothSocketEngine::close()
{
    qDebug() << "CBluetoothSocketEngine::close";
    Q_D(CBluetoothSocketEngine);
    if (d->readNotifier) {
        d->readNotifier->setEnabled(false);
    }
    if (d->writeNotifier) {
        d->writeNotifier->setEnabled(false);
    }
    if (d->exceptNotifier) {
        d->exceptNotifier->setEnabled(false);
    }

    if(d->socketDescriptor != INVALID_SOCKET) {
        d->nativeClose();
        d->socketDescriptor = INVALID_SOCKET;
    }
    d->socketState = CBluetoothSocket::UnconnectedState;
    d->hasSetSocketError = false;
    d->serviceId = NULL_UUID;
    d->localAddress.clear();
    d->peerAddress.clear();

    if (d->readNotifier) {
        qDeleteInEventHandler(d->readNotifier);
        d->readNotifier = NULL;
    }
    if (d->writeNotifier) {
        qDeleteInEventHandler(d->writeNotifier);
        d->writeNotifier = NULL;
    }
    if (d->exceptNotifier) {
        qDeleteInEventHandler(d->exceptNotifier);
        d->exceptNotifier = NULL;
    }
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEngine::bytesAvailable() const
{
    Q_D(const CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::bytesAvailable(), -1);
    Q_CHECK_NOT_STATE(CBluetoothSocketEngine::bytesAvailable(), CBluetoothSocket::UnconnectedState, -1);

    return d->nativeBytesAvailable();
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEngine::read(char* data, qint64 maxlen)
{
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::read(), -1);
    Q_CHECK_STATES(CBluetoothSocketEngine::read(), CBluetoothSocket::ConnectedState, CBluetoothSocket::BoundState, -1);

    qint64 readBytes = d->nativeRead(data, maxlen);

    // Handle remote close
    if (readBytes == 0) {
        d->setError(CBluetoothSocketEngine::NetworkError, CBluetoothSocketEnginePrivate::RemoteHostClosedErrorString);
        close();
        return -1;

    } else if (readBytes == -1) {
        if (!d->hasSetSocketError) {
            d->hasSetSocketError = true;
            d->socketError = CBluetoothSocketEngine::NetworkError;
            d->socketErrorString = qt_error_string();
        }
        close();
        return -1;
    }
    return readBytes;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEngine::write(const char* data, qint64 len)
{
    Q_D(CBluetoothSocketEngine);
    Q_CHECK_VALID_SOCKETLAYER(CBluetoothSocketEngine::write(), -1);
    Q_CHECK_STATE(CBluetoothSocketEngine::write(), CBluetoothSocket::ConnectedState, -1);
    return d->nativeWrite(data, len);
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocketEngine::bytesToWrite() const
{
    return 0;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::setOption(CBluetoothSocketEngine::SocketOption option, int value)
{
    return d_func()->setOption(option, value);
}

///----------------------------------------------------------------------------
CBluetoothSocketEngine::Error CBluetoothSocketEngine::error() const
{
    return d_func()->socketError;
}

///----------------------------------------------------------------------------
QString CBluetoothSocketEngine::errorString() const
{
    return d_func()->socketErrorString;
}

///----------------------------------------------------------------------------
CBluetoothSocket::SocketState CBluetoothSocketEngine::state() const
{
    return d_func()->socketState;
}

///----------------------------------------------------------------------------
CBluetoothAddress CBluetoothSocketEngine::localAddress() const
{
    return d_func()->localAddress;
}

///----------------------------------------------------------------------------
quint32 CBluetoothSocketEngine::localPort() const
{
    return d_func()->localPort;
}

///----------------------------------------------------------------------------
CBluetoothAddress CBluetoothSocketEngine::peerAddress() const
{
    return d_func()->peerAddress;
}

///----------------------------------------------------------------------------
quint16 CBluetoothSocketEngine::peerPort() const
{
    return d_func()->peerPort;
}

///----------------------------------------------------------------------------
QUuid CBluetoothSocketEngine::serviceId() const
{
    return d_func()->serviceId;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::isReadNotificationEnabled() const
{
    Q_D(const CBluetoothSocketEngine);
    return d->readNotifier && d->readNotifier->isEnabled();
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::setReadNotificationEnabled(bool enable)
{
    Q_D(CBluetoothSocketEngine);
    if (d->readNotifier) {
        d->readNotifier->setEnabled(enable);
    } else if (enable && d->threadData->hasEventDispatcher()) {
        d->readNotifier = new CReadNotifier(d->socketDescriptor, this);
        d->readNotifier->setEnabled(true);
    }
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::isWriteNotificationEnabled() const
{
    Q_D(const CBluetoothSocketEngine);
    return d->writeNotifier && d->writeNotifier->isEnabled();
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::setWriteNotificationEnabled(bool enable)
{
    Q_D(CBluetoothSocketEngine);
    if (d->writeNotifier) {
        d->writeNotifier->setEnabled(enable);
    } else if (enable && d->threadData->hasEventDispatcher()) {
        d->writeNotifier = new CWriteNotifier(d->socketDescriptor, this);
        d->writeNotifier->setEnabled(true);
    }
}

///----------------------------------------------------------------------------
bool CBluetoothSocketEngine::isExceptionNotificationEnabled() const
{
    Q_D(const CBluetoothSocketEngine);
    return d->exceptNotifier && d->exceptNotifier->isEnabled();
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::setExceptionNotificationEnabled(bool enable)
{
    Q_D(CBluetoothSocketEngine);
    if (d->exceptNotifier) {
        d->exceptNotifier->setEnabled(enable);
    } else if (enable && d->threadData->hasEventDispatcher()) {
        d->exceptNotifier = new CExceptionNotifier(d->socketDescriptor, this);
        d->exceptNotifier->setEnabled(true);
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::readNotification()
{
    if (CAbstractSocketEngineReceiver *receiver = d_func()->receiver) {
        receiver->readNotification();
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::writeNotification()
{
    if (CAbstractSocketEngineReceiver *receiver = d_func()->receiver) {
        receiver->writeNotification();
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::closeNotification()
{
    if (CAbstractSocketEngineReceiver *receiver = d_func()->receiver) {
        receiver->closeNotification();
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::exceptionNotification()
{
    if (CAbstractSocketEngineReceiver *receiver = d_func()->receiver) {
        receiver->exceptionNotification();
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::connectionNotification()
{
    Q_D(CBluetoothSocketEngine);
    Q_ASSERT(state() == CBluetoothSocket::ConnectingState);

    connectToHost(d->peerAddress, d->serviceId);
    if (state() != CBluetoothSocket::ConnectingState) {
        // we changed states
        if (CAbstractSocketEngineReceiver *receiver = d_func()->receiver) {
            receiver->connectionNotification();
        }
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketEngine::setReceiver(CAbstractSocketEngineReceiver* receiver)
{
    d_func()->receiver = receiver;
}
