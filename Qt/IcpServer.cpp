#include <assert.h>
#include <QDataStream>

#include "IcpServer.h"
#include "IcpConnectionServer.h"

using namespace icp;

///----------------------------------------------------------------------------
IcpServer::IcpServer(const QString& qsName, QObject *parent) :
    QObject(parent),
    m_qsName(qsName),
    m_bStarted(false)
{
    assert(parent->metaObject()->className() == "IcpConnectionServer");
}

///----------------------------------------------------------------------------
IcpServer::~IcpServer()
{
    QMap<IcpAddress, IcpSocket*>::const_iterator it = m_PendingConnections.cbegin();
    for (; it != m_PendingConnections.cend(); ++it) delete *it;
}

///----------------------------------------------------------------------------
QByteArray IcpServer::wrapMessage(const QByteArray& baMessage, qint32 iMessageCode, bool bPrependSize)
{
    return ((IcpConnectionServer*) parent())->wrapMessage(baMessage, iMessageCode, bPrependSize);
}

///----------------------------------------------------------------------------
qint32 IcpServer::unwrapServiceMessage(const QByteArray& baDatagram)
{
    QByteArray baMagic = serviceMagic();
    return ((IcpConnectionServer*) parent())->unwrapServiceMessage(baMagic, baDatagram);
}

///----------------------------------------------------------------------------
void IcpServer::addPendingSocket(const IcpAddress& address, IcpSocket* pSocket)
{
    m_PendingConnections.remove(address);
    m_PendingConnections.insert(address, pSocket);
}
