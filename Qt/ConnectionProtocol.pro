QT += core
QT += core-private
QT += network
QT += xml
QT -= gui

TARGET = ConnectionProtocol
TEMPLATE = lib

CONFIG += staticlib
CONFIG += console

DEFINES += ENABLE_BLUETOOTH

CONFIG(release, debug|release){
    DEFINES += QT_NO_DEBUG_OUTPUT
#    DEFINES += "QNATIVESOCKETENGINE_DEBUG"

    win32-msvc* {
        QMAKE_LFLAGS += /NODEFAULTLIB:libcmt
    }

} else {
    win32-msvc* {
        DEFINES += "QNATIVESOCKETENGINE_DEBUG"
    }
}

win32-msvc* {
    DEFINES += "_BIND_TO_CURRENT_VCLIBS_VERSION=1"
}

SOURCES += \
    IcpClient.cpp \
    IcpClientList.cpp \
    IcpConnectionServer.cpp \
    IcpServer.cpp \
    IcpServerIp.cpp \
    IcpException.cpp \
    IcpSocket.cpp \
    CBluetoothAddress.cpp

HEADERS += \
    IcpClient.h \
    IcpClientList.h \
    IcpConnectionServer.h \
    IcpServer.h \
    IcpServerIp.h \
    IcpException.h \
    IcpAddress.h \
    IcpSocket.h \
    CBluetoothAddress.h

contains(DEFINES, ENABLE_BLUETOOTH) {
    message("BLUETOOTH IS ENABLED")
    SOURCES += \
        IcpServerBluetooth.cpp \
        CBluetoothSocket.cpp \
        CBluetoothServer.cpp \
        CBluetoothLocalDevice.cpp \
        CBluetoothHostInfo.cpp \
        CBluetoothServiceDiscoveryAgent.cpp \
        CBluetoothServiceInfo.cpp \
        CBluetoothDeviceInfo.cpp \
        CBluetoothSocketEngine_win.cpp

    HEADERS += \
        IcpServerBluetooth.h \
        CBluetoothSocket.h \
        CBluetoothServer.h \
        CBluetoothSocket_p.h \
        CBluetoothLocalDevice.h \
        CBluetoothHostInfo.h \
        CBluetoothServiceDiscoveryAgent.h \
        CBluetoothServiceInfo.h \
        CBluetoothDeviceInfo.h \
        CBluetoothSocketEngine_p.h \
        CBluetoothServiceDiscoveryAgent_p.h

} else {
    message("BLUETOOTH IS DISABLED")
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
