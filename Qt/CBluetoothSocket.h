#ifndef CBLUETOOTHSOCKET_H
#define CBLUETOOTHSOCKET_H

#include <QObject>
#include <QIODevice>
#include <QAbstractSocket>
#include <QUuid>

#include "CBluetoothAddress.h"
#include "CBluetoothServiceInfo.h"

class CBluetoothSocketPrivate;

class CBluetoothSocket : public QIODevice
{
    Q_OBJECT
public:
    enum SocketError {
        UnknownSocketError       = QAbstractSocket::UnknownSocketError,
        NoSocketError            = -2,
        HostNotFoundError        = QAbstractSocket::HostNotFoundError,
        ServiceNotFoundError     = QAbstractSocket::SocketAddressNotAvailableError,
        NetworkError             = QAbstractSocket::NetworkError,
        UnsupportedProtocolError = 8,
        OperationError           = QAbstractSocket::OperationError
    };

    enum SocketState {
        UnconnectedState   = QAbstractSocket::UnconnectedState,
        ServiceLookupState = QAbstractSocket::HostLookupState,
        ConnectingState    = QAbstractSocket::ConnectingState,
        ConnectedState     = QAbstractSocket::ConnectedState,
        BoundState         = QAbstractSocket::BoundState,
        ClosingState       = QAbstractSocket::ClosingState,
        ListeningState     = QAbstractSocket::ListeningState
    };

    CBluetoothSocket(QObject* parent = 0);
    ~CBluetoothSocket();

    // from QIODevice
    virtual qint64 bytesAvailable() const;
    virtual qint64 bytesToWrite()   const;
    virtual bool canReadLine()      const;
    virtual void close();
    virtual bool isSequential()     const;

    void connectToService(const CBluetoothServiceInfo& service, OpenMode openMode = ReadWrite);
//    void connectToService(const CBluetoothAddress& address, const QUuid& uuid, OpenMode openMode = ReadWrite);
//    void connectToService(const CBluetoothAddress& address, quint16 port, OpenMode openMode = ReadWrite);
    void disconnectFromService();

    void abort();

    SocketError error()       const;
    QString     errorString() const;

    CBluetoothAddress localAddress() const;
//    QString           localName()    const;
    quint16           localPort()    const;

    CBluetoothAddress peerAddress() const;
//    QString           peerName()    const;
    quint16           peerPort()    const;

    bool setSocketDescriptor(int socketDescriptor, OpenMode openMode = ReadWrite);
    int  socketDescriptor() const;

//    CBluetoothServiceInfo::Protocol socketType() const;

    SocketState state() const;

signals:
    void connected();
    void disconnected();
    void error(CBluetoothSocket::SocketError error);
    void stateChanged(CBluetoothSocket::SocketState state);

protected:
    virtual qint64 readData(char *data, qint64 maxlen);
    virtual qint64 writeData(const char *data, qint64 len);

//    void doDeviceDiscovery(const QBluetoothServiceInfo & service, OpenMode openMode);
    void setSocketError(CBluetoothSocket::SocketError error);
    void setSocketState(CBluetoothSocket::SocketState state);

private:
    Q_DISABLE_COPY(CBluetoothSocket)
    Q_DECLARE_PRIVATE(CBluetoothSocket)

    Q_PRIVATE_SLOT(d_func(), void _q_startConnecting(const CBluetoothServiceInfo&))
    Q_PRIVATE_SLOT(d_func(), void _q_abortConnectionAttempt())
    Q_PRIVATE_SLOT(d_func(), void _q_testConnection())
//    Q_PRIVATE_SLOT(d_func(), void _q_forceDisconnect())
};

Q_DECLARE_METATYPE(CBluetoothSocket::SocketState)
Q_DECLARE_METATYPE(CBluetoothSocket::SocketError)

#endif // CBLUETOOTHSOCKET_H
