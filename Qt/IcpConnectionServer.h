#ifndef ICPCONNECTIONSERVER_H
#define ICPCONNECTIONSERVER_H

#include <QTcpServer>
#include <QUdpSocket>

#include "CBluetoothLocalDevice.h"
#include "CBluetoothServer.h"

#include "IcpServer.h"
//#include "IcpClient.h"
#include "IcpClientList.h"

namespace icp {

struct Parameters {
    int     ipPort;
    QString optionalIP;
    QUuid   bthServiceUUID;
    QString bthServiceName;
};

class IcpConnectionServer : public QObject
{
    Q_OBJECT
public:
    static const qint32 MSG_INVALID     = -1;
    static const qint32 MSG_HANDSHAKE   = 1;   // UDP
    static const qint32 MSG_CONNECT     = 2;   // TCP
    static const qint32 MSG_ONLINE      = 3;   // UDP
    static const qint32 MSG_GOODBYE     = 4;   // UDP
    static const qint32 MSG_DATA        = 255; // TCP (Max value)

    static const int    MSG_MAX_SIZE = 1048576;

private:
    // Datagram "Magic": 8 bytes
    static const QByteArray MAGIC_CLIENT;
    static const QByteArray MAGIC_SERVER;

    static const int MAGIC_LENGTH  = 8;
    // Header: "Magic"; 4b (qint32) code; 4b (qint32) data length
    static const int HEADER_LENGTH = MAGIC_LENGTH + 8;

    static const int START_STOP_TIMEOUT         = 8000;
    static const int MISSED_ONLINE_CHECKS_LIMIT = 10;

public:
    explicit IcpConnectionServer(qint16 iFlavour, QObject *parent = 0);
    virtual ~IcpConnectionServer();

    void start();
    void stop();

    Parameters getParameters();
    void       setParameters(Parameters params);

    void sendData(IcpAddress address, const QByteArray& baData);

    bool isRunning() const;
    bool waitForStart();
    bool waitForStop();

    bool    clientIsConnected(const IcpAddress& address) const {return m_pClients->at(address) != NULL;}
    QString getConnectedList()                           const {return m_pClients->xml();}

    QString errorString() const {return m_qsErrorString;}

    QByteArray wrapMessage(const QByteArray& baMessage, qint32 iMessageCode, bool bPrependSize = false);

    qint32 unwrapServiceMessage(const QByteArray& magic, const QByteArray& baDatagram);

signals:
    void data(quint64 address, const QByteArray& baData);
    void connection();
    void disconnection();
    void error();

public slots:
    void slotStart();
    void slotStop();

    void slotSendData(quint64 address, const QByteArray& baData);

private slots:
    void slotRead(const IcpAddress& clientAddress, const QByteArray& baDatagram);
    void slotDisconnection(const IcpAddress& address);

private:
    Q_DISABLE_COPY(IcpConnectionServer)

    QByteArray unwrapMessage(const QByteArray& baDatagram, qint32* piMessageCode, qint32* piMessageSize);

    bool waitFor(bool bStart);

    IcpClientList* m_pClients;
    QMap<IcpAddress::Protocol, IcpServer*> m_Servers;

    QString m_qsName;
    qint32  m_iFlavour;

    int     m_iIpPort;
    QString m_qsOptionalIP;
    QUuid   m_BthServiceUUID;
    QString m_BthServiceName;

    bool    m_bStarted;
    QString m_qsErrorString;
};

}

#endif // ICPCONNECTIONSERVER_H
