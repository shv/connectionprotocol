#ifndef CBLUETOOTHSERVER_H
#define CBLUETOOTHSERVER_H

#include <QObject>
#include <QUuid>

#include "CBluetoothAddress.h"
#include "CBluetoothSocket.h"
#include "CBluetoothServiceInfo.h"

class CBluetoothServerPrivate;

class CBluetoothServer : public QObject
{
    Q_OBJECT
public:
    explicit CBluetoothServer(CBluetoothServiceInfo::Protocol serverType, QObject *parent = 0);
    ~CBluetoothServer();

    enum Error {
        NoError                       = 0,
        UnknownError                  = 1,
        PoweredOffError               = 2,
        InputOutputError              = 3,
        ServiceAlreadyRegisteredError = 4,
        UnsupportedProtocolError      = 5
    };

signals:
    void error(CBluetoothServer::Error error);
    void newConnection();

public:
    bool listen(const QUuid& serviceId, const QString& serviceName = QString());
    void close();

    CBluetoothServiceInfo::Protocol serverType() const;

    CBluetoothAddress serverAddress() const;

    bool isListening() const;
    bool hasPendingConnections() const;

    CBluetoothSocket* nextPendingConnection();

    void setMaxPendingConnections(int numConnections);
    int  maxPendingConnections() const;

    Error error() const;
    QString errorString() const; // Non Qt

protected:
    void incomingConnection(qintptr socketDescriptor);
    void addPendingConnection(CBluetoothSocket* socket);

private:
    Q_DISABLE_COPY(CBluetoothServer)
    Q_DECLARE_PRIVATE(CBluetoothServer)
};

#endif // CBLUETOOTHSERVER_H
