#include "IcpServerIp.h"

#include <QHostAddress>
#include <QTcpSocket>
#include <QDataStream>
#include <QNetworkInterface>

#include "IcpConnectionServer.h"
#include "IcpException.h"

using namespace icp;

const QHostAddress IcpServerIp::MULTICAST_GROUP("230.229.170.71");

///----------------------------------------------------------------------------
IcpServerIp::IcpServerIp(const QString& qsName, QObject* parent) :
    IcpServer(qsName, parent),
    m_uiNextBlockSize(0),
    m_iIpPort(0),
    m_OptionalIP()
{
    m_pServer = new QTcpServer(this);
    connect(m_pServer, SIGNAL(newConnection()), SLOT(slotNewConnection()));
}

///----------------------------------------------------------------------------
IcpServerIp::~IcpServerIp()
{
    stop();
}

///----------------------------------------------------------------------------
void IcpServerIp::start(Parameters params)
{
    qDebug() << "IcpServerIp::start";
    if (isStarted()) return;
    setStarted();

    if (params.ipPort == 0) {
        setStopped();
        return;
    }

    m_iIpPort = params.ipPort;
    if (!m_OptionalIP.setAddress(params.optionalIP)) {
        m_OptionalIP = QHostAddress::Null;
    }

    QString qsErrorString("");

    // Network
    int iInterfacesAvailable = 0;
    QVector<QNetworkInterface> netInterfaces = QNetworkInterface::allInterfaces().toVector();
    QVector<QNetworkInterface>::const_iterator itI = netInterfaces.cbegin();
    for (; itI != netInterfaces.cend(); ++itI) {
        const QNetworkInterface& netInterface = *itI;
        QNetworkInterface::InterfaceFlags flags = netInterface.flags();

        if (!flags.testFlag(QNetworkInterface::IsUp) || !flags.testFlag(QNetworkInterface::CanBroadcast) || flags.testFlag(QNetworkInterface::IsLoopBack)) continue;

        ++iInterfacesAvailable;

        QVector<QNetworkAddressEntry> entries = netInterface.addressEntries().toVector();
        QVector<QNetworkAddressEntry>::const_iterator itE = entries.cbegin();
        for (; itE != entries.cend(); ++itE) {
            const QNetworkAddressEntry& entry = *itE;
            QHostAddress address = entry.ip();
            if (address.protocol() != QAbstractSocket::IPv4Protocol || address.isLoopback()) continue;

            int iNetMask = QHostAddress::parseSubnet("0.0.0.0/" + entry.netmask().toString()).second;
//            qDebug() << QHostAddress::parseSubnet("0.0.0.0/" + entry.netmask().toString()).first << QHostAddress::parseSubnet("0.0.0.0/" + entry.netmask().toString()).second;

            // Ordinal
            QUdpSocket* pUdpSocket = new QUdpSocket();
            if (pUdpSocket->bind(address, m_iIpPort, QAbstractSocket::ShareAddress | QAbstractSocket::ReuseAddressHint)) {
                QPair<QUdpSocket*, int> pair(pUdpSocket, iNetMask);

                m_UdpSockets.append(pair);
                connect(pUdpSocket, SIGNAL(readyRead()), SLOT(slotUDPRead()));

            } else {
                qsErrorString = pUdpSocket->errorString();
                delete pUdpSocket;
            }
        }

        // Multicast
        if (flags.testFlag(QNetworkInterface::CanMulticast)) {
            QUdpSocket* pUdpSocket = new QUdpSocket();
            if (pUdpSocket->bind(QHostAddress::Any, m_iIpPort, QAbstractSocket::ShareAddress | QAbstractSocket::ReuseAddressHint)) {
                pUdpSocket->joinMulticastGroup(QHostAddress(MULTICAST_GROUP), netInterface);
                pUdpSocket->setSocketOption(QAbstractSocket::MulticastTtlOption, QVariant(MULTICAST_TTL));

                m_MCSockets.append(pUdpSocket);
                connect(pUdpSocket, SIGNAL(readyRead()), SLOT(slotUDPRead()));

            } else {
//                qsErrorString = pUdpSocket->errorString();
                qWarning() << "UDP-MC" << "Unable to start the server:" << pUdpSocket->errorString();
                delete pUdpSocket;
            }
        }
    }

    bool bStarted = m_UdpSockets.size() != 0;
    if (!bStarted) {
        if (qsErrorString.isEmpty()) {
            if (iInterfacesAvailable == 0) {
                qsErrorString = tr("TCP/IP network interface is unavailable");
            } else {
                qsErrorString = tr("Unknown network error");
            }
        }
        qWarning() << "UDP" << "Unable to start the server:" << qsErrorString;

    } else {
        bStarted = m_pServer->listen(QHostAddress::AnyIPv4, m_iIpPort);
        if (!bStarted) {
            qWarning() << "TCP" << "Unable to start server:" << m_pServer->errorString();
            qsErrorString = m_pServer->errorString();
            m_pServer->close();
            clearUDPSocketList();

            bStarted = false;
        }
    }

    if (!bStarted) {
        setStopped();
        IcpException error(qsErrorString);
        error.raise();
    }

    sendHandshake(IcpAddress::Null());
}

///----------------------------------------------------------------------------
void IcpServerIp::stop()
{
    qDebug() << "IcpServerIp::stop()";
    sendGoodbye(IcpAddress::Null());
    setStopped();

    if (isActive()) {
        m_pServer->close();
        clearUDPSocketList();
    }
}

///----------------------------------------------------------------------------
void IcpServerIp::sendHandshake(const IcpAddress& address)
{
//#ifdef QT_NO_DEBUG
    QByteArray baName = name().toUtf8();
    if (address.isNull()) {
        sendUdp(baName, QHostAddress::Broadcast, IcpConnectionServer::MSG_HANDSHAKE);
        if (!m_OptionalIP.isNull()) {
            sendUdp(baName, m_OptionalIP, IcpConnectionServer::MSG_HANDSHAKE);
        }

    } else {
        sendUdp(baName, (QHostAddress) address, IcpConnectionServer::MSG_HANDSHAKE);
    }
//#else
//    address; // Warning supress
//    sendUdp(name().toUtf8(), QHostAddress::Broadcast, IcpConnectionServer::MSG_HANDSHAKE);
//#endif
}

///----------------------------------------------------------------------------
void IcpServerIp::sendOnlineNotification(const IcpAddress& address)
{
//    if (iProtocol == TCP) {
//        CClient2* pClient = m_Clients.at(clientAddress);
//        if (pClient != NULL) sendTcp(m_qsName.toUtf8(), pClient->socket(), MSG_ONLINE);
//    } else if (iProtocol == UDP) {
//#ifdef QT_NO_DEBUG
    sendUdp(name().toUtf8(), (QHostAddress) address, IcpConnectionServer::MSG_ONLINE);
//#else
//        sendUdp(name().toUtf8(), QHostAddress::Broadcast, IcpConnectionServer::MSG_ONLINE);
//#endif
//    }
}

///----------------------------------------------------------------------------
void IcpServerIp::sendGoodbye(const IcpAddress& address)
{
    sendUdp(name().toUtf8(), address.isNull() ? QHostAddress::Broadcast: (QHostAddress) address, IcpConnectionServer::MSG_GOODBYE);
}

///----------------------------------------------------------------------------
void IcpServerIp::sendData(IcpSocket* pSocket, const QByteArray& baData)
{
    sendTcp(baData, (QTcpSocket*) *pSocket, IcpConnectionServer::MSG_DATA);
}

///----------------------------------------------------------------------------
void IcpServerIp::slotUDPRead()
{
    qDebug() << "IcpServerIp::slotUDPRead()";
    QByteArray   baDatagram;
    QUdpSocket*  pUdpSocket = (QUdpSocket*)sender();
    QHostAddress clientAddress;
    quint16      uiPort;

    do {
        if (pUdpSocket->pendingDatagramSize() > UDP_MAX_SIZE) {
            pUdpSocket->readDatagram(baDatagram.data(), 0);
            continue;
        }

        baDatagram.resize(pUdpSocket->pendingDatagramSize());
        pUdpSocket->readDatagram(baDatagram.data(), baDatagram.size(), &clientAddress, &uiPort);
        if (uiPort != m_iIpPort) continue;

        emit dataReceived(IcpAddress(clientAddress), baDatagram);

    } while(pUdpSocket->hasPendingDatagrams());
}

///----------------------------------------------------------------------------
void IcpServerIp::slotTCPRead()
{
    qDebug() << "IcpServerIp::slotTCPRead()";
    QTcpSocket*  pClientSocket = (QTcpSocket*)sender();
    QHostAddress clientAddress = pClientSocket->peerAddress();

    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_1);

    if (m_uiNextBlockSize == 0 && pClientSocket->bytesAvailable() >= (qint64)sizeof(quint32)) {
        in >> m_uiNextBlockSize;
    }

    if (m_uiNextBlockSize > IcpConnectionServer::MSG_MAX_SIZE) {
        pClientSocket->abort();
        sendHandshake(IcpAddress(clientAddress));
        m_uiNextBlockSize = 0;

    } else if (pClientSocket->bytesAvailable() >= m_uiNextBlockSize) {
        // All data is available
        quint32 uiPacketSize = m_uiNextBlockSize;
        m_uiNextBlockSize = 0;

        QByteArray baDatagram(uiPacketSize, '\0');
        in.readRawData(baDatagram.data(), uiPacketSize);

        emit dataReceived(IcpAddress(clientAddress), baDatagram);
    }
}

///----------------------------------------------------------------------------
void IcpServerIp::slotNewConnection()
{
    QTcpSocket* pClientSocket = m_pServer->nextPendingConnection();
    IcpAddress clientAddress(pClientSocket->peerAddress());

    qDebug() << QString("Client '%1' connected.").arg(pClientSocket->peerAddress().toString());

    connect(pClientSocket, SIGNAL(readyRead()),                   SLOT(slotTCPRead()));
    connect(pClientSocket, SIGNAL(disconnected()),                SLOT(slotDisconnection()));
    connect(pClientSocket, SIGNAL(disconnected()), pClientSocket, SLOT(deleteLater()));

    addPendingSocket(clientAddress, new IcpSocket(pClientSocket));

    sendTcp(name().toUtf8(), pClientSocket, IcpConnectionServer::MSG_CONNECT);
}

///----------------------------------------------------------------------------
void IcpServerIp::slotDisconnection()
{
    QTcpSocket* pClientSocket = (QTcpSocket*) sender();

    emit clientDisconnected(pClientSocket->peerAddress());

    qDebug() << QString("TCP client '%1' disconnected.").arg(pClientSocket->peerAddress().toString());

    pClientSocket->abort();
    pClientSocket->close();
}

/////----------------------------------------------------------------------------
//void IcpServerIp::slotStart(void* pParameter)
//{
//    start(pParameter);
//}

/////----------------------------------------------------------------------------
//void IcpServerIp::slotStop()
//{
//    stop();
//}

///----------------------------------------------------------------------------
void IcpServerIp::sendUdp(const QByteArray& baMessage, const QHostAddress& address, int iMessageCode)
{
    if (m_UdpSockets.size() == 0) return;

    QByteArray baDatagram = wrapMessage(baMessage, iMessageCode);

    QVector<QPair<QUdpSocket*, int> >::const_iterator it = m_UdpSockets.cbegin();
    for(; it != m_UdpSockets.cend(); ++it) {
        const QPair<QUdpSocket*, int>& pair = *it;

        QUdpSocket*         pSocket      = pair.first;
        const QHostAddress& localAddress = pSocket->localAddress();
        quint32             iMask        = pair.second;

        if (address == QHostAddress::Broadcast) {
            quint32 uiBitMask = 0xffffffff >> iMask;
            QHostAddress* pBroadcastAddress = new QHostAddress(localAddress.toIPv4Address() | uiBitMask);

            pSocket->writeDatagram(baDatagram, *pBroadcastAddress, m_iIpPort);
            qDebug() << "UDP sent from" << localAddress.toString() << "to" << pBroadcastAddress->toString() << ":" << baDatagram;

            delete pBroadcastAddress;

        } else {//if (address.isInSubnet(localAddress, iMask)) {
            pSocket->writeDatagram(baDatagram, address, m_iIpPort);
            qDebug() << "UDP sent from" << localAddress.toString() << "to" << address.toString() << ":" << baDatagram;
        }
    }

    if (address == QHostAddress::Broadcast) {
        QVector<QUdpSocket*>::const_iterator it = m_MCSockets.begin();
        for (; it != m_MCSockets.end(); ++it) {
            QUdpSocket* pSocket = *it;

            pSocket->writeDatagram(baDatagram, MULTICAST_GROUP, m_iIpPort);
            qDebug() << "Multicast UDP sent to" << MULTICAST_GROUP.toString() << ":" << baDatagram;
        }
    }

//    debug_log(baDatagram, "sendUdp.log");
}

///----------------------------------------------------------------------------
void IcpServerIp::sendTcp(const QByteArray& baMessage, QTcpSocket* pSocket, int iMessageCode)
{
    if (pSocket->state() != QAbstractSocket::ConnectedState) return;

    QByteArray baDatagram = wrapMessage(baMessage, iMessageCode, true);

    pSocket->write(baDatagram);

    qDebug() << "TCP sent:" << baMessage;

//    debug_log(baDatagram, "sendTcp.log");
}

///----------------------------------------------------------------------------
void IcpServerIp::clearUDPSocketList()
{
    qDebug() << "clearUDPSocketList()";
    QVector<QPair<QUdpSocket*, int> >::const_iterator it = m_UdpSockets.begin();
    for (; it != m_UdpSockets.end(); ++it) {
        QUdpSocket* pSocket = (*it).first;

        disconnect(pSocket, 0, 0, 0);
        pSocket->close();
        delete pSocket;
    }
    m_UdpSockets.clear();

    QVector<QUdpSocket*>::const_iterator it2 = m_MCSockets.begin();
    for (; it2 != m_MCSockets.end(); ++it2) {
        QUdpSocket* pSocket = *it2;

        disconnect(pSocket, 0, 0, 0);
        pSocket->leaveMulticastGroup(MULTICAST_GROUP, pSocket->multicastInterface());
        pSocket->close();
        delete pSocket;
    }
    m_MCSockets.clear();
}
