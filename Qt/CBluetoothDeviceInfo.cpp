#include "CBluetoothDeviceInfo.h"

//#include IcpConnectionServerer.h"

///----------------------------------------------------------------------------
CBluetoothDeviceInfo::CBluetoothDeviceInfo() :
    m_qsName(""),
    m_Address()
{
}

///----------------------------------------------------------------------------
CBluetoothDeviceInfo::CBluetoothDeviceInfo(const CBluetoothAddress& address, const QString& name) :
    m_qsName(name),
    m_Address(address)
{
}

///----------------------------------------------------------------------------
CBluetoothDeviceInfo::CBluetoothDeviceInfo(const CBluetoothDeviceInfo& other) :
    m_qsName(other.m_qsName),
    m_Address(other.m_Address)
{
}

/////----------------------------------------------------------------------------
//QList<QUuid> CBluetoothDeviceInfo::serviceUuids(CBluetoothDeviceInfo::DataCompleteness* completeness) const
//{
//    QList<QUuid> uuids = QList<QUuid>();
//    if (isValid()) {
////        GUID classId = (GUID) IcpConnectionServer::BLUETOOTH_SERVICE_UUID;

////        m_Address.toString().toStdWString();
////        //Initialise quering the device services
////        WSAQUERYSET servicesQuery = {sizeof(WSAQUERYSET)};
////        servicesQuery.dwNameSpace      = NS_BTH;
////        servicesQuery.lpszContext      = QSaddressString;
////        servicesQuery.lpServiceClassId = &classId;

////        HANDLE hLookupServices;
////        DWORD dwServicesResult = WSALookupServiceBegin(&servicesQuery,
////                                                       LUP_RETURN_NAME | LUP_RETURN_TYPE |
////                                                       LUP_RETURN_BLOB | LUP_RETURN_COMMENT | LUP_FLUSHCACHE,
////                                                       &hLookupServices);
////        if (dwServicesResult == SOCKET_ERROR){
////            cout << "An error " << WSAGetLastError() << " occured while initializing query for services\n\n";
////            continue;
////        }

////        //Start quering for device services
////        wcout << "\tServices: \n";
////        while(dwServicesResult == 0){
////            char  servicesResultBuffer[sizeof(WSAQUERYSET) + 8096] = {};
////            DWORD dwServicesResultLength = sizeof(servicesResultBuffer);

////            WSAQUERYSET* pServicesResult = (WSAQUERYSET*) servicesResultBuffer;

////            dwServicesResult = WSALookupServiceNext(hLookupServices,
////                                                    LUP_DEEP | LUP_RETURN_ALL | LUP_FLUSHCACHE,
////                                                    &dwServicesResultLength, pServicesResult);
////            if(dwServicesResult == 0) {
////                WCHAR name[1024] = {};
////                DWORD nameLength = sizeof(name) / sizeof(WCHAR);
////                BLOB* lpSdpBlob = pServicesResult->lpBlob;
////                DWORD attrResult = BluetoothSdpGetString(lpSdpBlob->pBlobData, lpSdpBlob->cbSize, NULL, STRING_NAME_OFFSET, name, &nameLength);

////                SDP_ELEMENT_DATA descriptorData = {};
////                attrResult = BluetoothSdpGetAttributeValue(lpSdpBlob->pBlobData, lpSdpBlob->cbSize, SDP_ATTRIB_PROTOCOL_DESCRIPTOR_LIST, &descriptorData);
////                if (attrResult == ERROR_SUCCESS) {

////                } else {
////                    wcout << ", descriptor = " << (attrResult == ERROR_FILE_NOT_FOUND ? "Not found" : "Invalid parameter");
////                }

////                SDP_ELEMENT_DATA attributeData = {};
////                attrResult = BluetoothSdpGetAttributeValue(lpSdpBlob->pBlobData, lpSdpBlob->cbSize, SDP_ATTRIB_SERVICE_ID, &attributeData);

////                wcout << "\t\t " << name;
////                if (attrResult == ERROR_SUCCESS) {
////                    GUID guid = attributeData.data.uuid128;
////                    printf(", uuid = {%08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX}",
////                           attributeData.data.uuid128.Data1, guid.Data2, guid.Data3,
////                           guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
////                           guid.Data4[4], guid.Data4[5], guid.Data4[6], attributeData.data.uuid128.Data4[7]);

////                } else {
////                    wcout << ", uuid = " << (attrResult == ERROR_FILE_NOT_FOUND ? "Not found" : "Invalid parameter");
////                    BluetoothSdpEnumAttributes(lpSdpBlob->pBlobData, lpSdpBlob->cbSize,
////                                               (PFN_BLUETOOTH_ENUM_ATTRIBUTES_CALLBACK)&(::BleetoothAttributeEnumerationCallback), NULL);
////                }
////                wcout << "\n";
//////                    wcout << "Service: " << pServicesResult->lpszServiceInstanceName << "; " << "\n";
//////                    wcout << "Service: " << pServicesResult->lpszServiceInstanceName << "; " << pServicesResult->lpszComment << "\n";
//////                    wcout << "Service: " << pResults2->lpszServiceInstanceName << ", " << pResults2->lpServiceClassId <<"\n";
////            }
////        }

////        WSALookupServiceEnd(hLookupServices);


//        *completeness = DataIncomplete;

//    } else {
//        *completeness = DataUnavailable;
//    }

//    return uuids;
//}
