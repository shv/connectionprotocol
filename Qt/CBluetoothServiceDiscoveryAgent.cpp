#include "CBluetoothServiceDiscoveryAgent.h"
#include "CBluetoothServiceDiscoveryAgent_p.h"

#include <QDebug>
#include <QThread>

#include <winsock2.h>
//#include <ws2bth.h>
//#include <bthdef.h>
//#include <Dbt.h>
#pragma warning (disable : 4068)
#include <BluetoothAPIs.h>

#include "private/qobject_p.h"


///----------------------------------------------------------------------------
/// CBluetoothServiceDiscoveryAgentPrivate
///----------------------------------------------------------------------------
class CBluetoothServiceDiscoveryAgentPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(CBluetoothServiceDiscoveryAgent)
public:
    enum ErrorString {
        UnableToEnumerateServices
    };

    explicit CBluetoothServiceDiscoveryAgentPrivate();
    virtual ~CBluetoothServiceDiscoveryAgentPrivate();

    void setError(CBluetoothServiceDiscoveryAgent::Error error, ErrorString errorString);

    HANDLE radioHandle;

    QList<QUuid>      uuidFilter;
    CBluetoothAddress remoteAddress;
    bool              isActive;

    QList<CBluetoothServiceInfo> discoveredServices;

    CBluetoothServiceDiscoveryAgent::Error error;
    QString                                errorString;

    CServiceDiscoveryThread* discoveryThread;
};

///----------------------------------------------------------------------------
CBluetoothServiceDiscoveryAgentPrivate::CBluetoothServiceDiscoveryAgentPrivate() :
    radioHandle(NULL),
    isActive(false),
    error(CBluetoothServiceDiscoveryAgent::NoError)
{
    uuidFilter.append(QUuid(RFCOMM_PROTOCOL_UUID));

    HANDLE hRadio(NULL);

    BLUETOOTH_RADIO_INFO        radioInfo       = {sizeof(BLUETOOTH_RADIO_INFO)};
    BLUETOOTH_FIND_RADIO_PARAMS findRadioParams = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};

    HBLUETOOTH_RADIO_FIND hRadioFind = ::BluetoothFindFirstRadio(&findRadioParams, &hRadio);

    bool bError = hRadioFind == NULL;
    if (!bError) {
        bError = ::BluetoothGetRadioInfo(hRadio, &radioInfo) != ERROR_SUCCESS;
        if (!bError) {
            radioHandle = hRadio;
        }
        ::BluetoothFindRadioClose(hRadioFind);
    }

    if (bError && hRadio != NULL) {
        ::CloseHandle(hRadio);
    }
}

///----------------------------------------------------------------------------
CBluetoothServiceDiscoveryAgentPrivate::~CBluetoothServiceDiscoveryAgentPrivate()
{
}

///----------------------------------------------------------------------------
void CBluetoothServiceDiscoveryAgentPrivate::setError(CBluetoothServiceDiscoveryAgent::Error error, ErrorString errorString)
{
    this->error = error;
    switch (errorString) {
    case UnableToEnumerateServices:
        this->errorString = CBluetoothServiceDiscoveryAgent::tr("Unable to enumerate installed services.");
        break;
    default:
        break;
    }

    emit q_func()->error();
}

///----------------------------------------------------------------------------
/// CServiceDiscoveryThread
///----------------------------------------------------------------------------
CServiceDiscoveryThread::CServiceDiscoveryThread(CBluetoothServiceDiscoveryAgent::DiscoveryMode mode, CBluetoothServiceDiscoveryAgentPrivate* d, QObject* parent) :
    QThread(parent),
    d(d),
    mode(mode),
    isCancelled(false)
{
}

///----------------------------------------------------------------------------
CServiceDiscoveryThread::~CServiceDiscoveryThread()
{
}

///----------------------------------------------------------------------------
void CServiceDiscoveryThread::run()
{
    d->isActive = true;

    bool addressFiltered = !d->remoteAddress.isNull();
    QList<QUuid> filter = QList<QUuid>(d->uuidFilter);
    if (mode == CBluetoothServiceDiscoveryAgent::MinimalDiscovery) {
        filter.removeOne(QUuid(RFCOMM_PROTOCOL_UUID));
    }

    HANDLE                         hFindDevice;
    BLUETOOTH_DEVICE_INFO          deviceInfo         = {sizeof(BLUETOOTH_DEVICE_INFO)};
    BLUETOOTH_DEVICE_SEARCH_PARAMS deviceSearchParams = {sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS)};
    deviceSearchParams.fReturnAuthenticated = TRUE;
    deviceSearchParams.fReturnRemembered    = FALSE;
    deviceSearchParams.fReturnUnknown       = FALSE;
    deviceSearchParams.fReturnConnected     = TRUE;
    deviceSearchParams.fIssueInquiry        = FALSE;
    deviceSearchParams.cTimeoutMultiplier   = 1;
    deviceSearchParams.hRadio               = d->radioHandle;

    hFindDevice = ::BluetoothFindFirstDevice(&deviceSearchParams, &deviceInfo);
    if (!isCancelled && hFindDevice != NULL) {
        do {
            CBluetoothAddress remoteAddress = CBluetoothAddress(deviceInfo.Address.ullLong);
            if (addressFiltered && remoteAddress != remoteAddress) {
                continue;
            }

            CBluetoothDeviceInfo device = CBluetoothDeviceInfo(remoteAddress, QString::fromWCharArray(deviceInfo.szName));

            if (mode == CBluetoothServiceDiscoveryAgent::MinimalDiscovery) {
                GUID  guids[10] = {};
                DWORD guidsSize = sizeof(guids);
                DWORD result = ::BluetoothEnumerateInstalledServices(d->radioHandle, &deviceInfo, &guidsSize, guids);

                if (result == ERROR_SUCCESS) {
                    loopThroughEnumeratedServices(filter, device, guids, guidsSize);

                } else if (result == ERROR_MORE_DATA) {
                    GUID* guids = new GUID[guidsSize];
                    DWORD result = ::BluetoothEnumerateInstalledServices(d->radioHandle, &deviceInfo, &guidsSize, guids);
                    if (result == ERROR_SUCCESS) {
                        loopThroughEnumeratedServices(filter, device, guids, guidsSize);
                    }
                    delete guids;

                } else {
                    d->setError(CBluetoothServiceDiscoveryAgent::UnknownError, CBluetoothServiceDiscoveryAgentPrivate::UnableToEnumerateServices);
                }

            } else {
                wchar_t addressString[40];
                remoteAddress.toString().toWCharArray(addressString);
                qDebug() << "CServiceDiscoveryThread::run():" << device.name() << "1" << remoteAddress.toString();

                foreach (QUuid uuid, filter) {
                    if (isCancelled) {
                        break;
                    }

                    qDebug() << "CServiceDiscoveryThread::run():" << device.name() << "2" << uuid.toString();
                    GUID classId = (GUID) uuid;

                    WSAQUERYSET servicesQuery = {sizeof(WSAQUERYSET)};
                    servicesQuery.dwNameSpace      = NS_BTH;
                    servicesQuery.lpszContext      = addressString;
                    servicesQuery.lpServiceClassId = &classId;

                    //Initialise quering the device services
                    HANDLE hLookupServices;
                    DWORD  dwResult = 0;
                    dwResult = ::WSALookupServiceBegin(&servicesQuery, LUP_RETURN_BLOB | LUP_FLUSHCACHE, &hLookupServices);
                    if (dwResult == SOCKET_ERROR){
                        qWarning() << "CServiceDiscoveryThread::run():" << device.name() << "begin service discovery with cached data";
                        dwResult = ::WSALookupServiceBegin(&servicesQuery, LUP_RETURN_BLOB, &hLookupServices);
                    }

                    if (dwResult == SOCKET_ERROR){
                        qWarning() << "CServiceDiscoveryThread::run():" << device.name() << "An error" << ::WSAGetLastError() << " occured while initializing query for services";
                        continue;
                    }

                    while(!isCancelled && dwResult == 0){
                        char  servicesResultBuffer[sizeof(WSAQUERYSET) + 8096] = {};
                        DWORD dwServicesResultLength = sizeof(servicesResultBuffer);

                        WSAQUERYSET* pServicesResult = (WSAQUERYSET*) servicesResultBuffer;

                        dwResult = ::WSALookupServiceNext(hLookupServices, LUP_RETURN_BLOB | LUP_FLUSHCACHE, &dwServicesResultLength, pServicesResult);
                        if(dwResult == 0) {
                            CBluetoothServiceInfo info = CBluetoothServiceInfo();
                            info.setDevice(device);
                            info.setServiceUuid(uuid);

                            WCHAR name[1024] = {};
                            DWORD nameLength = sizeof(name) / sizeof(WCHAR);
                            BLOB* lpSdpBlob = pServicesResult->lpBlob;
                            DWORD attrResult = ::BluetoothSdpGetString(lpSdpBlob->pBlobData, lpSdpBlob->cbSize, NULL, STRING_NAME_OFFSET, name, &nameLength);
                            if (attrResult == ERROR_SUCCESS) {
                                info.setServiceName(QString::fromWCharArray(name));
                            } else {
                                info.setServiceName("");
                                qWarning() << "CServiceDiscoveryThread::run():" << device.name() << "An error" << ::WSAGetLastError() << " occured while quering service name";
                            }
                            emit serviceDiscovered(info);
                        }
                    }

                    ::WSALookupServiceEnd(hLookupServices);
                }
            }
        } while(!isCancelled && ::BluetoothFindNextDevice(hFindDevice, &deviceInfo));
        ::BluetoothFindDeviceClose(hFindDevice);
    }

    d->isActive = false;
    if (isCancelled) {
        emit canceled();
    }
}

///----------------------------------------------------------------------------
void CServiceDiscoveryThread::loopThroughEnumeratedServices(const QList<QUuid>& filter, const CBluetoothDeviceInfo& device, GUID guids[], int guidsSize)
{
    bool filtered = !filter.isEmpty();
    for (int i = 0; !isCancelled && i < guidsSize; i++) {
        QUuid uuid = QUuid(guids[i]);
        if (filtered && !filter.contains(uuid)) {
            continue;
        }

        CBluetoothServiceInfo info = CBluetoothServiceInfo();
        info.setDevice(device);
        info.setServiceUuid(uuid);
        info.setServiceName("");

        emit serviceDiscovered(info);
    }
}

///----------------------------------------------------------------------------
void CServiceDiscoveryThread::slotCancel()
{
    isCancelled = true;
}

///----------------------------------------------------------------------------
/// CBluetoothServiceDiscoveryAgent
///----------------------------------------------------------------------------
CBluetoothServiceDiscoveryAgent::CBluetoothServiceDiscoveryAgent(QObject *parent) :
    QObject(*new CBluetoothServiceDiscoveryAgentPrivate(), parent)
{
}

///----------------------------------------------------------------------------
QList<CBluetoothServiceInfo> CBluetoothServiceDiscoveryAgent::discoveredServices() const
{
    return d_func()->discoveredServices;
}

///----------------------------------------------------------------------------
bool CBluetoothServiceDiscoveryAgent::isActive() const
{
    return d_func()->isActive;
}

///----------------------------------------------------------------------------
bool CBluetoothServiceDiscoveryAgent::setRemoteAddress(const CBluetoothAddress& address)
{
    Q_D(CBluetoothServiceDiscoveryAgent);
    if (d->isActive) {
        return false;
    }
    d->remoteAddress = address;
    return true;
}

///----------------------------------------------------------------------------
CBluetoothAddress CBluetoothServiceDiscoveryAgent::remoteAddress() const
{
    return d_func()->remoteAddress;
}

///----------------------------------------------------------------------------
CBluetoothServiceDiscoveryAgent::Error CBluetoothServiceDiscoveryAgent::error() const
{
    return d_func()->error;
}

///----------------------------------------------------------------------------
QString CBluetoothServiceDiscoveryAgent::errorString() const
{
    return d_func()->errorString;
}

///----------------------------------------------------------------------------
void CBluetoothServiceDiscoveryAgent::setUuidFilter(const QList<QUuid>& uuids)
{
    d_func()->uuidFilter = uuids;
}

///----------------------------------------------------------------------------
void CBluetoothServiceDiscoveryAgent::setUuidFilter(const QUuid& uuid)
{
    Q_D(CBluetoothServiceDiscoveryAgent);
    d->uuidFilter.clear();
    d->uuidFilter.append(uuid);
}

///----------------------------------------------------------------------------
QList<QUuid> CBluetoothServiceDiscoveryAgent::uuidFilter() const
{
    return d_func()->uuidFilter;
}

///----------------------------------------------------------------------------
void CBluetoothServiceDiscoveryAgent::clear()
{
    Q_D(CBluetoothServiceDiscoveryAgent);
    if (!d->isActive) {
        d->discoveredServices.clear();
        d->uuidFilter.clear();
        d->uuidFilter.append(QUuid(RFCOMM_PROTOCOL_UUID));
    }
}

///----------------------------------------------------------------------------
void CBluetoothServiceDiscoveryAgent::start(CBluetoothServiceDiscoveryAgent::DiscoveryMode mode)
{
    Q_D(CBluetoothServiceDiscoveryAgent);
    if (d->isActive) {
        return;
    }

    if (d->radioHandle == NULL) {
        emit finished();
        return;
    }

    d->discoveryThread = new CServiceDiscoveryThread(mode, d, this);
    connect(d->discoveryThread, SIGNAL(finished())                              , this              , SIGNAL(finished()));
    connect(d->discoveryThread, SIGNAL(finished())                              , d->discoveryThread, SLOT(deleteLater()));
    connect(d->discoveryThread, SIGNAL(canceled())                              , this              , SIGNAL(canceled()));
    connect(d->discoveryThread, SIGNAL(serviceDiscovered(CBluetoothServiceInfo)), this              , SIGNAL(serviceDiscovered(CBluetoothServiceInfo)));
    d->discoveryThread->start();
}

///----------------------------------------------------------------------------
void CBluetoothServiceDiscoveryAgent::stop()
{
    Q_D(CBluetoothServiceDiscoveryAgent);
    if (d->isActive) {
        d->discoveryThread->slotCancel();
        d->discoveryThread->disconnect(SIGNAL(finished()));
        d->discoveryThread->disconnect(SIGNAL(serviceDiscovered(CBluetoothServiceInfo)));
    }
}
