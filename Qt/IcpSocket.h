#ifndef ICPSOCKET_H
#define ICPSOCKET_H

#include <QObject>
#include <QTcpSocket>

#include "IcpAddress.h"
#include "CBluetoothSocket.h"

namespace icp {

class IcpSocket
{
public:
    IcpSocket(const IcpSocket& socket) : m_pSocket(socket.m_pSocket), m_Address(socket.m_Address), m_Protocol(socket.m_Protocol) {}

    IcpSocket(QTcpSocket*       pSocket) : m_pSocket(pSocket), m_Address(pSocket->peerAddress()), m_Protocol(IcpAddress::IpV4)      {}
    IcpSocket(CBluetoothSocket* pSocket) : m_pSocket(pSocket), m_Address(pSocket->peerAddress()), m_Protocol(IcpAddress::Bluetooth) {}

    IcpAddress::Protocol protocol() const {return m_Protocol;}

    const IcpAddress& peerAddress() const {return m_Address;}

    void abort();
    void disconnectSlots() {m_pSocket->disconnect();}

    inline operator QTcpSocket*()       {return m_Protocol == IcpAddress::IpV4      ? (QTcpSocket*)       m_pSocket : NULL;}
    inline operator CBluetoothSocket*() {return m_Protocol == IcpAddress::Bluetooth ? (CBluetoothSocket*) m_pSocket : NULL;}

private:
    QObject*             m_pSocket;
    IcpAddress           m_Address;
    IcpAddress::Protocol m_Protocol;
};

}

#endif // ICPSOCKET_H
