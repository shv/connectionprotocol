#include "IcpException.h"

using namespace icp;

///----------------------------------------------------------------------------
IcpException::IcpException(const QString& qsErrorString) :
    QException(),
    m_qsErrorString(qsErrorString)
{
}
