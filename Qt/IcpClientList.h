#ifndef ICPCLIENTLIST_H
#define ICPCLIENTLIST_H

#include <QObject>
#include <QVector>
#include <QMap>
#include <QMutex>

#include <QTcpSocket>
#include <QHostAddress>

#include "CBluetoothAddress.h"

#include "IcpClient.h"

namespace icp {

class IcpClientList : public QObject
{
    Q_OBJECT
public:
    IcpClientList(QObject *parent = 0);
    ~IcpClientList();

    void append(const QString& qsName, IcpSocket* pSocket, IcpServer* pServer);
    void remove(const IcpAddress& address, bool bSilently = false);
//    void clear();

    IcpClient* at(const IcpAddress& address) const;
    IcpClient* at(int index)                 const;

    int size() const {return m_lspClients.size();}

    QString xml(int indent = -1) const;

private:
    void append(IcpClient* pClient);
    void remove(IcpClient* pClient);

    QVector<IcpClient*>          m_lspClients;
    QMap<IcpAddress, IcpClient*> m_mppClients;

    QMutex m_Mutex;
};

}

#endif // ICPCLIENTLIST_H
