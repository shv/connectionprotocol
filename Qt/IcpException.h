#ifndef ICPEXCEPTION_H
#define ICPEXCEPTION_H

#include <QException>
#include <QString>

namespace icp {

class IcpException : public QException
{
public:
    IcpException(const QString& qsErrorString);

    void raise() const {throw *this; }
    IcpException *clone() const {return new IcpException(*this);}

    QString errorString() const {return m_qsErrorString;}

private:
    QString m_qsErrorString;
};

}

#endif // ICPEXCEPTION_H
