
#include <QUuid>
#include <QThread>
#include <QMutexLocker>
#include <QDebug>

#include <winsock2.h>
#include <ws2bth.h>
#include <bthdef.h>
#include <Dbt.h>
#pragma warning (disable : 4068)
#include <BluetoothAPIs.h>

#include "CBluetoothLocalDevice.h"
#include "CBluetoothServiceInfo.h"

LPCWSTR WIN_LISTENER_CLASS_NAME = L"BLUETOOTH_WIN_LISTENER_HELPER_CLASS";

HWND                          CBluetoothLocalDevice::s_hWndListener(NULL);
QList<CBluetoothLocalDevice*> CBluetoothLocalDevice::s_lsInstances;
QMutex                        CBluetoothLocalDevice::s_Mutex;

///----------------------------------------------------------------------------
CBluetoothLocalDevice::CBluetoothLocalDevice(QObject *parent) :
    QObject(parent),
    m_hRadio(NULL),
    m_bIsDefaultAdapter(true),
    m_Info()
{
    m_hRadio = findDefaultAdapter(&m_Info);
    initWinListener(this);
}

///----------------------------------------------------------------------------
CBluetoothLocalDevice::CBluetoothLocalDevice(const CBluetoothAddress& address, QObject* parent) :
    QObject(parent),
    m_hRadio(NULL),
    m_bIsDefaultAdapter(false),
    m_Info()
{
    m_Info.setAddress(address);
    m_hRadio = findAdapter(address, &m_Info);
    initWinListener(this);
}

///----------------------------------------------------------------------------
CBluetoothLocalDevice::~CBluetoothLocalDevice()
{
    if (m_hRadio != NULL) {
        ::CloseHandle(m_hRadio);
    }

    closeWinListener(this);
}

///----------------------------------------------------------------------------
QList<CBluetoothHostInfo> CBluetoothLocalDevice::allDevices()
{
    QList<CBluetoothHostInfo> devices = QList<CBluetoothHostInfo>();

    HANDLE                      hRadio;
    BLUETOOTH_RADIO_INFO        radioInfo       = {sizeof(BLUETOOTH_RADIO_INFO)};
    BLUETOOTH_FIND_RADIO_PARAMS findRadioParams = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};

    HBLUETOOTH_RADIO_FIND hRadioFind = ::BluetoothFindFirstRadio(&findRadioParams, &hRadio);

    if (hRadioFind != NULL) {
        do {
            if (::BluetoothGetRadioInfo(hRadio, &radioInfo) == ERROR_SUCCESS) {
                CBluetoothHostInfo info = CBluetoothHostInfo();
                info.setName(QString::fromWCharArray(radioInfo.szName));
                info.setAddress(CBluetoothAddress(radioInfo.address.ullLong));
                devices.append(info);
            }
        } while(::BluetoothFindNextRadio(hRadioFind, &hRadio));
        ::BluetoothFindRadioClose(hRadioFind);
    }

    if (hRadio != NULL) {
        ::CloseHandle(hRadio);
        hRadio  = NULL;
    }

    return devices;
}

///----------------------------------------------------------------------------
CBluetoothLocalDevice::HostMode CBluetoothLocalDevice::hostMode() const
{
    HostMode mode = HostPoweredOff;
    if (m_hRadio != NULL) {
        if(::BluetoothIsConnectable(m_hRadio)) {
            mode = HostConnectable;
        } else if (::BluetoothIsDiscoverable(m_hRadio)) {
            mode = HostDiscoverable;
        }
    }

    return mode;
}

///----------------------------------------------------------------------------
QList<CBluetoothAddress> CBluetoothLocalDevice::connectedDevices() const
{
    QList<CBluetoothAddress> devices = QList<CBluetoothAddress>();

    if (m_hRadio != NULL) {
        HANDLE                         hFindDevice;
        BLUETOOTH_DEVICE_INFO          deviceInfo         = {sizeof(BLUETOOTH_DEVICE_INFO)};
        BLUETOOTH_DEVICE_SEARCH_PARAMS deviceSearchParams = {sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS)};
        deviceSearchParams.fReturnAuthenticated = FALSE;
        deviceSearchParams.fReturnRemembered    = FALSE;
        deviceSearchParams.fReturnUnknown       = FALSE;
        deviceSearchParams.fReturnConnected     = TRUE;
        deviceSearchParams.fIssueInquiry        = FALSE;
        deviceSearchParams.cTimeoutMultiplier   = 1;
        deviceSearchParams.hRadio               = m_hRadio;

        hFindDevice = ::BluetoothFindFirstDevice(&deviceSearchParams, &deviceInfo);
        if (hFindDevice != NULL) {
            do {
                CBluetoothAddress address = CBluetoothAddress(deviceInfo.Address.ullLong);
                devices.append(address);
            } while(::BluetoothFindNextDevice(hFindDevice, &deviceInfo));
            ::BluetoothFindDeviceClose(hFindDevice);
        }
    }

    return devices;
}

/////----------------------------------------------------------------------------
//QList<CBluetoothAddress> CBluetoothLocalDevice::pairedDevices() const
//{
//    QList<CBluetoothAddress> devices = QList<CBluetoothAddress>();

//    if (m_hRadio != NULL) {
//        HANDLE                         hFindDevice;
//        BLUETOOTH_DEVICE_INFO          deviceInfo         = {sizeof(BLUETOOTH_DEVICE_INFO)};
//        BLUETOOTH_DEVICE_SEARCH_PARAMS deviceSearchParams = {sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS)};
//        deviceSearchParams.fReturnAuthenticated = TRUE;
//        deviceSearchParams.fReturnRemembered    = FALSE;
//        deviceSearchParams.fReturnUnknown       = FALSE;
//        deviceSearchParams.fReturnConnected     = TRUE;
//        deviceSearchParams.fIssueInquiry        = FALSE;
//        deviceSearchParams.cTimeoutMultiplier   = 1;
//        deviceSearchParams.hRadio               = m_hRadio;

//        hFindDevice = ::BluetoothFindFirstDevice(&deviceSearchParams, &deviceInfo);
//        if (hFindDevice != NULL) {
//            do {
//                CBluetoothAddress address = CBluetoothAddress(deviceInfo.Address.ullLong);
//                devices.append(address);
//            } while(::BluetoothFindNextDevice(hFindDevice, &deviceInfo));
//            ::BluetoothFindDeviceClose(hFindDevice);
//        }
//    }

//    return devices;
//}

///----------------------------------------------------------------------------
void CBluetoothLocalDevice::interfaceArrival()
{
    HANDLE             hRadio(NULL);
    CBluetoothHostInfo info;

    if (isValid()) {
        hRadio = findAdapter(address(), &info);
        if (hRadio == NULL) {
            if (!m_bIsDefaultAdapter) {
                info.setAddress(address());
            }
            m_Info = info;

            ::CloseHandle(m_hRadio);
            m_hRadio = NULL;

            emit hostModeStateChanged(hostMode());
        }
    }

    if (!isValid()) {
        hRadio = m_bIsDefaultAdapter ? findDefaultAdapter(&info) : findAdapter(m_Info.address(), &info);
        if (hRadio != NULL) {
            m_hRadio = hRadio;
            m_Info   = info;

            emit hostModeStateChanged(hostMode());
            qDebug() << "interfaceArrival(): " << m_Info.address().toString();
        }
    }
}

///----------------------------------------------------------------------------
void CBluetoothLocalDevice::interfaceRemove()
{
    qDebug() << "interfaceRemove(): ";
    if (isValid()) {
        HANDLE             hRadio(NULL);
        CBluetoothHostInfo info;

        hRadio = m_bIsDefaultAdapter ? findDefaultAdapter(&info) : findAdapter(address(), &info);
        if (hRadio == NULL) {
            if (!m_bIsDefaultAdapter) {
                info.setAddress(m_Info.address());
            }
            m_Info = info;

            ::CloseHandle(m_hRadio);
            m_hRadio = NULL;

            emit hostModeStateChanged(hostMode());
        }
    }
}

///----------------------------------------------------------------------------
HANDLE CBluetoothLocalDevice::findDefaultAdapter(CBluetoothHostInfo* pInfo)
{
    HANDLE hRadio(NULL);

    BLUETOOTH_RADIO_INFO        radioInfo       = {sizeof(BLUETOOTH_RADIO_INFO)};
    BLUETOOTH_FIND_RADIO_PARAMS findRadioParams = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};

    HBLUETOOTH_RADIO_FIND hRadioFind = ::BluetoothFindFirstRadio(&findRadioParams, &hRadio);

    bool bError = hRadioFind == NULL;
    if (!bError) {
        bError = ::BluetoothGetRadioInfo(hRadio, &radioInfo) != ERROR_SUCCESS;
        if (!bError) {
            pInfo->setName(QString::fromWCharArray(radioInfo.szName));
            pInfo->setAddress(CBluetoothAddress(radioInfo.address.ullLong));
        }
        ::BluetoothFindRadioClose(hRadioFind);
    }

    if (bError) {
        if (hRadio != NULL) {
            ::CloseHandle(hRadio);
        }
        hRadio  = NULL;
    }

    return hRadio;
}

///----------------------------------------------------------------------------
HANDLE CBluetoothLocalDevice::findAdapter(const CBluetoothAddress& address, CBluetoothHostInfo* pInfo)
{
    HANDLE hRadio(NULL);
    quint64 uiAddress = address.toUInt64();

    BLUETOOTH_RADIO_INFO        radioInfo       = {sizeof(BLUETOOTH_RADIO_INFO)};
    BLUETOOTH_FIND_RADIO_PARAMS findRadioParams = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};

    HBLUETOOTH_RADIO_FIND hRadioFind = ::BluetoothFindFirstRadio(&findRadioParams, &hRadio);

    bool bError = hRadioFind == NULL;
    bool bFound = false;
    if (!bError) {
        do {
            bError = ::BluetoothGetRadioInfo(hRadio, &radioInfo) != ERROR_SUCCESS;
            if (!bError) {
                bFound = radioInfo.address.ullLong == uiAddress;
            }
        } while(!bFound && ::BluetoothFindNextRadio(hRadioFind, &hRadio));
        ::BluetoothFindRadioClose(hRadioFind);
    }

    if (bFound) {
        pInfo->setAddress(address);
        pInfo->setName(QString::fromWCharArray(radioInfo.szName));

    } else if (hRadio != NULL) {
        ::CloseHandle(hRadio);
        hRadio  = NULL;
    }

    return hRadio;
}

///----------------------------------------------------------------------------
LRESULT CALLBACK CBluetoothLocalDevice::winMessagesHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (message == WM_DEVICECHANGE) {
        DEV_BROADCAST_HDR* hdr = (DEV_BROADCAST_HDR*) lParam;

        switch (wParam) {
        case DBT_DEVICEARRIVAL: {
            if (hdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
                DEV_BROADCAST_DEVICEINTERFACE* dif = (DEV_BROADCAST_DEVICEINTERFACE*) lParam;

                if (dif->dbcc_classguid == GUID_BTHPORT_DEVICE_INTERFACE) {
                    QMutexLocker locker(&s_Mutex);
                    for (int i = 0; i < s_lsInstances.size(); ++i) {
                        s_lsInstances.at(i)->interfaceArrival();
                    }
                }
            }
        }
            break;

        case DBT_DEVICEREMOVECOMPLETE: {
            if  (hdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
                DEV_BROADCAST_DEVICEINTERFACE* dif = (DEV_BROADCAST_DEVICEINTERFACE*) lParam;

                if (dif->dbcc_classguid == GUID_BTHPORT_DEVICE_INTERFACE) {
                    QMutexLocker locker(&s_Mutex);
                    for (int i = 0; i < s_lsInstances.size(); ++i) {
                        s_lsInstances.at(i)->interfaceRemove();
                    }
                }
            }
        }
            break;

        case DBT_CUSTOMEVENT: {
            if (hdr->dbch_devicetype == DBT_DEVTYP_HANDLE) {
                DEV_BROADCAST_HANDLE* dh = (DEV_BROADCAST_HANDLE*) lParam;

                if (dh->dbch_eventguid == GUID_BLUETOOTH_RADIO_IN_RANGE) {
                    BTH_RADIO_IN_RANGE* rir = (BTH_RADIO_IN_RANGE*) dh->dbch_data;

                    if (rir->deviceInfo.flags & BDIF_ADDRESS) {
                        bool bIsPaired  = rir->deviceInfo.flags    & BDIF_PAIRED;
                        bool bWasPaired = rir->previousDeviceFlags & BDIF_PAIRED;

                        if (bIsPaired ^ bWasPaired) {
                            CBluetoothAddress address(rir->deviceInfo.address);
                            QMutexLocker locker(&s_Mutex);
                            if (bIsPaired) {
                                for (int i = 0; i < s_lsInstances.size(); ++i) {
                                    emit s_lsInstances.at(i)->deviceConnected(address);
                                }

                            } else {
                                for (int i = 0; i < s_lsInstances.size(); ++i) {
                                    emit s_lsInstances.at(i)->deviceDisconnected(address);
                                }
                            }
                        }
                    }
                }
            }
        }
            break;

        default:
            break;
        }
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

///----------------------------------------------------------------------------
void CBluetoothLocalDevice::initWinListener(CBluetoothLocalDevice* pInstance)
{
    QMutexLocker locker(&s_Mutex);
    if (s_hWndListener == NULL) {
        qRegisterMetaType<CBluetoothAddress>("CBluetoothAddress");
        qRegisterMetaType<CBluetoothLocalDevice::Error>(   "CBluetoothLocalDevice::Error");
        qRegisterMetaType<CBluetoothLocalDevice::HostMode>("CBluetoothLocalDevice::HostMode");
        qRegisterMetaType<CBluetoothLocalDevice::Pairing>( "CBluetoothLocalDevice::Pairing");
        qRegisterMetaType<CBluetoothServiceInfo>("CBluetoothServiceInfo");

        WNDCLASSEX wx = {};
        wx.cbSize = sizeof(WNDCLASSEX);
        wx.lpfnWndProc   = winMessagesHandler;
        wx.lpszClassName = WIN_LISTENER_CLASS_NAME;
        if (RegisterClassEx(&wx)) {
//            s_hWndListener = ::CreateWindowEx(0, WIN_LISTENER_CLASS_NAME, L"BluetoothWinListenerHelper", 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);
            s_hWndListener = ::CreateWindowEx(0, WIN_LISTENER_CLASS_NAME, L"BluetoothWinListenerHelper", 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);

            if (s_hWndListener == NULL) {
                qDebug() << "Failed 'CreateWindowEx' with error:" << ::GetLastError();

            } else {
                DEV_BROADCAST_DEVICEINTERFACE filter = {sizeof(DEV_BROADCAST_DEVICEINTERFACE)};
                filter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
                filter.dbcc_classguid  = GUID_BTHPORT_DEVICE_INTERFACE;

                if (::RegisterDeviceNotification(s_hWndListener, (LPVOID) &filter, DEVICE_NOTIFY_WINDOW_HANDLE) == NULL) {
                    qDebug() << "Failed 'RegisterDeviceNotification' with error:" << ::GetLastError();
                }

                DEV_BROADCAST_HANDLE filter2 = {sizeof(DEV_BROADCAST_HANDLE)};
                filter2.dbch_devicetype = DBT_DEVTYP_HANDLE;
                filter2.dbch_handle  = pInstance->m_hRadio;

                if (::RegisterDeviceNotification(s_hWndListener, (LPVOID) &filter2, DEVICE_NOTIFY_WINDOW_HANDLE) == NULL) {
                    qDebug() << "Failed 'RegisterDeviceNotification' with error:" << ::GetLastError();
                }
            }
        }
    }

    s_lsInstances.append(pInstance);
}

///----------------------------------------------------------------------------
void CBluetoothLocalDevice::closeWinListener(CBluetoothLocalDevice* pInstance)
{
    QMutexLocker locker(&s_Mutex);
    s_lsInstances.removeAll(pInstance);

    if (s_lsInstances.size() == 0) {
        if (!::DestroyWindow(s_hWndListener)) {
            qDebug() << "Failed 'DestroyWindow' with error:" << GetLastError();
        }
        s_hWndListener = NULL;

        if(!::UnregisterClass(WIN_LISTENER_CLASS_NAME, NULL)) {
            qDebug() << "Failed 'UnregisterClass' with error:" << GetLastError();
        }
    }
}
