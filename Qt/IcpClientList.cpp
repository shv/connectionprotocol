#include <assert.h>
#include <QDomDocument>

#include "IcpClientList.h"
#include "CBluetoothSocket.h"

using namespace icp;

///----------------------------------------------------------------------------
IcpClientList::IcpClientList(QObject* parent) :
    QObject(parent)
{
}

///----------------------------------------------------------------------------
IcpClientList::~IcpClientList()
{
}

/////----------------------------------------------------------------------------
void IcpClientList::append(const QString& qsName, IcpSocket* pSocket, IcpServer* pServer)
{
    append(new IcpClient(qsName, pSocket, pServer, this));
}

///----------------------------------------------------------------------------
void IcpClientList::append(IcpClient* pClient)
{
    assert(pClient != NULL);
    IcpAddress address = pClient->address();
    assert(m_mppClients.constFind(address) == m_mppClients.cend());

    m_lspClients.append(pClient);
    m_mppClients.insert(address, pClient);
}

///----------------------------------------------------------------------------
void IcpClientList::remove(IcpClient *pClient)
{
    if (pClient == NULL) return;
    IcpAddress address = pClient->address();
    assert(m_mppClients.constFind(address) != m_mppClients.cend());

    QMutexLocker locker(&m_Mutex);

    int index = m_lspClients.lastIndexOf(pClient);
    while (index >= 0) {
        m_lspClients.remove(index);
        index = m_lspClients.lastIndexOf(pClient);
    }
    m_mppClients.remove(address);

    delete pClient;
}

///----------------------------------------------------------------------------
void IcpClientList::remove(const IcpAddress& address, bool bSilently)
{
    IcpClient* pClient = at(address);
    qDebug() << "IcpClientList::remove(): pClient =" << pClient;
    if (pClient != NULL) {
        if (bSilently) pClient->socket()->disconnectSlots();
        remove(pClient);
    }
}

/////----------------------------------------------------------------------------
//void IcpClientList::clear()
//{
//    QMutexLocker locker(&m_Mutex);

//    QVector<IcpClient*>::const_iterator it = m_lspClients.cbegin();
//    for (; it != m_lspClients.cend(); ++it) delete *it;

//    m_lspClients.clear();
//    m_mppClients.clear();
//}

///----------------------------------------------------------------------------
IcpClient* IcpClientList::at(const IcpAddress& address) const
{
    QMap<IcpAddress, IcpClient*>::const_iterator it = m_mppClients.constFind(address);
    if (it == m_mppClients.cend()) return NULL;
    return it.value();
}

///----------------------------------------------------------------------------
IcpClient* IcpClientList::at(int index) const
{
    return m_lspClients.at(index);
}

///----------------------------------------------------------------------------
QString IcpClientList::xml(int indent) const
{
    QDomDocument doc;
    QDomProcessingInstruction pi = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\" ");
    doc.insertBefore(pi, QDomNode());

    QDomElement  list = doc.createElement("device_list");
    doc.appendChild(list);

    QMap<IcpAddress, IcpClient*>::const_iterator it = m_mppClients.cbegin();
    for (; it != m_mppClients.cend(); ++it) {
        IcpClient* pClient = it.value();

        QString qsType;
        switch (pClient->protocol()) {
        case IcpAddress::IpV4:
            qsType = "ip";
            break;
        case IcpAddress::Bluetooth:
            qsType = "bluetooth";
            break;
        default:
            qsType = "unknown";
            break;
        }

        QDomElement client = doc.createElement("device");
        client.setAttribute("name"   , pClient->name());
        client.setAttribute("id"     , it.key().toUInt64());
        client.setAttribute("type"   , qsType);
        client.setAttribute("address", QString(pClient->address()));

        list.appendChild(client);
    }

    qDebug() << "IcpClientList::xml(): \n" + doc.toString(true);
    return doc.toString(indent);
}
