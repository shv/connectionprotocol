#include <QStringList>
#include <QDebug>

#include "CBluetoothAddress.h"

///----------------------------------------------------------------------------
CBluetoothAddress::CBluetoothAddress() :
    m_qsAddress("00:00:00:00:00:00"),
    m_uiAddress(0)
{
}

///----------------------------------------------------------------------------
CBluetoothAddress::CBluetoothAddress(quint64 address) :
    m_qsAddress("00:00:00:00:00:00"),
    m_uiAddress(address)
{
    QString strAddress = "";
    quint64 tmpAddress = address;
    for (int i = 0; i < 6; i++) {
        strAddress = QString(":%1").arg(0xFF & tmpAddress, 2, 16, QChar('0')) + strAddress;
        tmpAddress = tmpAddress >> 8;
    }
    m_qsAddress = strAddress.mid(1);
    m_uiAddress = address;
//    qDebug() << "CBluetoothAddress::CBluetoothAddress(quint64 address): " << QString("%1").arg(m_uiAddress);
}

///----------------------------------------------------------------------------
CBluetoothAddress::CBluetoothAddress(const QString& qsAddress) :
    m_qsAddress("00:00:00:00:00:00"),
    m_uiAddress(0)
{
    quint64 uiAddress = 0;

    QStringList parts = qsAddress.split(":");
    bool ok = parts.size() == 6;
    if (ok) {
        for (int i = 0; i < parts.size(); ++i) {
            uiAddress = (uiAddress << 8) | parts.at(i).toUShort(&ok, 16);
            if (!ok) break;
        }
    }

    if (ok) {
        m_qsAddress = qsAddress;
        m_uiAddress = uiAddress;
//        qDebug() << "CBluetoothAddress::CBluetoothAddress(const QString& qsAddress): " << QString("%1").arg(m_uiAddress);
    }
}

///----------------------------------------------------------------------------
CBluetoothAddress::CBluetoothAddress(const CBluetoothAddress& address)
{
    *this = address;
}

///----------------------------------------------------------------------------
void CBluetoothAddress::clear()
{
    m_qsAddress = "00:00:00:00:00:00";
    m_uiAddress = 0;
}

///----------------------------------------------------------------------------
bool CBluetoothAddress::isNull() const
{
    return m_uiAddress == 0;
}

///----------------------------------------------------------------------------
QString CBluetoothAddress::toString() const
{
    return m_qsAddress;
}

///----------------------------------------------------------------------------
quint64 CBluetoothAddress::toUInt64() const
{
    return m_uiAddress;
}

///----------------------------------------------------------------------------
CBluetoothAddress& CBluetoothAddress::operator=(const CBluetoothAddress& right)
{
    if (this == &right) return *this;
    m_qsAddress = right.m_qsAddress;
    m_uiAddress = right.m_uiAddress;
    return *this;
}

///----------------------------------------------------------------------------
bool CBluetoothAddress::operator<(const CBluetoothAddress& right) const
{
    return m_uiAddress < right.m_uiAddress;
}

///----------------------------------------------------------------------------
bool CBluetoothAddress::operator!=(const CBluetoothAddress& right) const
{
    return m_uiAddress != right.m_uiAddress;
}

///----------------------------------------------------------------------------
bool CBluetoothAddress::operator==(const CBluetoothAddress& right) const
{
    return m_uiAddress == right.m_uiAddress;
}
