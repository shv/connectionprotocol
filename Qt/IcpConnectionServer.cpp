#include <QHostInfo>
#include <QStringList>
#include <QFile>
#include <QTimer>
#include <QEventLoop>

#include "IcpConnectionServer.h"

#ifdef ENABLE_BLUETOOTH
#include "IcpServerBluetooth.h"
#endif

#include "IcpServerIp.h"
#include "IcpException.h"

using namespace icp;

const QByteArray IcpConnectionServer::MAGIC_CLIENT = QString("ICP#Clnt").toUtf8();
const QByteArray IcpConnectionServer::MAGIC_SERVER = QString("ICP#Srvr").toUtf8();

const QString TAG = QString("IcpConnectionServer::");

#ifndef QT_NO_DEBUG_OUTPUT
void debug_log(const QByteArray& ba, QString qsLogName = "debug.log")
{
    QFile logFile("C:\\QTBuild\\" + qsLogName);
    if (logFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        logFile.write(ba);
        logFile.close();
    }
}
#endif

///----------------------------------------------------------------------------
IcpConnectionServer::IcpConnectionServer(qint16 iFlavour, QObject *parent /*= 0*/) :
    QObject(parent),
    m_iFlavour((qint32) iFlavour << 16),
    m_qsName(QHostInfo::localHostName()),
    m_iIpPort(0),
    m_qsOptionalIP(),
    m_BthServiceUUID("{00000000-0000-0000-0000-000000000000}"),
    m_BthServiceName(""),
    m_bStarted(false),
    m_qsErrorString("")
{
    m_pClients = new IcpClientList(this);

    m_Servers.insert(IcpAddress::IpV4     , new IcpServerIp(m_qsName, this));
#ifdef ENABLE_BLUETOOTH
    m_Servers.insert(IcpAddress::Bluetooth, new IcpServerBluetooth(m_qsName, this));
#endif

    foreach (IcpServer* pSvr, m_Servers) {
        connect(pSvr, SIGNAL(dataReceived(IcpAddress, QByteArray)), SLOT(slotRead(IcpAddress, QByteArray)));
        connect(pSvr, SIGNAL(clientDisconnected(IcpAddress))      , SLOT(slotDisconnection(IcpAddress)));
    }
}

///----------------------------------------------------------------------------
IcpConnectionServer::~IcpConnectionServer()
{
    if (isRunning()) stop();
}

///----------------------------------------------------------------------------
void IcpConnectionServer::start()
{
    qDebug() << "IcpConnectionServer::start";
    if (isRunning()) return;

    m_bStarted = true;
    m_qsErrorString = "";

    Parameters params = getParameters();

    QString qsSep = "\n";
    bool bStarted = false;
    foreach (IcpServer* pSvr, m_Servers) {
        try {
            pSvr->start(params);
            if (pSvr->isStarted()) {
//                pSvr->sendHandshake(IcpAddress::Null());
                bStarted = true;
            }

        } catch (IcpException &e) {
            m_qsErrorString += m_qsErrorString + qsSep + e.errorString();
        }
    }

    if (bStarted) {
        m_qsErrorString = "";

    } else {
        m_qsErrorString = m_qsErrorString.mid(qsSep.length());
        m_bStarted = false;

        emit error();
    }
}

///----------------------------------------------------------------------------
void IcpConnectionServer::stop()
{
    qDebug() << "IcpConnectionServer::stop";
    if (!m_bStarted) return;
    m_bStarted = false;

    // Specially for HTC phones
    for (int i = 0; i < m_pClients->size(); ++i) {
        m_pClients->at(i)->sendGoodbye();
    }

    foreach (IcpServer* pSvr, m_Servers) {
//        pSvr->sendGoodbye(IcpAddress::Null());
        pSvr->stop();
    }
}

///----------------------------------------------------------------------------
Parameters IcpConnectionServer::getParameters()
{
    Parameters params = {m_iIpPort, m_qsOptionalIP, m_BthServiceUUID, m_BthServiceName};
    return params;
}

///----------------------------------------------------------------------------
void IcpConnectionServer::setParameters(Parameters params)
{
    bool bRestartIPServer = false;

    if (params.ipPort != m_iIpPort) {
        m_iIpPort = params.ipPort;
        bRestartIPServer = true;
    }

    if (params.optionalIP != m_qsOptionalIP) {
        m_qsOptionalIP = params.optionalIP;
        bRestartIPServer = true;
    }

    if (bRestartIPServer) {
        IcpServer* pSvr = m_Servers.value(IcpAddress::IpV4);
        if (pSvr != NULL && pSvr->isStarted()) {
            pSvr->stop();
            pSvr->start(params);
            m_bStarted = m_bStarted || pSvr->isStarted();
        }
    }

    if (!params.bthServiceUUID.isNull() && params.bthServiceUUID != m_BthServiceUUID) {
        m_BthServiceUUID = params.bthServiceUUID;

        if (!params.bthServiceName.isEmpty() && params.bthServiceName != m_BthServiceName) {
            m_BthServiceName = params.bthServiceName;
        }

        IcpServer* pSvr = m_Servers.value(IcpAddress::Bluetooth);
        if (pSvr != NULL && pSvr->isStarted()) {
            pSvr->stop();
            pSvr->start(params);
            m_bStarted = m_bStarted || pSvr->isStarted();
        }
    }
}

///----------------------------------------------------------------------------
void IcpConnectionServer::sendData(IcpAddress address, const QByteArray& baData)
{
    qDebug() << "sendData(): isRunning" << isRunning();
    if (!isRunning()) return;

    IcpClient* pClient = m_pClients->at(address);
    qDebug() << "sendData(): pClient = " << pClient->name();
    if (pClient == NULL) return;

    pClient->sendData(baData);
}

///----------------------------------------------------------------------------
bool IcpConnectionServer::isRunning() const
{
    foreach (IcpServer* pSvr, m_Servers) {
        if (pSvr->isActive()) return true;
    }
//    return false;
    return m_pClients->size() > 0;
}

///----------------------------------------------------------------------------
bool IcpConnectionServer::waitForStart()
{
    return waitFor(true);
}

///----------------------------------------------------------------------------
bool IcpConnectionServer::waitForStop()
{
    return waitFor(false);
}

///----------------------------------------------------------------------------
void IcpConnectionServer::slotStart()
{
    start();
}

///----------------------------------------------------------------------------
void IcpConnectionServer::slotStop()
{
    stop();
}

///----------------------------------------------------------------------------
void IcpConnectionServer::slotSendData(quint64 address, const QByteArray &baData)
{
    sendData(IcpAddress(address), baData);
}

///----------------------------------------------------------------------------
void IcpConnectionServer::slotRead(const IcpAddress& address, const QByteArray& baDatagram)
{
    IcpClient* pClient = m_pClients->at(address);
    IcpServer* pServer = (IcpServer*) sender();

    qint32 iCode;
    qint32 iDataLength;
    QByteArray baData = unwrapMessage(baDatagram, &iCode, &iDataLength);

    qDebug() << "IcpConnectionServer::slotRead" << QString(address) << "code =" << iCode << "size =" << iDataLength << ":" << QString(baData);

    if (baData.size() != iDataLength) return;

    switch (iCode) {
    case MSG_ONLINE: {
        qDebug() << "MSG_ONLINE: " << QString(baData);
        if (pClient != NULL) {
            if (iDataLength > 4) {
                qint32 iInterval = -1;

                QByteArray  baInterval = baData.left(4);
                QDataStream dsInterval(&baInterval, QIODevice::ReadOnly);
                dsInterval.setVersion(QDataStream::Qt_5_1);
                dsInterval >> iInterval;

//                QString qsClientName = QString::fromUtf8(baDatagram.mid(HEADER_LENGTH + 4));
                if (iInterval > 0) {
                    pClient->startTimeoutTimer(iInterval * MISSED_ONLINE_CHECKS_LIMIT);
                    pServer->sendOnlineNotification(address);
                }
            }

        } else {
            pServer->sendOnlineNotification(address);
        }
    }
        break;

    case MSG_HANDSHAKE:
        qDebug() << "MSG_HANDSHAKE: " << QString(baData);
        pServer->sendHandshake(address);
        break;

    case MSG_DATA: {
        qDebug() << "MSG_DATA: " << QString(baData);
        emit data((quint64)address, baData);
    }
        break;

    case MSG_CONNECT: {
        qDebug() << "MSG_CONNECT, " << QString(baData);
        IcpSocket* pPendingSocket = pServer->takePendingSocket(address);
        if (pPendingSocket != NULL) {
            if (pClient != NULL) {
                m_pClients->remove(address, true);
            }

            m_pClients->append(QString(baData), pPendingSocket, pServer);

            emit connection();
        }
    }
        break;
    }
}

///----------------------------------------------------------------------------
void IcpConnectionServer::slotDisconnection(const IcpAddress& address)
{
    qDebug() << "IcpConnectionServer::slotDisconnection():" << address.toString();
    m_pClients->remove(address);
    emit disconnection();
}

///----------------------------------------------------------------------------
QByteArray IcpConnectionServer::wrapMessage(const QByteArray& baMessage, qint32 iMessageCode, bool bPrependSize)
{
    static int iMagicLength = MAGIC_SERVER.size();

    quint32 uiMessageSize = baMessage.size();
    quint32 uiPacketSize = HEADER_LENGTH + uiMessageSize;

    QByteArray baDatagram((bPrependSize ? sizeof(quint32) : 0) + uiPacketSize, '\0');
    QDataStream dsDatagram(&baDatagram, QIODevice::WriteOnly);
    dsDatagram.setVersion(QDataStream::Qt_5_1);

    if (bPrependSize) {
        dsDatagram << quint32(uiPacketSize);
    }
    // header
    dsDatagram.writeRawData(MAGIC_SERVER.data(), iMagicLength);
    dsDatagram << (m_iFlavour | iMessageCode);
    dsDatagram << uiMessageSize;
    // data
    dsDatagram.writeRawData(baMessage, uiMessageSize);

    return baDatagram;
}

///----------------------------------------------------------------------------
qint32 IcpConnectionServer::unwrapServiceMessage(const QByteArray& magic, const QByteArray& baDatagram)
{
    qint32     iCode       = -1;
    QByteArray baMessage;

    if ((baDatagram.size() >= MAGIC_LENGTH) && baDatagram.startsWith(magic)) {
        QByteArray baData = baDatagram.mid(MAGIC_LENGTH);
        QDataStream dsData(&baData, QIODevice::ReadOnly);
        dsData.setVersion(QDataStream::Qt_5_1);

        qint32 iTmpCode = 0;
        dsData >> iTmpCode;
        if ((iTmpCode & m_iFlavour) == m_iFlavour) {
            iCode = iTmpCode & 0xffff;
        }
    }

    return iCode;
}

///----------------------------------------------------------------------------
QByteArray IcpConnectionServer::unwrapMessage(const QByteArray& baDatagram, qint32* piMessageCode, qint32* piMessageSize)
{
    qint32     iCode       = MSG_INVALID;
    qint32     iDataLength = 0;
    QByteArray baMessage;

    if ((baDatagram.size() >= HEADER_LENGTH) && baDatagram.startsWith(MAGIC_CLIENT)) {
        QByteArray baHeaderData = baDatagram.mid(MAGIC_LENGTH, 8);
        QDataStream dsHeaderData(&baHeaderData, QIODevice::ReadOnly);
        dsHeaderData.setVersion(QDataStream::Qt_5_1);

        qint32 iTmpCode = 0;
        dsHeaderData >> iTmpCode;
        if ((iTmpCode & m_iFlavour) == m_iFlavour) {
            iCode = iTmpCode & 0xffff;
            dsHeaderData >> iDataLength;

            if (iDataLength > 0) {
                baMessage = baDatagram.mid(HEADER_LENGTH, iDataLength);
            }
        }
    }

    *piMessageCode = iCode;
    *piMessageSize = iDataLength;

    return baMessage;
}

///----------------------------------------------------------------------------
bool IcpConnectionServer::waitFor(bool bStart)
{
    QEventLoop loop;
    int i = 0;
    int limit = START_STOP_TIMEOUT / 100;
    while (i < limit && (bStart != isRunning()) ) {
        QTimer::singleShot(100, &loop, SLOT(quit()));
        loop.exec();
        ++i;
    }
    return (bStart == isRunning());
}

