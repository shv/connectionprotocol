#include "CBluetoothServer.h"

#include <initguid.h>
#include <winsock2.h>
#include <ws2bth.h>

#include "private/qobject_p.h"
#include <QQueue>


#include "CBluetoothLocalDevice.h"
#include "CBluetoothSocketEngine_p.h"

#define Q_CHECK_SOCKETENGINE(returnValue) do { \
    if (!d->socketEngine) { \
        return returnValue; \
    } } while (0)


///----------------------------------------------------------------------------
/// CBluetoothServerPrivate
///----------------------------------------------------------------------------
class CBluetoothServerPrivate : public QObjectPrivate, public CAbstractSocketEngineReceiver
{
    Q_DECLARE_PUBLIC(CBluetoothServer)
public:
    explicit CBluetoothServerPrivate();
    virtual ~CBluetoothServerPrivate();

    QQueue<CBluetoothSocket*> pendingConnections;

    qintptr           socketDescriptor;
    QUuid             serviceId;
    CBluetoothAddress address;
    quint32           port;

    CBluetoothSocket::SocketState state;
    CBluetoothServiceInfo::Protocol serverType;

    int maxConnections;

    CBluetoothServer::Error serverSocketError;
    QString                 serverSocketErrorString;

    CBluetoothSocketEngine* socketEngine;

    void setError();

    // from CAbstractSocketEngineReceiver
    void readNotification();
    void closeNotification()             {readNotification();}
    inline void writeNotification()      {}
    inline void exceptionNotification()  {}
    inline void connectionNotification() {}
};

///----------------------------------------------------------------------------
CBluetoothServerPrivate::CBluetoothServerPrivate() :
    socketDescriptor(INVALID_SOCKET),
    address(0),
    port(0),
    state(CBluetoothSocket::UnconnectedState),
    serverType(CBluetoothServiceInfo::UnknownProtocol),
    maxConnections(4),
    serverSocketError(CBluetoothServer::NoError),
    socketEngine(NULL)
{
}

///----------------------------------------------------------------------------
CBluetoothServerPrivate::~CBluetoothServerPrivate()
{
}

///----------------------------------------------------------------------------
void CBluetoothServerPrivate::setError()
{
    switch (socketEngine->error()) {
    case CBluetoothSocketEngine::UnsupportedProtocolError:
        serverSocketError = CBluetoothServer::UnsupportedProtocolError;
        break;
    case CBluetoothSocketEngine::ServiceAlreadyRegisteredError:
        serverSocketError = CBluetoothServer::ServiceAlreadyRegisteredError;
        break;
    case CBluetoothSocketEngine::PoweredOffError:
        serverSocketError = CBluetoothServer::PoweredOffError;
        break;
    case CBluetoothSocketEngine::InputOutputError:
    case CBluetoothSocketEngine::TemporaryError:
        serverSocketError = CBluetoothServer::InputOutputError;
        break;
    default:
        serverSocketError = CBluetoothServer::UnknownError;
    }
    serverSocketErrorString = socketEngine->errorString();
}

//void CBluetoothServerPrivate::pauseAccepting()
//{
//    socketEngine->setReadNotificationEnabled(false);
//}

//void CBluetoothServerPrivate::resumeAccepting()
//{
//    socketEngine->setReadNotificationEnabled(true);
//}

///----------------------------------------------------------------------------
void CBluetoothServerPrivate::readNotification()
{
    Q_Q(CBluetoothServer);
    for (;;) {
        if (pendingConnections.count() >= maxConnections) {
            qDebug("CBluetoothServerPrivate::readNotification(): too many connections");
            if (socketEngine->isReadNotificationEnabled()) {
                socketEngine->setReadNotificationEnabled(false);
            }
            return;
        }

        qintptr descriptor = socketEngine->accept();
        if (descriptor == INVALID_SOCKET) {
            if (socketEngine->error() != CBluetoothSocketEngine::TemporaryError) {
//                socketEngine->setReadNotificationEnabled(false);
                setError();
                emit q->error(serverSocketError);
            }
            break;
        }
        qDebug("CBluetoothServerPrivate::readNotification(): accepted socket %i", descriptor);
        q->incomingConnection(descriptor);

        QPointer<CBluetoothServer> that = q;
        emit q->newConnection();
        if (!that || !q->isListening()) {
            return;
        }
    }
}


///----------------------------------------------------------------------------
/// CBluetoothServer
///----------------------------------------------------------------------------
CBluetoothServer::CBluetoothServer(CBluetoothServiceInfo::Protocol serverType, QObject* parent) :
    QObject(*new CBluetoothServerPrivate, parent)
{
    qDebug() << "CBluetoothServer::CBluetoothServer()";
    Q_D(CBluetoothServer);
    d->serverType = serverType;

//    QList<CBluetoothHostInfo> devices = CBluetoothLocalDevice::allDevices();
//    for (int i = 0; i < devices.size(); ++i) {
//        qDebug() << "CBluetoothServer():" << devices.at(i).name() << devices.at(i).address().toString();
//    }
}

///----------------------------------------------------------------------------
CBluetoothServer::~CBluetoothServer()
{
    qDebug() << "CBluetoothServer::~CBluetoothServer()";
    close();
}

///----------------------------------------------------------------------------
bool CBluetoothServer::listen(const QUuid& serviceId, const QString& serviceName)
{
    Q_D(CBluetoothServer);
    if (d->state == CBluetoothSocket::ListeningState) {
        qWarning("CBluetoothServer::listen(): called when already listening");
        return false;
    }

    delete d->socketEngine;
    d->socketEngine = new CBluetoothSocketEngine(this);

    if (!d->socketEngine->initialize(d->serverType)) {
        d->setError();
        return false;
    }

    if (!d->socketEngine->bind()) {
        d->setError();
        return false;
    }

    if (!d->socketEngine->registerService(serviceId, serviceName)) {
        d->setError();
        return false;
    }

    if (!d->socketEngine->listen()) {
        d->setError();
        return false;
    }

    qDebug() << "CBluetoothServer::listen(): 2";

    d->socketEngine->setReceiver(d);
    d->socketEngine->setReadNotificationEnabled(true);

    d->state = CBluetoothSocket::ListeningState;
    d->address   = d->socketEngine->localAddress();
    d->serviceId = d->socketEngine->serviceId();
    d->port      = d->socketEngine->localPort();

    return true;
}

///----------------------------------------------------------------------------
void CBluetoothServer::close()
{
    Q_D(CBluetoothServer);
    qDeleteAll(d->pendingConnections);
    d->pendingConnections.clear();

    if (d->socketEngine) {
        d->socketEngine->close();
        QT_TRY {
            d->socketEngine->deleteLater();
        } QT_CATCH(const std::bad_alloc &) {
            // in out of memory situations, the socketEngine
            // will be deleted in ~CBluetoothServer (it's a child-object of this)
        }
        d->socketEngine = NULL;
    }

    d->state = CBluetoothSocket::UnconnectedState;
}

///----------------------------------------------------------------------------
CBluetoothAddress CBluetoothServer::serverAddress() const
{
    return d_func()->address;
}

bool CBluetoothServer::isListening() const
{

    Q_D(const CBluetoothServer);
    Q_CHECK_SOCKETENGINE(false);
    qDebug() << "CBluetoothServer::isListening():" << (d->socketEngine->state() == CBluetoothSocket::ListeningState);
    return d->socketEngine->state() == CBluetoothSocket::ListeningState;
}

///----------------------------------------------------------------------------
bool CBluetoothServer::hasPendingConnections() const
{
    return !d_func()->pendingConnections.isEmpty();
}

///----------------------------------------------------------------------------
CBluetoothSocket* CBluetoothServer::nextPendingConnection()
{
    Q_D(CBluetoothServer);
    if (d->pendingConnections.isEmpty()) {
        return NULL;
    }

    if (!d->socketEngine->isReadNotificationEnabled()) {
        d->socketEngine->setReadNotificationEnabled(true);
    }

    return d->pendingConnections.dequeue();
}

///----------------------------------------------------------------------------
void CBluetoothServer::setMaxPendingConnections(int numConnections)
{
    d_func()->maxConnections = numConnections;
}

///----------------------------------------------------------------------------
int CBluetoothServer::maxPendingConnections() const
{
    return d_func()->maxConnections;
}

///----------------------------------------------------------------------------
CBluetoothServer::Error CBluetoothServer::error() const
{
    return d_func()->serverSocketError;
}

///----------------------------------------------------------------------------
QString CBluetoothServer::errorString() const
{
    return d_func()->serverSocketErrorString;
}

///----------------------------------------------------------------------------
void CBluetoothServer::incomingConnection(qintptr socketDescriptor)
{
    CBluetoothSocket* clientSocket = new CBluetoothSocket(this);
    clientSocket->setSocketDescriptor(socketDescriptor);
    addPendingConnection(clientSocket);
}

///----------------------------------------------------------------------------
void CBluetoothServer::addPendingConnection(CBluetoothSocket* socket)
{
    Q_D(CBluetoothServer);
    d->pendingConnections.enqueue(socket);
}
