#ifndef ICPADDRESS_H
#define ICPADDRESS_H

#include <QHostAddress>

#include "CBluetoothAddress.h"

namespace icp {

class IcpAddress
{
public:
    enum Protocol {
        IpV4,
        Bluetooth,
        Unknown
    };

    IcpAddress(const IcpAddress& address) : m_uiAddress(address.m_uiAddress), m_Protocol(address.m_Protocol) {}

    IcpAddress(const QHostAddress&      address) : m_uiAddress(address.toIPv4Address())      , m_Protocol(IpV4)      {}
    IcpAddress(const CBluetoothAddress& address) : m_uiAddress(address.toUInt64() | BTH_MASK), m_Protocol(Bluetooth) {}

    IcpAddress(quint64 address) : m_uiAddress(address), m_Protocol((address & BTH_MASK) == 0 ? IpV4 : Bluetooth) {}

    static IcpAddress Null() {return IcpAddress();}
    bool isNull() const {return m_uiAddress == 0;}

    Protocol protocol() const {return m_Protocol;}

    QString toString() const {return isNull() ? QString("NULL") : (protocol() == IpV4 ? QHostAddress(m_uiAddress).toString() : CBluetoothAddress(m_uiAddress).toString());}
    quint64 toUInt64() const {return m_uiAddress;}

    inline operator QHostAddress()      const {return QHostAddress(toUInt64());}
    inline operator CBluetoothAddress() const {return CBluetoothAddress(toUInt64() & ~BTH_MASK);}
    inline operator QString()           const {return toString();}

    inline operator quint64() const {return toUInt64();}

    friend bool operator>( const IcpAddress& left, const IcpAddress& right) {return left.m_uiAddress >  right.m_uiAddress;}
    friend bool operator>=(const IcpAddress& left, const IcpAddress& right) {return left.m_uiAddress >= right.m_uiAddress;}
    friend bool operator<( const IcpAddress& left, const IcpAddress& right) {return left.m_uiAddress <  right.m_uiAddress;}
    friend bool operator<=(const IcpAddress& left, const IcpAddress& right) {return left.m_uiAddress <= right.m_uiAddress;}
    friend bool operator==(const IcpAddress& left, const IcpAddress& right) {return left.m_uiAddress == right.m_uiAddress;}
    friend bool operator!=(const IcpAddress& left, const IcpAddress& right) {return left.m_uiAddress != right.m_uiAddress;}

private:
    IcpAddress() : m_uiAddress(0), m_Protocol(Unknown) {}

    static const quint64 BTH_MASK = 0x4000000000000000;

    quint64  m_uiAddress;
    Protocol m_Protocol;
};

}

#endif // ICPADDRESS_H
