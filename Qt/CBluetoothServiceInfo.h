#ifndef CBLUETOOTHSERVICEINFO_H
#define CBLUETOOTHSERVICEINFO_H

#include <QUuid>

#include "CBluetoothDeviceInfo.h"

class CBluetoothServiceInfo
{
public:
    CBluetoothServiceInfo();
    CBluetoothServiceInfo(const CBluetoothServiceInfo& other);
    ~CBluetoothServiceInfo();

    enum Protocol {
        UnknownProtocol,
        L2capProtocol,
        RfcommProtocol
    };

    bool isComplete() const;

    CBluetoothDeviceInfo device() const;

    QUuid   serviceUuid() const;
    QString serviceName() const;

    CBluetoothServiceInfo::Protocol socketProtocol() const;

    void setDevice(const CBluetoothDeviceInfo& device);

    void setServiceUuid(const QUuid& uuid);
    void setServiceName(const QString& name);

private:
    CBluetoothDeviceInfo m_Device;
    QUuid                m_Uuid;
    QString              m_Name;
};

#endif // CBLUETOOTHSERVICEINFO_H
