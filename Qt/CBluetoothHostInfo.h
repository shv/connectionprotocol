#ifndef CBLUETOOTHHOSTINFO_H
#define CBLUETOOTHHOSTINFO_H

#include "CBluetoothAddress.h"

class CBluetoothHostInfo
{
public:
    CBluetoothHostInfo();
    CBluetoothHostInfo(const CBluetoothHostInfo& other);
    ~CBluetoothHostInfo() {}

    CBluetoothAddress address() const {return m_Address;}
    QString           name()    const {return m_qsName;}

    void setAddress(const CBluetoothAddress& address);
    void setName(   const QString& name);

    CBluetoothHostInfo& operator=(const CBluetoothHostInfo& other);

private:
    QString           m_qsName;
    CBluetoothAddress m_Address;
};

#endif // CBLUETOOTHHOSTINFO_H
