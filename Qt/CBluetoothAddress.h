#ifndef CBLUETOOTHADDRESS_H
#define CBLUETOOTHADDRESS_H

#include <QString>

class CBluetoothAddress
{
public:
    CBluetoothAddress();
    CBluetoothAddress(quint64 address);
    CBluetoothAddress(const QString& qsAddress);
    CBluetoothAddress(const CBluetoothAddress& address);
    ~CBluetoothAddress() {}

    void clear();
    bool isNull() const;

    QString toString() const;
    quint64 toUInt64() const;

    CBluetoothAddress& operator=( const CBluetoothAddress& right);
    bool               operator<( const CBluetoothAddress& right) const;
    bool               operator!=(const CBluetoothAddress& right) const;
    bool               operator==(const CBluetoothAddress& right) const;

    operator QString() const {return toString();}

private:
    QString m_qsAddress;
    quint64 m_uiAddress;
};

#endif // CBLUETOOTHADDRESS_H
