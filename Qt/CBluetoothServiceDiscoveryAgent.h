#ifndef CBLUETOOTHSERVICEDISCOVERYAGENT_H
#define CBLUETOOTHSERVICEDISCOVERYAGENT_H

#include <QObject>
#include <QUuid>

#include "CBluetoothAddress.h"
#include "CBluetoothServiceInfo.h"

class CBluetoothServiceDiscoveryAgentPrivate;

class CBluetoothServiceDiscoveryAgent : public QObject
{
    Q_OBJECT
public:
    explicit CBluetoothServiceDiscoveryAgent(QObject* parent = 0);
//    CBluetoothServiceDiscoveryAgent(const CBluetoothAddress& deviceAdapter, QObject* parent = 0);
    ~CBluetoothServiceDiscoveryAgent() {}

    enum DiscoveryMode {
        MinimalDiscovery,
        FullDiscovery
    };

    enum Error {
        NoError,
        PoweredOffError,
        InputOutputError,
        InvalidBluetoothAdapterError,
        UnknownError
    };

    QList<CBluetoothServiceInfo> discoveredServices() const;

    bool isActive() const;

    bool setRemoteAddress(const CBluetoothAddress & address);
    CBluetoothAddress remoteAddress() const;

    Error   error()       const;
    QString errorString() const;

    void setUuidFilter(const QList<QUuid>& uuids);
    void setUuidFilter(const QUuid& uuid);

    QList<QUuid> uuidFilter() const;

signals:
    void canceled();
    void error(CBluetoothServiceDiscoveryAgent::Error error);
    void finished();
    void serviceDiscovered(const CBluetoothServiceInfo& info);

public slots:
    void clear();
    void start(DiscoveryMode mode = MinimalDiscovery);
    void stop();

private:
    Q_DISABLE_COPY(CBluetoothServiceDiscoveryAgent)
    Q_DECLARE_PRIVATE(CBluetoothServiceDiscoveryAgent)

//    Q_PRIVATE_SLOT(d_func(), void _q_canceled())
};

#endif // CBLUETOOTHSERVICEDISCOVERYAGENT_H
