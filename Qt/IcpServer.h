#ifndef ICPSERVER_H
#define ICPSERVER_H

#include <QObject>

#include "IcpAddress.h"
#include "IcpSocket.h"
//#ifndef ICPCONNECTIONSERVER_H
//#include "IcpConnectionServer.h"
//#endif

namespace icp {

struct Parameters;

class IcpServer : public QObject
{
    Q_OBJECT
public:
    explicit IcpServer(const QString& qsName, QObject *parent = 0);
    virtual ~IcpServer();

    const QString& name() const {return m_qsName;}

    virtual IcpAddress::Protocol protocol() = 0;

    virtual void start(Parameters params) = 0;
    virtual void stop()                   = 0;

    virtual bool isStarted() const {return m_bStarted;}
    virtual bool isActive()  const = 0;

    virtual void sendHandshake(const IcpAddress& address)               = 0;
    virtual void sendOnlineNotification(const IcpAddress& address)      = 0;
    virtual void sendGoodbye(const IcpAddress& address)                 = 0;
    virtual void sendData(IcpSocket* pSocket, const QByteArray& baData) = 0;

    IcpSocket* takePendingSocket(const IcpAddress& address) {return m_PendingConnections.take(address);}

signals:
    void dataReceived(const IcpAddress& address, const QByteArray& baData);
    void clientDisconnected(const IcpAddress& address);
//    void error();

protected:
    void setStarted() {m_bStarted = true;}
    void setStopped() {m_bStarted = false;}

    virtual QByteArray serviceMagic() const {return QByteArray();}

    QByteArray wrapMessage(const QByteArray& baMessage, qint32 iMessageCode, bool bPrependSize = false);
    qint32 unwrapServiceMessage(const QByteArray& baDatagram);

    void addPendingSocket(const IcpAddress& address, IcpSocket* pSocket);

private:
    QString m_qsName;

    bool m_bStarted;

    QMap<IcpAddress, IcpSocket*> m_PendingConnections;
};

}

#endif // ICPSERVER_H
