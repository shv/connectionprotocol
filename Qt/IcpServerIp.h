#ifndef ICPSERVERIP_H
#define ICPSERVERIP_H

#include <QTcpServer>
#include <QUdpSocket>

#include "IcpServer.h"

namespace icp {

class IcpServerIp : public IcpServer
{
    Q_OBJECT
private:
    static const quint32      UDP_MAX_SIZE = 1024;
    static const QHostAddress MULTICAST_GROUP;
    static const int          MULTICAST_TTL = 4;

public:
    explicit IcpServerIp(const QString& qsName, QObject *parent = 0);
    virtual ~IcpServerIp();

    virtual IcpAddress::Protocol protocol() {return IcpAddress::IpV4;}

    void start(Parameters params);
    void stop();

    bool isActive() const {return m_UdpSockets.size() > 0 && m_pServer->isListening();}

    void sendHandshake(const IcpAddress& address);
    void sendOnlineNotification(const IcpAddress& address);
    void sendGoodbye(const IcpAddress& address);
    void sendData(IcpSocket* pSocket, const QByteArray& baData);

private slots:
    void slotNewConnection();
    void slotDisconnection();

    void slotUDPRead();
    void slotTCPRead();

private:
    void sendUdp(const QByteArray& baMessage, const QHostAddress& address, int iMessageCode);
    void sendTcp(const QByteArray& baMessage,       QTcpSocket*   pSocket, int iMessageCode);

    void clearUDPSocketList();

    QVector<QPair<QUdpSocket*, int>> m_UdpSockets; // where int is a subnet mask
    QVector<QUdpSocket*>             m_MCSockets;
    QTcpServer*                      m_pServer;

    quint32      m_uiNextBlockSize;

    int          m_iIpPort;
    QHostAddress m_OptionalIP;
};

}

#endif // ICPSERVERIP_H
