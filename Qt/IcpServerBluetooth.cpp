#include "IcpServerBluetooth.h"

#include <QDataStream>

#include "CBluetoothSocket.h"

#include "IcpConnectionServer.h"
#include "IcpException.h"

using namespace icp;

const QByteArray IcpServerBluetooth::MAGIC_BLUETOOTH = QString("ICP#Blth").toUtf8();

///----------------------------------------------------------------------------
IcpServerBluetooth::IcpServerBluetooth(const QString& qsName, QObject *parent) :
    IcpServer(qsName, parent),
    m_pAgent(NULL),
    m_pServer(NULL),
    m_uiNextBlockSize(0)
{
    m_pLocalDevice = new CBluetoothLocalDevice(this);
    connect(m_pLocalDevice, SIGNAL(hostModeStateChanged(CBluetoothLocalDevice::HostMode)), SLOT(slotHostModeChanged(CBluetoothLocalDevice::HostMode)));
    connect(m_pLocalDevice, SIGNAL(deviceConnected(CBluetoothAddress))                   , SLOT(slotDeviceConnected(CBluetoothAddress)));
    connect(m_pLocalDevice, SIGNAL(deviceDisconnected(CBluetoothAddress))                , SLOT(slotDeviceDisconnected(CBluetoothAddress)));
    connect(m_pLocalDevice, SIGNAL(error(CBluetoothLocalDevice::Error))                  , SLOT(slotAdapterError(CBluetoothLocalDevice::Error)));
}

///----------------------------------------------------------------------------
IcpServerBluetooth::~IcpServerBluetooth()
{
    stop();
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::start(Parameters params)
{
    qDebug() << "IcpServerBluetooth::start()";
    if (isStarted()) return;
    setStarted();

    if (params.bthServiceUUID.isNull()) {
        setStopped();
        return;
    }

    m_ServiceUuid = params.bthServiceUUID;
    m_ServiceName = params.bthServiceName;

    innerStart(false);
    sendHandshake(IcpAddress::Null());
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::stop()
{
    qDebug() << "IcpServerBluetooth::stop()";
    sendGoodbye(IcpAddress::Null());
    setStopped();

    innerStop();
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::sendHandshake(const IcpAddress& address)
{
    qDebug() << "IcpServerBluetooth::sendHandshake(): " << address.toString();

    if (address.isNull()) {
        if (m_pAgent == NULL) return;

        foreach (CBluetoothSocket* pSocket, m_Neighbours) {
            send(name().toUtf8(), pSocket, IcpConnectionServer::MSG_HANDSHAKE);
        }

        m_pAgent->setUuidFilter(m_ServiceUuid);
        m_pAgent->start(CBluetoothServiceDiscoveryAgent::FullDiscovery);

    } else {
        send(name().toUtf8(), (CBluetoothAddress) address, IcpConnectionServer::MSG_ONLINE);
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::sendOnlineNotification(const IcpAddress& address)
{
    send(name().toUtf8(), (CBluetoothAddress) address, IcpConnectionServer::MSG_ONLINE);
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::sendGoodbye(const IcpAddress& address)
{
    if (address.isNull()) {
        foreach (CBluetoothSocket* pSocket, m_Neighbours) {
            send(name().toUtf8(), pSocket, IcpConnectionServer::MSG_GOODBYE);
        }

    } else {
        send(name().toUtf8(), (CBluetoothAddress) address, IcpConnectionServer::MSG_GOODBYE);
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::sendData(IcpSocket* pSocket, const QByteArray& baData)
{
    send(baData, (CBluetoothSocket*) *pSocket, IcpConnectionServer::MSG_DATA);
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotHostModeChanged(CBluetoothLocalDevice::HostMode mode)
{
    qDebug() << "IcpServerBluetooth::slotHostModeChanged(): " << mode;
    if (isStarted()) {
        if (mode == CBluetoothLocalDevice::HostPoweredOff) {
            innerStop();
        } else {
            innerStart();
            sendHandshake(IcpAddress::Null());
        }
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotDeviceConnected(const CBluetoothAddress& address)
{
    qDebug() << "IcpServerBluetooth::slotDeviceConnected(): " << address.toString();
    if (m_Neighbours.contains(address)) {
        return;
    }

    if (m_pAgent->isActive()) {
        m_PendingDevices.append(address);

    } else {
        m_pAgent->setRemoteAddress(address);
        m_pAgent->setUuidFilter(m_ServiceUuid);
        m_pAgent->start(CBluetoothServiceDiscoveryAgent::FullDiscovery);
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotDeviceDisconnected(const CBluetoothAddress& address)
{
    qDebug() << "IcpServerBluetooth::slotDeviceDisconnected(): " << address.toString();
    CBluetoothSocket* pSocket = m_Neighbours.value(address, NULL);
    if (pSocket != NULL) {
        pSocket->close();
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotAdapterError(CBluetoothLocalDevice::Error error)
{
    qWarning() << "IcpServerBluetooth::slotAdapterError(): " << error;
    QString qsErrorString("");
    switch (error) {
    case CBluetoothLocalDevice::PairingError:
        qsErrorString = IcpServerBluetooth::tr("Pairing error");
        break;
    default:
        qsErrorString = IcpServerBluetooth::tr("Unknown adapter error");
        break;
    }

    IcpException e(qsErrorString);
    e.raise();
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotServerError(CBluetoothServer::Error error)
{
    qWarning() << "IcpServerBluetooth::slotServerError(): " << error;
    CBluetoothServer* pServer = (CBluetoothServer*) sender();
    IcpException e(pServer->errorString());
    e.raise();
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotNeighbourConnected()
{
    CBluetoothSocket* pSocket = qobject_cast<CBluetoothSocket*>(sender());
    if (pSocket == NULL) {
        pSocket = m_pServer->nextPendingConnection();
    }

    connect(pSocket, SIGNAL(readyRead())            , SLOT(slotNeighbourRead()));
    connect(pSocket, SIGNAL(disconnected())         , SLOT(slotNeighbourDisconnected()));
    connect(pSocket, SIGNAL(disconnected()), pSocket, SLOT(deleteLater()));
    connect(pSocket, SIGNAL(error(CBluetoothSocket::SocketError))       , SLOT(slotNeighbourError(CBluetoothSocket::SocketError)));
    connect(pSocket, SIGNAL(stateChanged(CBluetoothSocket::SocketState)), SLOT(slotTest(CBluetoothSocket::SocketState)));

    CBluetoothAddress address = pSocket->peerAddress();
    qDebug() << "IcpServerBluetooth::slotNeighbourConnected():" << address.toString();
    QMutexLocker locker(&m_Mutex);
    m_Neighbours.insert(address, pSocket);
    locker.unlock();

    send(name().toUtf8(), pSocket, IcpConnectionServer::MSG_HANDSHAKE);
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotNeighbourRead()
{
    CBluetoothSocket* pSocket = (CBluetoothSocket*) sender();
    CBluetoothAddress address = pSocket->peerAddress();
    qDebug() << "IcpServerBluetooth::slotNeighbourRead():";

    QDataStream in(pSocket);
    in.setVersion(QDataStream::Qt_5_1);

    if (m_uiNextBlockSize == 0 && pSocket->bytesAvailable() >= (qint64)sizeof(quint32)) {
        in >> m_uiNextBlockSize;
    }

    if (m_uiNextBlockSize > IcpConnectionServer::MSG_MAX_SIZE) {
        pSocket->abort();
        sendHandshake(IcpAddress(address));
        m_uiNextBlockSize = 0;

    } else if (pSocket->bytesAvailable() >= m_uiNextBlockSize) {
        // All data is available
        quint32 uiPacketSize = m_uiNextBlockSize;
        m_uiNextBlockSize = 0;

        QByteArray baDatagram(uiPacketSize, '\0');
        in.readRawData(baDatagram.data(), uiPacketSize);

        if (baDatagram.startsWith(MAGIC_BLUETOOTH)) {
            qint32     iCode = unwrapServiceMessage(baDatagram);
            IcpAddress clientAddress(address);
            switch (iCode) {
            case MSG_BTH_CONNECT:
                m_ActiveClients.append(address);
                qDebug() << "IcpServerBluetooth::slotNeighbourRead(): MSG_BTH_CONNECT;" << address.toString();
                addPendingSocket(clientAddress, new IcpSocket(pSocket));
                send(name().toUtf8(), pSocket, IcpConnectionServer::MSG_CONNECT);
                break;

            case MSG_BTH_DISCONNECT:
                if (m_ActiveClients.contains(address)) {
                    m_ActiveClients.removeAll(address);
                }
                qDebug() << "IcpServerBluetooth::slotNeighbourRead(): MSG_BTH_DISCONNECT;" << address.toString();
                emit clientDisconnected(clientAddress);
                break;

            default:
                break;
            }

        } else {
            emit dataReceived(IcpAddress(address), baDatagram);
        }
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotNeighbourDisconnected()
{
    CBluetoothSocket* pSocket = (CBluetoothSocket*) sender();
    CBluetoothAddress address = pSocket->peerAddress();
    qDebug() << "IcpServerBluetooth::slotNeighbourDisconnected(): address =" << address.toString();
    qDebug() << "IcpServerBluetooth::slotNeighbourDisconnected(): Is active client =" << m_ActiveClients.contains(address);

    if (m_ActiveClients.contains(address)) {
        m_ActiveClients.removeAll(address);
        emit clientDisconnected(IcpAddress(address));
    }

    QMutexLocker locker(&m_Mutex);
    m_Neighbours.remove(address);
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotNeighbourError(CBluetoothSocket::SocketError /*error*/)
{
    CBluetoothSocket* pSocket = (CBluetoothSocket*) sender();
    CBluetoothAddress address = pSocket->peerAddress();
    qDebug() << "IcpServerBluetooth::slotNeighbourError():";

    if (pSocket->state() == CBluetoothSocket::UnconnectedState) {
        pSocket->deleteLater();

        QMutexLocker locker(&m_Mutex);
        m_Neighbours.remove(address);
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotTest(CBluetoothSocket::SocketState state)
{
    CBluetoothSocket* pSocket = (CBluetoothSocket*) sender();
    QString qsState;
    switch (state) {
    case CBluetoothSocket::UnconnectedState:
        qsState = "UnconnectedState";
        break;
    case CBluetoothSocket::ServiceLookupState:
        qsState = "ServiceLookupState";
        break;
    case CBluetoothSocket::ConnectingState:
        qsState = "ConnectingState";
        break;
    case CBluetoothSocket::ConnectedState:
        qsState = "ConnectedState";
        break;
    case CBluetoothSocket::BoundState:
        qsState = "BoundState";
        break;
    case CBluetoothSocket::ClosingState:
        qsState = "ClosingState";
        break;
    case CBluetoothSocket::ListeningState:
        qsState = "ListeningState";
        break;
    default:
        break;
    }

    qDebug() << "IcpServerBluetooth::slotTest(): " << qsState << pSocket->peerAddress().toString();
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotServiceDiscovered(const CBluetoothServiceInfo& info)
{
    qDebug() << "IcpServerBluetooth::serviceDiscovered():" << info.device().name() << info.serviceName() << info.serviceUuid().toString();
    if (m_Neighbours.contains(info.device().address())) {
        qDebug() << "IcpServerBluetooth::serviceDiscovered(): m_Neighbours.contains(" << info.device().address().toString() << ")";
        return;
    }

    CBluetoothSocket* pSocket = new CBluetoothSocket(this);
    connect(pSocket, SIGNAL(connected())                                  , SLOT(slotNeighbourConnected()));
    connect(pSocket, SIGNAL(error(CBluetoothSocket::SocketError)), pSocket, SLOT(deleteLater()));
    pSocket->connectToService(info);
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotServiceDiscoveryFinished()
{
    qDebug() << "IcpServerBluetooth::slotServiceDiscoveryFinished():";
//    if (!m_PendingDevices.isEmpty()) {
//        m_pAgent->setRemoteAddress(m_PendingDevices.takeFirst());
//        m_pAgent->setUuidFilter(m_ServiceUuid);
//        m_pAgent->start(CBluetoothServiceDiscoveryAgent::FullDiscovery);
    //    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::slotServiceDiscoveryCancelled()
{
    qDebug() << "IcpServerBluetooth::slotServiceDiscoveryCancelled():";
    if (!isStarted()) {
        m_pAgent->deleteLater();
        m_pAgent = NULL;
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::innerStart(bool mute)
{
    qDebug() << "IcpServerBluetooth::innerStart():";
    if (!isStarted() || isActive() || m_pLocalDevice->hostMode() == CBluetoothLocalDevice::HostPoweredOff) return;

    m_pAgent = new CBluetoothServiceDiscoveryAgent(this);
    connect(m_pAgent, SIGNAL(serviceDiscovered(CBluetoothServiceInfo)), this, SLOT(slotServiceDiscovered(CBluetoothServiceInfo)));
    connect(m_pAgent, SIGNAL(finished())                              , this, SLOT(slotServiceDiscoveryFinished()));
    connect(m_pAgent, SIGNAL(canceled())                              , this, SLOT(slotServiceDiscoveryCancelled()));

    m_pServer = new CBluetoothServer(CBluetoothServiceInfo::RfcommProtocol, this);
    connect(m_pServer, SIGNAL(newConnection()), SLOT(slotNeighbourConnected()));

    if (!m_pServer->listen(m_ServiceUuid, m_ServiceName)) {
        qWarning() << "Bluetooth. Unable to start server:" << m_pServer->errorString();
        m_pServer->close();
        m_pServer->deleteLater();
        m_pServer = NULL;

        if (!mute) {
            qDebug() << "IcpServerBluetooth::innerStart(): Error";
            IcpException error(m_pServer->errorString());
            error.raise();
        }
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::innerStop()
{
    qDebug() << "IcpServerBluetooth::innerStop():";

    if (m_pAgent != NULL) {
        if (m_pAgent->isActive()) {
            m_pAgent->stop();
        } else {
            m_pAgent->deleteLater();
            m_pAgent = NULL;
        }
    }

    foreach (CBluetoothSocket* pSocket, m_Neighbours) {
        pSocket->close();
    }
    m_Neighbours.clear();

    if (m_pServer != NULL) {
        m_pServer->close();
        m_pServer->deleteLater();
        m_pServer = NULL;
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::send(const QByteArray& baMessage, const CBluetoothAddress& address, int iMessageCode)
{
    CBluetoothSocket* pSocket = m_Neighbours.value((CBluetoothAddress) address, NULL);
    if (pSocket != NULL) {
        send(baMessage, pSocket, iMessageCode);
    }
}

///----------------------------------------------------------------------------
void IcpServerBluetooth::send(const QByteArray& baMessage, CBluetoothSocket* pSocket, int iMessageCode)
{
    if (pSocket->state() != CBluetoothSocket::ConnectedState) return;

    QByteArray baDatagram = wrapMessage(baMessage, iMessageCode, true);

    pSocket->write(baDatagram);
    qDebug() << "Bluetooth sent:" << baMessage << "peerAddress =" << pSocket->peerAddress().toString() << "localAddress =" << pSocket->localAddress().toString();
}
