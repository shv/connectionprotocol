#include "IcpSocket.h"

using namespace icp;

///----------------------------------------------------------------------------
void IcpSocket::abort()
{
    switch (m_Protocol) {
    case IcpAddress::IpV4:
        ((QTcpSocket*) m_pSocket)->abort();
        break;
#ifdef ENABLE_BLUETOOTH
    case IcpAddress::Bluetooth:
        ((CBluetoothSocket*) m_pSocket)->abort();
        break;
#endif
    }
}

