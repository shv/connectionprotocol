#include "CBluetoothServiceInfo.h"

///----------------------------------------------------------------------------
CBluetoothServiceInfo::CBluetoothServiceInfo()
{
}

///----------------------------------------------------------------------------
CBluetoothServiceInfo::CBluetoothServiceInfo(const CBluetoothServiceInfo& other) :
    m_Device(other.device()),
    m_Uuid(other.serviceUuid()),
    m_Name(other.serviceName())
{
}

///----------------------------------------------------------------------------
CBluetoothServiceInfo::~CBluetoothServiceInfo() {
}

///----------------------------------------------------------------------------
bool CBluetoothServiceInfo::isComplete() const
{
    return false;
}

///----------------------------------------------------------------------------
CBluetoothDeviceInfo CBluetoothServiceInfo::device() const
{
    return m_Device;
}

///----------------------------------------------------------------------------
QUuid CBluetoothServiceInfo::serviceUuid() const
{
    return m_Uuid;
}

///----------------------------------------------------------------------------
QString CBluetoothServiceInfo::serviceName() const
{
    return m_Name;
}

///----------------------------------------------------------------------------
CBluetoothServiceInfo::Protocol CBluetoothServiceInfo::socketProtocol() const
{
    return RfcommProtocol;
}

///----------------------------------------------------------------------------
void CBluetoothServiceInfo::setDevice(const CBluetoothDeviceInfo& device)
{
    m_Device = device;
}

///----------------------------------------------------------------------------
void CBluetoothServiceInfo::setServiceUuid(const QUuid& uuid)
{
    m_Uuid = uuid;
}

///----------------------------------------------------------------------------
void CBluetoothServiceInfo::setServiceName(const QString& name)
{
    m_Name = name;
}
