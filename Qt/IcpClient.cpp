#include "IcpClient.h"

using namespace icp;

///----------------------------------------------------------------------------
IcpClient::IcpClient(const QString& qsName, IcpSocket* pSocket, IcpServer* pServer, QObject* parent) :
    QObject(parent),
    m_qsName(qsName),
    m_pSocket(pSocket),
    m_pServer(pServer),
    m_iTimerId(-1)
{
}

///----------------------------------------------------------------------------
void IcpClient::startTimeoutTimer(int iTimeOut)
{
    if (m_iTimerId != -1) killTimer(m_iTimerId);
    m_iTimerId = startTimer(iTimeOut);
}
