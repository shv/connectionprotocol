#ifndef CBLUETOOTHSOCKET_P_H
#define CBLUETOOTHSOCKET_P_H

#include <QList>
#include <QTimer>
#include "private/qringbuffer_p.h"
#include "private/qiodevice_p.h"

#include "CBluetoothSocketEngine_p.h"

class CBluetoothSocketPrivate : public QIODevicePrivate, public CAbstractSocketEngineReceiver
{
    Q_DECLARE_PUBLIC(CBluetoothSocket)
public:
///    static const int READ_BUFFER_LENGTH = 8192;

    CBluetoothSocketPrivate();
    virtual ~CBluetoothSocketPrivate();

    // from CAbstractSocketEngineReceiver
    inline void readNotification()      {canReadNotification();}
    inline void writeNotification()     {canWriteNotification();}
    inline void exceptionNotification() {}
    inline void closeNotification()     {canCloseNotification();}
    void connectionNotification();

    bool canReadNotification();
    bool canWriteNotification();
    void canCloseNotification();

    // slots
    void _q_startConnecting(const CBluetoothServiceInfo& serviceInfo);
    void _q_testConnection();
    void _q_abortConnectionAttempt();
//    void _q_forceDisconnect();

    bool readSocketNotifierCalled;
    bool readSocketNotifierState;
    bool readSocketNotifierStateSet;

    bool emittedReadyRead;
    bool emittedBytesWritten;

    bool abortCalled;
    bool closeCalled;
    bool pendingClose;

//    QAbstractSocket::PauseModes pauseMode;

//    QString hostName;
//    quint16 port;
//    QHostAddress host;
//    QList<QHostAddress> addresses;

    CBluetoothAddress localAddress;
    quint16           localPort;
    CBluetoothAddress peerAddress;
    quint16           peerPort;
    QString           peerName;

    CBluetoothSocketEngine *socketEngine;
    qintptr cachedSocketDescriptor;

    void resetSocketLayer();
    bool flush();

    bool initSocketLayer(CBluetoothServiceInfo::Protocol protocol);
//    void startConnectingByName(const QString &host);
    void fetchConnectionParameters();
//    void setupSocketNotifiers();
    bool readFromSocket();

///    qint64 readBufferMaxSize;
    QRingBuffer writeBuffer;

    bool isBuffered;
//    int blockingTimeout;

    QTimer *connectTimer;
    QTimer *disconnectTimer;
//    int connectTimeElapsed;

//    int hostLookupId;

//    QAbstractSocket::SocketType socketType;
    CBluetoothSocket::SocketState state;

    CBluetoothSocket::SocketError socketError;
    void setError();
    void setError(const QString& errorString);

//    QAbstractSocket::NetworkLayerProtocol preferredNetworkLayerProtocol;

//    bool prePauseReadSocketNotifierState;
//    bool prePauseWriteSocketNotifierState;
//    bool prePauseExceptionSocketNotifierState;
//    static void pauseSocketNotifiers(QAbstractSocket*);
//    static void resumeSocketNotifiers(QAbstractSocket*);
//    static QAbstractSocketEngine* getSocketEngine(QAbstractSocket*);
};


#endif // CBLUETOOTHSOCKET_P_H
