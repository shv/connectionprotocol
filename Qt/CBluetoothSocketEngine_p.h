#ifndef CBLUETOOTHSOCKETENGINE_P_H
#define CBLUETOOTHSOCKETENGINE_P_H

#include <QObject>
#include "private/qobject_p.h"

#include "CBluetoothSocket.h"
#include "CBluetoothServer.h"
#include "CBluetoothServiceInfo.h"

class CBluetoothSocketEnginePrivate;

class CAbstractSocketEngineReceiver {
public:
    virtual ~CAbstractSocketEngineReceiver(){}
    virtual void readNotification() = 0;
    virtual void writeNotification() = 0;
    virtual void closeNotification() = 0;
    virtual void exceptionNotification() = 0;
    virtual void connectionNotification() = 0;
};

class CBluetoothSocketEngine : public QObject
{
    Q_OBJECT
public:
    enum SocketOption {
        NonBlockingSocketOption,
        ReceiveBufferSocketOption,
        SendBufferSocketOption,
        AddressReusable,
        BindExclusively,
        ReceiveOutOfBandData,
        KeepAliveOption
    };

    enum Error {
        UnknownError,
        NoSocketError,
        HostNotFoundError,
        ServiceNotFoundError,
        NetworkError,
        UnsupportedProtocolError,
        OperationError,
        PoweredOffError,
        InputOutputError,
        ServiceAlreadyRegisteredError,
        TemporaryError
    };

    CBluetoothSocketEngine(QObject *parent = 0);
    ~CBluetoothSocketEngine();

    bool initialize(CBluetoothServiceInfo::Protocol protocol = CBluetoothServiceInfo::RfcommProtocol);
    bool initialize(qintptr socketDescriptor);

    qintptr socketDescriptor() const;

    bool isValid() const;

    bool connectToHost(const CBluetoothAddress &address, const QUuid& serviceId);
//    bool connectToHostByName(const QString &name, quint16 port);
    bool bind();
    bool registerService(const QUuid& serviceId, const QString& serviceName);
    bool listen();
    qintptr accept();
    void close();

    qint64 bytesAvailable() const;

    qint64 read(char *data, qint64 maxlen);
    qint64 write(const char *data, qint64 len);

//    qint64 readDatagram(char *data, qint64 maxlen, CBluetoothAddress *addr = 0,
//                            quint16 *port = 0);
//    qint64 writeDatagram(const char *data, qint64 len, const CBluetoothAddress &addr,
//                             quint16 port);
//    bool hasPendingDatagrams() const;
//    qint64 pendingDatagramSize() const;

    qint64 bytesToWrite() const;

//    qint64 receiveBufferSize() const;
//    void setReceiveBufferSize(qint64 bufferSize);

//    qint64 sendBufferSize() const;
//    void setSendBufferSize(qint64 bufferSize);

//    int option(SocketOption option) const;
    bool setOption(SocketOption option, int value);

//    bool waitForRead(int msecs = 30000, bool *timedOut = 0);
//    bool waitForWrite(int msecs = 30000, bool *timedOut = 0);
//    bool waitForReadOrWrite(bool *readyToRead, bool *readyToWrite,
//                bool checkRead, bool checkWrite,
//                int msecs = 30000, bool *timedOut = 0);

    Error error() const;
    QString errorString() const;
    CBluetoothSocket::SocketState state() const;
//    CBluetoothSocket::Protocol protocol() const;

    CBluetoothAddress localAddress() const;
    quint32 localPort() const;
    CBluetoothAddress peerAddress() const;
    quint16 peerPort() const;
//    QString peerName();
    QUuid serviceId() const;

    bool isReadNotificationEnabled() const;
    void setReadNotificationEnabled(bool enable);
    bool isWriteNotificationEnabled() const;
    void setWriteNotificationEnabled(bool enable);
    bool isExceptionNotificationEnabled() const;
    void setExceptionNotificationEnabled(bool enable);

public Q_SLOTS:
    void readNotification();
    void writeNotification();
    void closeNotification();
    void exceptionNotification();
    void connectionNotification();

public:
    void setReceiver(CAbstractSocketEngineReceiver* receiver);

private:
    Q_DECLARE_PRIVATE(CBluetoothSocketEngine)
    Q_DISABLE_COPY(CBluetoothSocketEngine)
};

#endif // CBLUETOOTHSOCKETENGINE_P_H
