#include "CBluetoothHostInfo.h"

///----------------------------------------------------------------------------
CBluetoothHostInfo::CBluetoothHostInfo() :
    m_qsName(""),
    m_Address()
{
}

///----------------------------------------------------------------------------
CBluetoothHostInfo::CBluetoothHostInfo(const CBluetoothHostInfo& other) :
    m_qsName(other.m_qsName),
    m_Address(other.m_Address)
{
}

///----------------------------------------------------------------------------
void CBluetoothHostInfo::setAddress(const CBluetoothAddress& address)
{
    m_Address = address;
}

///----------------------------------------------------------------------------
void CBluetoothHostInfo::setName(const QString& name)
{
    m_qsName = name;
}

///----------------------------------------------------------------------------
CBluetoothHostInfo& CBluetoothHostInfo::operator=(const CBluetoothHostInfo& other)
{
    m_qsName  = other.m_qsName;
    m_Address = other.m_Address;

    return *this;
}
