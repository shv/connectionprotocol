#ifndef CBLUETOOTHSERVICEDISCOVERYAGENT_P_H
#define CBLUETOOTHSERVICEDISCOVERYAGENT_P_H

#include "CBluetoothServiceDiscoveryAgent.h"

#include <QThread>

class CServiceDiscoveryThread : public QThread
{
    Q_OBJECT
public:
    explicit CServiceDiscoveryThread(CBluetoothServiceDiscoveryAgent::DiscoveryMode mode, CBluetoothServiceDiscoveryAgentPrivate* d, QObject* parent = 0);
    ~CServiceDiscoveryThread();

    void run();

signals:
    void canceled();
    void serviceDiscovered(const CBluetoothServiceInfo& info);

public slots:
    void slotCancel();

private:
    void loopThroughEnumeratedServices(const QList<QUuid>& filter, const CBluetoothDeviceInfo& device, GUID guids[], int guidsSize);

    CBluetoothServiceDiscoveryAgentPrivate*        d;
    CBluetoothServiceDiscoveryAgent::DiscoveryMode mode;

    bool isCancelled;
};

#endif // CBLUETOOTHSERVICEDISCOVERYAGENT_P_H
