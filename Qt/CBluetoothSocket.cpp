#include "CBluetoothSocket.h"
#include "CBluetoothSocket_p.h"

#include "private/qthread_p.h"
#include "private/qiodevice_p.h"
#include <QHostInfo>
#include <QTimer>
#include <QScopedValueRollback>

#include <winsock2.h>
#include <ws2bth.h>

#include "CBluetoothSocketEngine_p.h"

#define Q_CHECK_SOCKETENGINE(returnValue) do { \
    if (!d->socketEngine) { \
        return returnValue; \
    } } while (0)

#define BLUETOOTHSOCKET_BUFFERSIZE 32768
#define BLUETOOTHSOCKET_CONNECT_TIMEOUT 30000

///----------------------------------------------------------------------------
/// CBluetoothSocketPrivate
///----------------------------------------------------------------------------
CBluetoothSocketPrivate::CBluetoothSocketPrivate() :
    readSocketNotifierCalled(false),
    readSocketNotifierState(false),
    readSocketNotifierStateSet(false),
    emittedReadyRead(false),
    emittedBytesWritten(false),
    abortCalled(false),
    closeCalled(false),
    pendingClose(false),
//    pauseMode(QAbstractSocket::PauseNever),
//    port(0),
    localPort(0),
    peerPort(0),
    socketEngine(NULL),
    cachedSocketDescriptor(INVALID_SOCKET),
///    readBufferMaxSize(0),
    writeBuffer(BLUETOOTHSOCKET_BUFFERSIZE),
    isBuffered(false),
//    blockingTimeout(30000),
    connectTimer(NULL),
    disconnectTimer(NULL),
//    connectTimeElapsed(0),
//    hostLookupId(-1),
//    socketType(QAbstractSocket::UnknownSocketType),
    state(CBluetoothSocket::UnconnectedState),
    socketError(CBluetoothSocket::UnknownSocketError)//,
//    preferredNetworkLayerProtocol(QAbstractSocket::UnknownNetworkLayerProtocol)
{
}

///----------------------------------------------------------------------------
CBluetoothSocketPrivate::~CBluetoothSocketPrivate()
{
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::connectionNotification()
{
    // If in connecting state, check if the connection has been
    // established, otherwise flush pending data.
    if (state == CBluetoothSocket::ConnectingState) {
        _q_testConnection();
    }
}

///----------------------------------------------------------------------------
/// Slot connected to the read socket notifier. This slot is called
/// when new data is available for reading, or when the socket has
/// been closed. Handles recursive calls.
///
bool CBluetoothSocketPrivate::canReadNotification()
{
    Q_Q(CBluetoothSocket);

    // Prevent recursive calls
    if (readSocketNotifierCalled) {
        if (!readSocketNotifierStateSet) {
            readSocketNotifierStateSet = true;
            readSocketNotifierState = socketEngine->isReadNotificationEnabled();
            socketEngine->setReadNotificationEnabled(false);
        }
    }
    QScopedValueRollback<bool> rsncrollback(readSocketNotifierCalled);
    readSocketNotifierCalled = true;

    if (!isBuffered) {
        socketEngine->setReadNotificationEnabled(false);
    }

    // If buffered, read data from the socket into the read buffer
    qint64 newBytes = 0;
    if (isBuffered) {
///        // Return if there is no space in the buffer
///        if (readBufferMaxSize && buffer.size() >= readBufferMaxSize) {
///            return false;
///        }

        // If reading from the socket fails after getting a read
        // notification, close the socket.
        newBytes = buffer.size();
        if (!readFromSocket()) {
            qDebug() << "CBluetoothSocket::canReadNotification(): 1 peerAddress    =" << peerAddress.toString();
            q->disconnectFromService();
            return false;
        }
        newBytes = buffer.size() - newBytes;

///        // If read buffer is full, disable the read socket notifier.
///        if (readBufferMaxSize && buffer.size() == readBufferMaxSize) {
///            socketEngine->setReadNotificationEnabled(false);
///        }
    }

    // only emit readyRead() when not recursing, and only if there is data available
    bool hasData = newBytes > 0 || (!isBuffered && socketEngine);

    if (!emittedReadyRead && hasData) {
        QScopedValueRollback<bool> r(emittedReadyRead);
        emittedReadyRead = true;
        emit q->readyRead();
    }

    // If we were closed as a result of the readyRead() signal, return.
    if (state == CBluetoothSocket::UnconnectedState || state == CBluetoothSocket::ClosingState) {
        return true;
    }

    if (!hasData && socketEngine) {
        socketEngine->setReadNotificationEnabled(true);
    }

    // reset the read socket notifier state if we reentered inside the readyRead() connected slot.
    if (readSocketNotifierStateSet && socketEngine && readSocketNotifierState != socketEngine->isReadNotificationEnabled()) {
        socketEngine->setReadNotificationEnabled(readSocketNotifierState);
        readSocketNotifierStateSet = false;
    }
    return true;
}

///----------------------------------------------------------------------------
/// Slot connected to the write socket notifier. It's called during a
/// delayed connect or when the socket is ready for writing.
///
bool CBluetoothSocketPrivate::canWriteNotification()
{
    qDebug() << "CBluetoothSocketPrivate::canWriteNotification(): 1 peerAddress =" << peerAddress.toString() << "localAddress =" << localAddress.toString() << q_func() << socketEngine << this;
    if (socketEngine && socketEngine->isWriteNotificationEnabled()) {
        socketEngine->setWriteNotificationEnabled(false);
    }

    int tmp = writeBuffer.size();
    flush();
    if (socketEngine && !writeBuffer.isEmpty()) {
        socketEngine->setWriteNotificationEnabled(true);
    }
    return (writeBuffer.size() < tmp);
}

///----------------------------------------------------------------------------
/// Slot connected to the close socket notifier. It's called when the
/// socket is closed.
///
void CBluetoothSocketPrivate::canCloseNotification()
{
    Q_Q(CBluetoothSocket);

    qint64 newBytes = 0;
    if (isBuffered) {
        // Try to read to the buffer, if the read fail we can close the socket.
        newBytes = buffer.size();
        if (!readFromSocket()) {
            qDebug() << "CBluetoothSocketPrivate::canCloseNotification(): 1 peerAddress =" << peerAddress.toString();
            q->disconnectFromService();
            return;
        }
        newBytes = buffer.size() - newBytes;
        if (newBytes) {
            // If there was still some data to be read from the socket
            // then we could get another FD_READ. The disconnect will
            // then occur when we read from the socket again and fail
            // in canReadNotification or by the manually created
            // closeNotification below.
            emit q->readyRead();

            QMetaObject::invokeMethod(socketEngine, "closeNotification", Qt::QueuedConnection);
        }

    } else if (socketEngine) {
        emit q->readyRead();
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::_q_startConnecting(const CBluetoothServiceInfo& serviceInfo)
{
    Q_Q(CBluetoothSocket);

    qDebug() << "CBluetoothSocket::_q_startConnecting():";
    if (state != CBluetoothSocket::UnconnectedState) {
        return;
    }

    // Enter Connecting state (see also sn_write, which is called by
    // the write socket notifier after connect())
    state = CBluetoothSocket::ConnectingState;
    emit q->stateChanged(state);

    if (!initSocketLayer(serviceInfo.socketProtocol())) {
        emit q_func()->error(socketError);
        return;
    }

    if (socketEngine->connectToHost(serviceInfo.device().address(), serviceInfo.serviceUuid())) {
        fetchConnectionParameters();
        return;
    }

    // cache the socket descriptor even if we're not fully connected yet
    cachedSocketDescriptor = socketEngine->socketDescriptor();

    // Check that we're in delayed connection state.
    if (socketEngine->state() != CBluetoothSocket::ConnectingState) {
        qWarning() << "CBluetoothSocket::_q_startConnecting(): failed";
        return;
    }
    // Start the connect timer.
    if (threadData->hasEventDispatcher()) {
        if (!connectTimer) {
            connectTimer = new QTimer(q);
            QObject::connect(connectTimer, SIGNAL(timeout()),
                             q, SLOT(_q_abortConnectionAttempt()),
                             Qt::DirectConnection);
        }
        connectTimer->start(BLUETOOTHSOCKET_CONNECT_TIMEOUT);
    }

    // Wait for a write notification that will eventually call
    // _q_testConnection().
    socketEngine->setWriteNotificationEnabled(true);
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::_q_testConnection()
{
    Q_Q(CBluetoothSocket);

    if (socketEngine) {
        if (threadData->hasEventDispatcher()) {
            if (connectTimer) {
                connectTimer->stop();
            }
        }

        if (socketEngine->state() == CBluetoothSocket::ConnectedState) {
            // Fetch the parameters if our connection is completed;
            // otherwise, fall out and try the next address.
            fetchConnectionParameters();
            if (pendingClose) {
                qDebug() << "CBluetoothSocket::_q_testConnection(): 1 peerAddress    =" << peerAddress.toString();
                q_func()->disconnectFromService();
                pendingClose = false;
            }
            return;
        }
    }

    if (threadData->hasEventDispatcher()) {
        if (connectTimer) {
            connectTimer->stop();
        }
    }

    qWarning() << "CBluetoothSocketPrivate::testConnection(): failed";

    state = CBluetoothSocket::UnconnectedState;
    if (socketEngine) {
        if (socketEngine->error() == CBluetoothSocketEngine::UnknownError && socketEngine->state() == CBluetoothSocket::ConnectingState) {
            setError(CBluetoothSocket::tr("Connection refused"));

        } else {
            setError();
        }
    }
    emit q->stateChanged(state);
    emit q->error(socketError);
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::_q_abortConnectionAttempt()
{
    Q_Q(CBluetoothSocket);

    if (socketEngine) {
        socketEngine->setWriteNotificationEnabled(false);
    }

    connectTimer->stop();

    state = CBluetoothSocket::UnconnectedState;
    setError(CBluetoothSocket::tr("Connection timed out"));

    emit q->stateChanged(state);
    emit q->error(socketError);
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::resetSocketLayer()
{
    if (socketEngine) {
        socketEngine->close();
        socketEngine->disconnect();
        delete socketEngine;
        socketEngine = NULL;
        cachedSocketDescriptor = INVALID_SOCKET;
    }
    if (connectTimer) {
        connectTimer->stop();
    }
    if (disconnectTimer) {
        disconnectTimer->stop();
    }
}

///----------------------------------------------------------------------------
/// Writes pending data in the write buffers to the socket. The
/// function writes as much as it can without blocking.
///
/// It is usually invoked by canWriteNotification after one or more
/// calls to write().
///
/// Emits bytesWritten().
///
bool CBluetoothSocketPrivate::flush()
{
    Q_Q(CBluetoothSocket);
    qDebug() << "CBluetoothSocketPrivate::flush(): 1 peerAddress =" << peerAddress.toString() << socketEngine;
    if (!socketEngine || !socketEngine->isValid() || (writeBuffer.isEmpty()
        && socketEngine->bytesToWrite() == 0)) {
        // this covers the case when the buffer was empty, but we had to wait for the socket engine to finish
        if (state == CBluetoothSocket::ClosingState) {
            q->disconnectFromService();
        }
        return false;
    }

    int nextSize = writeBuffer.nextDataBlockSize();
    const char *ptr = writeBuffer.readPointer();

    // Attempt to write it all in one chunk.
    qint64 written = socketEngine->write(ptr, nextSize);
    if (written < 0) {
        setError();
        emit q->error(socketError);
        // an unexpected error so close the socket.
        q->abort();
        return false;
    }

    // Remove what we wrote so far.
    writeBuffer.free(written);
    if (written > 0) {
        // Don't emit bytesWritten() recursively.
        if (!emittedBytesWritten) {
            QScopedValueRollback<bool> r(emittedBytesWritten);
            emittedBytesWritten = true;
            emit q->bytesWritten(written);
        }
    }

    if (writeBuffer.isEmpty() && socketEngine && socketEngine->isWriteNotificationEnabled()
        && !socketEngine->bytesToWrite()) {
        socketEngine->setWriteNotificationEnabled(false);
    }

    if (state == CBluetoothSocket::ClosingState) {
        qDebug() << "CBluetoothSocketPrivate::flush(): 2 peerAddress =" << peerAddress.toString() << socketEngine;
        q->disconnectFromService();
    }

    return true;
}

///----------------------------------------------------------------------------
bool CBluetoothSocketPrivate::initSocketLayer(CBluetoothServiceInfo::Protocol protocol)
{
    Q_Q(CBluetoothSocket);

    resetSocketLayer();
    socketEngine = new CBluetoothSocketEngine(q);
    if (!socketEngine) {
        setError(CBluetoothSocket::tr("Operation on socket is not supported"));
        return false;
    }

    if (!socketEngine->initialize(protocol)) {
        setError();
        return false;
    }

    if (threadData->hasEventDispatcher()) {
        socketEngine->setReceiver(this);
    }

    return true;
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::fetchConnectionParameters()
{
    Q_Q(CBluetoothSocket);

//    peerName = hostName;
    if (socketEngine) {
        socketEngine->setReadNotificationEnabled(true);
        socketEngine->setWriteNotificationEnabled(true);
        localPort = socketEngine->localPort();
        peerPort = socketEngine->peerPort();
        localAddress = socketEngine->localAddress();
        qDebug() << "CBluetoothSocketPrivate::fetchConnectionParameters(): peerAddress = socketEngine->peerAddress()";
        peerAddress = socketEngine->peerAddress();
        cachedSocketDescriptor = socketEngine->socketDescriptor();
    }

    state = CBluetoothSocket::ConnectedState;
    emit q->stateChanged(state);
    emit q->connected();
}

///----------------------------------------------------------------------------
bool CBluetoothSocketPrivate::readFromSocket()
{
    // Find how many bytes we can read from the socket layer.
    qint64 bytesToRead = socketEngine->bytesAvailable();
    if (bytesToRead == 0) {
        // Under heavy load, certain conditions can trigger read notifications
        // for socket notifiers on which there is no activity. If we continue
        // to read 0 bytes from the socket, we will trigger behavior similar
        // to that which signals a remote close. When we hit this condition,
        // we try to read 4k of data from the socket, which will give us either
        // an EAGAIN/EWOULDBLOCK if the connection is alive (i.e., the remote
        // host has _not_ disappeared).
        bytesToRead = 4096;
    }
///    if (readBufferMaxSize && bytesToRead > (readBufferMaxSize - buffer.size())) {
///        bytesToRead = readBufferMaxSize - buffer.size();
///    }

    // Read from the socket, store data in the read buffer.
    char *ptr = buffer.reserve(bytesToRead);
    qint64 readBytes = socketEngine->read(ptr, bytesToRead);
    if (readBytes == -2) {
        // No bytes currently available for reading.
        buffer.chop(bytesToRead);
        return true;
    }
    buffer.chop(int(bytesToRead - (readBytes < 0 ? qint64(0) : readBytes)));

    if (!socketEngine->isValid()) {
        setError();
        emit q_func()->error(socketError);
        resetSocketLayer();
        return false;
    }

    return true;
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::setError()
{
    setError(socketEngine->errorString());
}

///----------------------------------------------------------------------------
void CBluetoothSocketPrivate::setError(const QString& errorString)
{
    switch (socketEngine->error()) {
    case CBluetoothSocketEngine::NoSocketError:
        socketError = CBluetoothSocket::NoSocketError;
        break;
    case CBluetoothSocketEngine::HostNotFoundError:
        socketError = CBluetoothSocket::HostNotFoundError;
        break;
    case CBluetoothSocketEngine::ServiceNotFoundError:
        socketError = CBluetoothSocket::ServiceNotFoundError;
        break;
    case CBluetoothSocketEngine::NetworkError:
        socketError = CBluetoothSocket::NetworkError;
        break;
    case CBluetoothSocketEngine::UnsupportedProtocolError:
        socketError = CBluetoothSocket::UnsupportedProtocolError;
        break;
    case CBluetoothSocketEngine::OperationError:
        socketError = CBluetoothSocket::OperationError;
        break;
    default:
        socketError = CBluetoothSocket::UnknownSocketError;
    }

    q_func()->setErrorString(errorString);
}


///----------------------------------------------------------------------------
/// CBluetoothSocket
///----------------------------------------------------------------------------
CBluetoothSocket::CBluetoothSocket(QObject* parent) :
    QIODevice(*new CBluetoothSocketPrivate, parent)
{
    qDebug() << "CBluetoothSocket::CBluetoothSocket()";
    d_func()->isBuffered = true;
}

///----------------------------------------------------------------------------
CBluetoothSocket::~CBluetoothSocket()
{
    qDebug() << "CBluetoothSocket::~CBluetoothSocket()";
    abort();
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocket::bytesAvailable() const
{
    Q_D(const CBluetoothSocket);
    qint64 available = QIODevice::bytesAvailable();

    if (!d->isBuffered && d->socketEngine && d->socketEngine->isValid()) {
        available += d->socketEngine->bytesAvailable();
    }

    return available;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocket::bytesToWrite() const
{
    return 0;
}

///----------------------------------------------------------------------------
bool CBluetoothSocket::canReadLine() const
{
    bool hasLine = d_func()->buffer.canReadLine();
    return hasLine || QIODevice::canReadLine();
}

///----------------------------------------------------------------------------
void CBluetoothSocket::close()
{
    Q_D(CBluetoothSocket);
    qDebug() << "CBluetoothSocket::close(): 1 peerAddress =" << peerAddress().toString() << this << d->socketEngine;
    QIODevice::close();
    if (d->state != UnconnectedState) {
        d->closeCalled = true;
        disconnectFromService();
    }

    if (d->state != UnconnectedState) {
        return;
    }

    d->localAddress.clear();
    d->localPort = 0;
    d->peerAddress.clear();
    d->peerPort = 0;
    d->peerName.clear();
    d->cachedSocketDescriptor = INVALID_SOCKET;
}

///----------------------------------------------------------------------------
bool CBluetoothSocket::isSequential() const
{
    return true;
}

///----------------------------------------------------------------------------
void CBluetoothSocket::connectToService(const CBluetoothServiceInfo& service, QIODevice::OpenMode openMode)
{
    Q_D(CBluetoothSocket);

    qDebug() << "CBluetoothSocket::connectToService(): device().address() =" << service.device().address();

    if (d->state == ConnectedState || d->state == ConnectingState
        || d->state == ClosingState || d->state == ServiceLookupState) {
        qWarning("CBluetoothSocket::connectToService() called when already looking up or connecting/connected to \"%s\"", qPrintable(service.device().address()));
        d->socketError = CBluetoothSocket::OperationError;
        setErrorString(CBluetoothSocket::tr("Trying to connect while connection is in progress"));
        emit error(d->socketError);
        return;
    }

    d->state = UnconnectedState;
    d->buffer.clear();
    d->writeBuffer.clear();
    d->abortCalled = false;
    d->closeCalled = false;
    d->pendingClose = false;
    d->localPort = 0;
    d->peerPort = 0;
    d->localAddress.clear();
    qDebug() << "CBluetoothSocket::connectToService(): peerAddress.clear()";
    d->peerAddress.clear();
    d->peerName = service.device().name();

    if (openMode & QIODevice::Unbuffered) {
        d->isBuffered = false; // Unbuffered socket
    }

    QIODevice::open(openMode);

    d->_q_startConnecting(service);
}

/////----------------------------------------------------------------------------
//void CBluetoothSocket::connectToService(const CBluetoothAddress& address, const QUuid& uuid, QIODevice::OpenMode openMode)
//{
//    Q_D(CBluetoothSocket);

//    if (d->state == ConnectedState || d->state == ConnectingState
//        || d->state == ClosingState || d->state == ServiceLookupState) {
//        qWarning("CBluetoothSocket::connectToService() called when already looking up or connecting/connected to \"%s\"", qPrintable(address));
//        d->socketError = CBluetoothSocket::OperationError;
//        setErrorString(CBluetoothSocket::tr("Trying to connect while connection is in progress"));
//        emit error(d->socketError);
//        return;
//    }

////    d->preferredNetworkLayerProtocol = protocol;
////    d->hostName = hostName;
////    d->port = port;
//    d->state = UnconnectedState;
//    d->buffer.clear();
//    d->writeBuffer.clear();
//    d->abortCalled = false;
//    d->closeCalled = false;
//    d->pendingClose = false;
//    d->localPort = 0;
//    d->peerPort = 0;
//    d->localAddress.clear();
//    d->peerAddress.clear();
////    d->peerName = hostName;
////    if (d->hostLookupId != -1) {
////        QHostInfo::abortHostLookup(d->hostLookupId);
////        d->hostLookupId = -1;
////    }

//    if (openMode & QIODevice::Unbuffered) {
//        d->isBuffered = false; // Unbuffered socket
//    }

//    QIODevice::open(openMode);
//    d->state = ServiceLookupState;
//    emit stateChanged(d->state);

//    CBluetoothAddress temp;
//    if (d->threadData->hasEventDispatcher()) {
////        // this internal API for QHostInfo either immediately gives us the desired
////        // QHostInfo from cache or later calls the _q_startConnecting slot.
////        bool immediateResultValid = false;
////        QHostInfo hostInfo = qt_qhostinfo_lookup(hostName,
////                                                 this,
////                                                 SLOT(_q_startConnecting(QHostInfo)),
////                                                 &immediateResultValid,
////                                                 &d->hostLookupId);
////        if (immediateResultValid) {
////            d->hostLookupId = -1;
////            d->_q_startConnecting(hostInfo);
////        }
//    }
//}

///----------------------------------------------------------------------------
void CBluetoothSocket::disconnectFromService()
{
    Q_D(CBluetoothSocket);
    qDebug() << "CBluetoothSocket::disconnectFromService(): 1 peerAddress =" << peerAddress().toString() << "localAddress =" << localAddress().toString() << this << d->socketEngine;

    if (d->state == UnconnectedState) {
        return;
    }

    if (!d->abortCalled && (d->state == ConnectingState || d->state == ServiceLookupState)) {
        d->pendingClose = true;
        return;
    }

    // Disable and delete read notification
    if (d->socketEngine) {
        d->socketEngine->setReadNotificationEnabled(false);
    }

    if (d->abortCalled) {
///        if (d->state == ServiceLookupState) {
///            QHostInfo::abortHostLookup(d->hostLookupId);
///            d->hostLookupId = -1;
///        }

    } else {
        // Perhaps emit closing()
        if (d->state != ClosingState) {
            d->state = ClosingState;
            emit stateChanged(d->state);
        }

        // Wait for pending data to be written.
        if (d->socketEngine && d->socketEngine->isValid() && (d->writeBuffer.size() > 0
            || d->socketEngine->bytesToWrite() > 0)) {
            d->socketEngine->setWriteNotificationEnabled(true);
            return;
        }
    }

    SocketState previousState = d->state;
    d->resetSocketLayer();
    d->state = UnconnectedState;
    emit stateChanged(d->state);
    emit readChannelFinished();       // we got an EOF

    // only emit disconnected if we were connected before
    if (previousState == ConnectedState || previousState == ClosingState) {
        emit disconnected();
    }

    d->localAddress.clear();
    d->localPort = 0;
    qDebug() << "CBluetoothSocket::disconnectFromService(): peerAddress.clear()" << d->peerAddress.toString();
    d->peerAddress.clear();
    d->peerPort = 0;

    if (d->closeCalled) {
        d->buffer.clear();
        d->writeBuffer.clear();
        QIODevice::close();
    }
}

///----------------------------------------------------------------------------
void CBluetoothSocket::abort()
{
    Q_D(CBluetoothSocket);
    if (d->state == UnconnectedState) {
        return;
    }

    if (d->connectTimer) {
        d->connectTimer->stop();
        delete d->connectTimer;
        d->connectTimer = NULL;
    }

    d->writeBuffer.clear();
    d->abortCalled = true;
    close();
}

///----------------------------------------------------------------------------
CBluetoothSocket::SocketError CBluetoothSocket::error() const
{
    return d_func()->socketError;
}

///----------------------------------------------------------------------------
QString CBluetoothSocket::errorString() const
{
    return d_func()->errorString;
}

///----------------------------------------------------------------------------
CBluetoothAddress CBluetoothSocket::localAddress() const
{
    return d_func()->localAddress;
}

///----------------------------------------------------------------------------
quint16 CBluetoothSocket::localPort() const
{
    return d_func()->localPort;
}

///----------------------------------------------------------------------------
CBluetoothAddress CBluetoothSocket::peerAddress() const
{
    return d_func()->peerAddress;
}

///----------------------------------------------------------------------------
quint16 CBluetoothSocket::peerPort() const
{
    return d_func()->peerPort;
}

///----------------------------------------------------------------------------
bool CBluetoothSocket::setSocketDescriptor(int socketDescriptor, QIODevice::OpenMode openMode)
{
    Q_D(CBluetoothSocket);

    d->resetSocketLayer();
    d->socketEngine = new CBluetoothSocketEngine(this);
    if (!d->socketEngine->initialize(socketDescriptor)) {
        qDebug() << "CBluetoothSocket::setSocketDescriptor(): Error";
        d->setError();
        return false;
    }

    if (d->threadData->hasEventDispatcher()) {
        d->socketEngine->setReceiver(d);
    }

    QIODevice::open(openMode);

    if (d->state != CBluetoothSocket::ConnectedState) {
        d->state = CBluetoothSocket::ConnectedState;
        emit stateChanged(d->state);
    }

    d->pendingClose = false;
    d->socketEngine->setReadNotificationEnabled(true);
    d->localAddress = d->socketEngine->localAddress();
    d->localPort    = d->socketEngine->localPort();
    d->peerAddress  = d->socketEngine->peerAddress();
    d->peerPort     = d->socketEngine->peerPort();
    d->cachedSocketDescriptor = socketDescriptor;

    return true;
}

///----------------------------------------------------------------------------
int CBluetoothSocket::socketDescriptor() const
{
    return d_func()->cachedSocketDescriptor;
}

/////----------------------------------------------------------------------------
//CBluetoothServiceInfo::Protocol CBluetoothSocket::socketType() const
//{
//    return d_func()->;
//}

///----------------------------------------------------------------------------
CBluetoothSocket::SocketState CBluetoothSocket::state() const
{
    return d_func()->state;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocket::readData(char* data, qint64 maxlen)
{
    Q_D(CBluetoothSocket);

    // Check if the read notifier can be enabled again.
    if (d->socketEngine && !d->socketEngine->isReadNotificationEnabled() && d->socketEngine->isValid()) {
        d->socketEngine->setReadNotificationEnabled(true);
    }

    if (!maxlen) {
        return 0;
    }

    if (d->isBuffered && d->buffer.isEmpty()) {
        // if we're still connected, return 0 indicating there may be more data in the future
        // if we're not connected, return -1 indicating EOF
        return d->state == CBluetoothSocket::ConnectedState ? qint64(0) : qint64(-1);
    }

    if (!d->socketEngine) {
        return -1;          // no socket engine is probably EOF
    }
    if (!d->socketEngine->isValid()) {
        return -1; // This is for unbuffered socket when we already had been disconnected
    }
    if (d->state != CBluetoothSocket::ConnectedState) {
        return -1; // This is for unbuffered socket if we're not connected yet
    }
    qint64 readBytes = d->socketEngine->read(data, maxlen);
    if (readBytes == -2) {
        // -2 from the engine means no bytes available (EAGAIN) so read more later
        return 0;
    } else if (readBytes < 0) {
        d->setError();
        d->resetSocketLayer();
        d->state = CBluetoothSocket::UnconnectedState;
    } else if (!d->socketEngine->isReadNotificationEnabled()) {
        // Only do this when there was no error
        d->socketEngine->setReadNotificationEnabled(true);
    }

    return readBytes;
}

///----------------------------------------------------------------------------
qint64 CBluetoothSocket::writeData(const char* data, qint64 len)
{
    Q_D(CBluetoothSocket);
    if (d->state == CBluetoothSocket::UnconnectedState) {
        d->setError(tr("Socket is not connected"));
        return -1;
    }

    if (!d->isBuffered && d->writeBuffer.isEmpty()) {
        // This code is for the new unbuffered socket use case
        qint64 written = d->socketEngine->write(data, len);
        if (written < 0) {
            d->setError();
            return written;

        } else if (written < len) {
            // Buffer what was not written yet
            char *ptr = d->writeBuffer.reserve(len - written);
            memcpy(ptr, data + written, len - written);
            if (d->socketEngine) {
                d->socketEngine->setWriteNotificationEnabled(true);
            }
        }
        return len; // size=actually written + what has been buffered
    }

    // This is the code path for normal buffered socket or
    // unbuffered socket when there was already something in the
    // write buffer and therefore we could not do a direct engine write.
    // We just write to our write buffer and enable the write notifier
    // The write notifier then flush()es the buffer.

    char *ptr = d->writeBuffer.reserve(len);
    if (len == 1) {
        *ptr = *data;
    } else {
        memcpy(ptr, data, len);
    }

    qint64 written = len;

    if (d->socketEngine && !d->writeBuffer.isEmpty()) {
        d->socketEngine->setWriteNotificationEnabled(true);
    }

    return written;
}

///----------------------------------------------------------------------------
void CBluetoothSocket::setSocketError(CBluetoothSocket::SocketError error)
{
    d_func()->socketError = error;
}

///----------------------------------------------------------------------------
void CBluetoothSocket::setSocketState(CBluetoothSocket::SocketState state)
{
    if (d_func()->state != state) {
        d_func()->state = state;
        emit stateChanged(d_func()->state);
    }
}

#include "moc_CBluetoothSocket.cpp"
