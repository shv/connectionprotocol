#ifndef ICPSERVERBLUETOOTH_H
#define ICPSERVERBLUETOOTH_H

#include "CBluetoothLocalDevice.h"
#include "CBluetoothServiceDiscoveryAgent.h"
#include "CBluetoothServer.h"
#include "CBluetoothSocket.h"

#include "IcpServer.h"

namespace icp {

class IcpServerBluetooth : public IcpServer
{
    Q_OBJECT
private:
    // Bluetooth service "Magic": 8 bytes
    static const QByteArray MAGIC_BLUETOOTH;
    // Bluetooth service codes
    static const qint32 MSG_BTH_CONNECT    = 1;
    static const qint32 MSG_BTH_DISCONNECT = 2;

public:
    explicit IcpServerBluetooth(const QString& qsName, QObject *parent = 0);
    virtual ~IcpServerBluetooth();

    virtual IcpAddress::Protocol protocol() {return IcpAddress::Bluetooth;}

    void start(Parameters params);
    void stop();

    bool isActive() const {return (m_pAgent != NULL && m_pAgent->isActive()) || (m_pServer != NULL && m_pServer->isListening());}

    void sendHandshake(const IcpAddress& address);
    void sendOnlineNotification(const IcpAddress& address);
    void sendGoodbye(const IcpAddress& address);
    void sendData(IcpSocket* pSocket, const QByteArray& baData);

protected:
    QByteArray serviceMagic() const {return MAGIC_BLUETOOTH;}

private slots:
    void slotHostModeChanged(CBluetoothLocalDevice::HostMode mode);
    void slotDeviceConnected(   const CBluetoothAddress& address);
    void slotDeviceDisconnected(const CBluetoothAddress& address);
    void slotAdapterError(CBluetoothLocalDevice::Error error);

    void slotServiceDiscovered(const CBluetoothServiceInfo& info);
    void slotServiceDiscoveryFinished();
    void slotServiceDiscoveryCancelled();

    void slotServerError(CBluetoothServer::Error error);

    void slotNeighbourConnected();
    void slotNeighbourRead();
    void slotNeighbourDisconnected();
    void slotNeighbourError(CBluetoothSocket::SocketError error);

    void slotTest(CBluetoothSocket::SocketState state);

private:
    void innerStart(bool mute = true);
    void innerStop();

    void send(const QByteArray& baMessage, const CBluetoothAddress &address, int iMessageCode);
    void send(const QByteArray& baMessage, CBluetoothSocket* pSocket       , int iMessageCode);

    QUuid   m_ServiceUuid;
    QString m_ServiceName;

    CBluetoothLocalDevice*           m_pLocalDevice;
    CBluetoothServiceDiscoveryAgent* m_pAgent;

    CBluetoothServer* m_pServer;
    CBluetoothSocket* m_pSocket;

    QList<CBluetoothAddress> m_ActiveClients;

    quint32 m_uiNextBlockSize;

    QList<CBluetoothAddress> m_PendingDevices;

    QMap<CBluetoothAddress, CBluetoothSocket*> m_Neighbours;
    QMutex m_Mutex;
};

}

#endif // ICPSERVERBLUETOOTH_H
